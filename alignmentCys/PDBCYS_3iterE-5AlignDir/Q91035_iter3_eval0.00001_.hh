TPLAYKQFIPNVAEKTLGASGRYEGKITRNSERFKELTPNYNPDIIFKDEENTGADRLMTQRCKDKLNALAISVMNQWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRSKYGMLARLAVEAGFDWVYYESKAHIHCSVKA
TAMRLKQFFPNLSENNLGASGRAEGKIARNSERFNELVSNYNPDIVFKDEENTGADRFMTKRCKECLNRLAIAVMNQWPGIRLRVTEAWDEDGNHPPGSLHYEGRAVDITTSDRRPEKYGLLAQLAVEAGFDWVHYESKHRVHCSVKA
TPMSYKQYVPGVSENNLGASGRAEGRITRSSERFNELVCNYNTDIDFKDEERSNADRFMTKRCKDCLNKLAIAVMNQWPGVRLRVTEAWDEDGHHPPGSLHYEGRAVDITTSDRDTKKYGLLAQLAVEAGFDWVHYESKYHVHCSVKA
TPLVFKQHVPNVSENTLPASGLGEGRVSRNDKRFRDLVPNYNVDIIFKDEEGTGADRLMTQRCKEKLNTLAISVMNQWPGVKLRVTEGWDEEGKHATDSLHYEGRAVDVTTSDRDRAKYGMLARLAVEAGFDWVYYESRSHIHCSVKS
TAMHYKQFFPNFSENNLGASGRAEGKITRDSERFNELVCNYNPDIVFKDEENTDADRLMTKRCKDCLNRLAIAVMNQWPGVHLRVTEAWDEDGHHPQGSLHYEGRAVDITTDDRETEKYGLLAQLAVEAGFDWVHYESKYHVHCSVKA
KPLVFQQHEPNVSEYSITASGPPEGRIQRNDSRFMELVPNYNADIEFKDDEGTGADRLMTQRCKEKLNTLAISVMNLWPNVRLRVIDGWVD---RAGSSLHNEGRAVDITTSDRDKSKYGMLARLAVEAGFDWVNYNRR-YIHCSCKS
VPFLKGEYVPKMSEQTIGASGPSTGRITRTSPRFHELVPNWNTDIEFRDEEESNEDRFYDATCKERLDNLAIRVANQWVRVKLKVLEAWDEGNDRLDDPLHYEGRAVDITTNDQDRRKYPMLARLAVEAGFDWVYYNDNI-VHCSVKS
TPLVFKQHVPNVSENTLGASGLTDGRIKRGDARFKSLVRNNNPDILFKDEEGNGVDRIMSQRCKDKLNTLAISVLNQWPGVKLRVTEAWDEDGMHAENSLHYEGRAVDITTSDRDRSKYGMLARLAVEAGFDWVYYETRGHIHCSVKS
TPLLYKQFIPNIPEKTLGASGRAEGKITRNSERFKELIPNYSADIIFKDEEKNGADRMMTQRCREKVNKLAIAVINQWPGLKLRVTEGWDEDGHHSQDSLHFEGRAVDITTSDRDRTKYGMLARLAVEAGFDWVYYESKYHIHCSVKS
TPLVFKQHVPNVSENTLGASGLAEGKISRSDQKFQDLVVNYNPDIIFKNEENTGADRIMTQRCKDKLNTLAIAVMNRWPGVKLRVTEAWDEDLHHTEDSLHYEGRAVDITTSDRDRTKYGMLARLAVEAGFDWVYYESRGHVHCSVKS
PRQPISSLFPMCAEKTLGASGRYEGKITRKSERFKELTPNYKPDIIFKDEENTGADRLMTQRCKDKLNSLAISVMNQWPGVKLRVTEGWDEDGHQSEESLHYEGRAVDITTSDRDKSKYGTLSRLAVEAGFDWVYYESKAHIHCSVKA
TPLVFKQHIPNVSENTVGASGIHEGKITKPDPRFKEMVTNLNPNIVFRDEEENNEDRVMSKRCKDKLNTLAIAVMNEWPGVKLRVTEAWDTQGHHAPTSLHYEGRAVDITTSDRVRSRYGMLARLAVEAGFDWVYYESRSHIHCSVRS
LPLVFKQHVPNVSENSLGASGMQEGPISRNDSKFRNLETNYNKDIIFKDEEGTGADRVMTQRCKEKLNILAVSVMNQWPGLRLMVTEGWDEDHMHARESLHYEGRAVDIMTSDKDRSKIGMLARLAVEAGFDWVYYESRSHIHCSVKS
TPLVFKQNVPNVSENSLGASGMSEGRIKRDDAKFKDLVRNHNADIVFKNEEGDGSDYHMTRRCQDKLNSLAVSVMNNWKGVMLRVTEAWNDNNSHAKDSLHYEGRAVDITTSDKDRAKYGMLARLAVEAGFDWVYYESRGHIHCSVKS
TPLVFKQHVPNVSENTLGASGLNEGKVSRDHQRFKELVPNYNSDIIFKDEEGTGADRLMTQRLKEKLNTLAISVMNQWPGVKLRVTEGWDEEGHHAIDSLHYEGRAVDITTSDRDRSKYGMLARLAVKAGFDWVYYESRFHIHCSVKS
TPFVYKQQMPAVSENTFGASGLFNGRITRDSERFHTLKQNFNTDIIFKDEEKTGADRFMTQRCKDKLNALAISVMNQWEGVKLRVTEGWDEDGFHTEESLHYEGRAVDITTSDRDRTKYGMLARLAVEAGFDWVYYESKAHIHCSVEA
VPLLYKQFVPSVPERTLGASGPAEGRVTRGSERFRDLVPNYNPDIIFKDEENSGADRLMTERCKERVNALAIAVMNMWPGVRLRVTEGWDEDGHHAQDSLHYEGRALDITTSDRDRNKYGLLARLAVEAGFDWVYYESRNHVHVSVKA
IPLQYKQFVPSVAEQTLGASGKAEGKVGRHSSRFKDLVPNYNPDIIFKDEENTGADRLMTERCKDRVNALAIAVMNMWPGIRLRVTEGWDEDGHHLPDSLHYEGRALDITTSDRDREKYGMLARLAVEAGFDWVYYESKAHIHVSVKA
TPLVFKQHVPNVSENTLGASGLTEGPITRDDPRFGDLIPNYNSDIIFKDEEGTGADRFMTQRCKEKLNTLAISVMNQWPGVKLRVTEGWDEEGHHAADSLHYEGRAVDVTTSDRDRSKYGMLAKLAVEAGFDWVYYESRAHIHCSVKS
TPLQYKQRVPNISEDTFGASGPPEGRINRNDERFNTLSPNNNDDIVFKDKEGTGADRLMTQRCKDKLNTLAISVMNEWPGIKLRVVEAWDEDQPNV-EPLHAEGRAVDITTSDRDKNKYGALARLAVEAGFDWVNYESKAWVHCSVKS
TPLVFKQHVPNVNEFTLGASGQSEGKLTRDHPKFKSLVPNYNSDIIFRDEEGTGADRLMTQRCKEKLNTLAILVMNQYPGVKLRVTEGFDEESYHSTQSLHYEGRAVDVTTSDRDRSKYGMLARLAVEAGFDFVYYESRSHIHCSVKS
RPLLRQQYVPHVSEGTIGASGPSEGRIYRNTPRYRKLERNYNTDIEFEDRERDGSDRTMTKRCKDKVNLLSMLVKNTWAGVSLKVIEAWDGDGVHRKGSLHYEGRAVDIKTSDNDLSKNGLLARLAVESGFDWVYYESKFYVHASVRA
RPLLRQQYVPHLSEGTVGASGPSEGRIYRNTPKYRKLERNYNPDILFREDEEDGSARIMTKRCKDKVDLLSTLVRNRWAGVSLQVISAWDGDGLHPRGSLHYEGRAVDIKTSDDDTTKYGLLARLAVEAGFDWVFYESKFHVHASVRA
-PLYFKQRVPDVDEFSLGASGRPQGKITRNSSKFNKLVACYNTDIVFKDEERTGADRLMSKRCREKLRNLATKVKQKWKGVKLRVTEAWDEDGQHSLDSLHYEGRAVDISTSDKDPKKLPDLGSLAVDAGFDWVYYDRRSSIHASVRS
VPLRYKQFVPNVPEKTLGASGKSEGKIRRGSERFIELVPNYNPDIIFKDEENTGADRLMTERCKDRVNALAISVMNMWPGVKLRVTEGWDEDGHHAHDSLHYEGRALDITTSDRDRSKYGMLARLAVEAGFDWVYYESKAHIHVSVKA
TPLLYKQCIPNVSENTLGASGPNEKKITREDDEFKDLQTVYNADIMFKDEEGTGADRLMTQRCKDRLNSLAISVMNQWPGVKLRVTEGWDEDGHHAPNSLHYEGRAVDITTNDRDRTKYGMLARLA-EAGFDWVFYQSRAHVHCSVKA
TPLVFKQHIPNVSENTVGASGNYEGKITRSLNPGSTFVKNMNPSIIFRDEENNDEDRMMSKRCKDKLNSLAIAVMNEWPGVKLRVTEAWDTEGHHAPTSLHYEGRAVDITTSDRERSRYGMLARLAVEAGFDWVYYESRSHIHCSVRS
TPLVFKQHVPNISENTLGASGLNEGRITRDDPRFKDLVENYNPDVVFKDEEGTGADRIMSQRCKDKINTLAISVMNQWPGVKLKVTEAWDEDGFHAKDSLHYEGRAVDITTDDRDRSKYGMLARLAVEAGFDWVYYENRGHIHCSVKS
TPLVFKQHVPNVAETTLAASGQGDGKINRDETRFKELVPNYNPDIIFKDEEGTGADRLMTLRCREKLNTLAISVMNQWPGVRLRVTEGWDEDGPHAINSLHYEGRAVDITTSDRDRSKYGMLARLAVDAGFDWVYYESRAHIHCSVKS
IPMKIREHIPDTSETSLQASGP--RKIKRGSNGYKELITNADPNIVFR-EDKAGNNRRMSKRCERKLKILSSLVRKEWIGVKVRVIRAYDD-GHHGPHSLHFSGRALDITTSDEKRDKLPMLGRLAYRAGFDWVY-RAKAYIHASVKS
TPLVFKQHVPNVSENTLGASGISDGKIRRNSEKFKNLVKNENPDIVFKNEEGGGSDYLMSRRCQDKLNSLAVSVMNNWKDVRLRVTEAWDDSNSHAKDSLHYEGRAVDITTSDRDRSKYGMLARLAVEAGFDWVYYVSRGHIHCSVKS
TPLKYKQIVPEISENTLGASGLSEGRIEREDEQFNELTPNYNADIIFKDEEGTGADRLMTQRCKDKLNALAILVMNQWPGVKLRVTEGWDEDGHHSPDSLHYEGRAVDITTSDRDRSKYGLLARLAVDAGYDWVYYESRAHIHCSVKA
TPLVYKQHVPNVSENTLGASGLAEGRISKEDPRFKKLVTNDNPDIIFRDEEGDGTDRIMTQRCKDKLKILAISVMNTWQGVKLRVTEAWDDDGHHAKDSLHYEGRAVDITTSDKDRAKYGMLAKLAVEAGFDWVYFESRGHIHCSVKS
TPLVFKQHVPNVPENTLTASGLTEGRIGRNDSRFKDLVPNYNQDIVFKDEEGTGADRLMTQRCKEKLNTLAISVMNQWPGVRLLVTEGWDEEGYHTPESLHYEGRAVDITTSDRDRSKYGMLARLAVEAGFDWVYYESRAHIHCSVKS
TPLVFKQHVPNVSENTIGASGLSDGAITKEDAKFKDLVSNENPNIEFKDEERTGADRMMSERCKEKLNMLAISVMNQWPGVKLRVTEAWDEDGTHSKDSLHYEGRAVDITTSDKDRA-YGMLARLAVESGFDWVYYESRNHIHCSVKS
TPLVFNQHEPNISENSKSASGPPEGRITREDEKFKDLVPNYNPDIEFKDDEGTGADRHMTQRCKEKLNTLAISVMNQWPGVRLRVIEGWDEE-SHLENSLHYEGRAVDITTSDRDRSKYGMLARLAVEADFDWVFYESRSYIHCSVKT
YPLVLKQTVPNLSEYMSGASGPIEGVIQRDSPNFKDLVPNYNRDIIFRDEEGTGADRLMSKRCREKLNTLSYSVMNEWPGVRLLVTESWDEDHQHGQESLHYEGRAVTIATSDRDQSKYGMLARLAVEAGFDWVSYVSRRHIYCSVKS
YPLVLKQTIPNLSEYQTGASGPLEGEIKRDSPKFKDLVPNYNRDILFRDEEGTGADRLMTKRCKEKLNVLAYSVMNEWPGVRLLVTESWDEDHQHGQESLHYEGRAVTIATSDREPSRYGMLARLAVEAGFDWVSYVSRRHIYCSVKS
FPLVLKQTVPNLSEYHNSASGPLEGAIQRDSPKFKNLVLNYNRDIEFRDEEGTGADRVMSKRCREKLNMLAYSVMNEWPGVRLRVTESWDEDRQHGQESLHYEGRAVTIATSDHDQSKYGMLARLAVEAGFDWVSYVSRRHIYCSVKS
YPLVLKQTVPNLSEYQSGASGPLEGVIDRKSPKFKDLVPLYNSDILFRDEEGTGADRMMTKRCKEKLVMLATSVMNEWPGVKLLVTESWDEDHHHGEQSLHYEGRAVTIATSDRDQSKYGMLARLAVEAGFDWVSYVSRRHIYCSVKS
YPLVLKQTVPNLSEHQLGASGPLEGEIPRDSPKFKDLVPNYNRDIVFKDEEGTGADRLMTKRCREKLNALAYSVMNEWPGVRLLVIESWDEDHDHGQESLHYEGRAVTIGTNDRDLSKYGMLARLAVEAGFDWVSYVSRRHIYCSVKS
TPLVFKQHVPNVSEHTLGAAGPAERRVARDDPRFRDLVPNYNADIVFKDEEGTGADRLMTQRCKEKLNTLAISVMNQWPGVRLRVIEGWDEEGGHAADSLHYEGRAVDVTTSDRDRSKYGMLARLAVEAGFDWVYYETRGHIHCSVKS
TPLVFKQHVPNVSENTLGASGLPEGRITRDDSRFKELVPNYNTDIYFKDEEGTGADRLMTQRCKEKLNTLAISVMNQWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRSKYGMLARLAVEAGFDWVYYESRSHIHCSVKS
-------------------------------------------------YETRNSERFMTQRCKDKLNSLAISVMNQWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRTKYGMLARLAVEAGFDWVYYESKAHTHCSVKA
---------------------------------------------------ITGADRFMTQRCKDKLNSLAILVMNQWEGIQLRVTEGWDEDGHHADNSLHYEGRAVDITTSDRDKKKYGMLARLAVEAGFDWVYFGSRSHVHCSVRS
---------------------------------------------------------FLSQRCKDRLNSLAISVMNQWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRNKYGLLARFQAPPGCQEAELHSSNTFMSLSPT
MPLIFKEHVPNVYENTLGASGLAEGPITRGSARFHELVPNYNPDIIFRDEEGTGADRLMTERCKERLDTLAISVMNQWPGVRLRVKEGWEDDTQHNEESLHFEGRVVDIATSDSDRSKYGMLGRLAVEAGFDWVYYEARSHVHCSCKS
------------------------------------LTPNYNPDVVFKDEEGTGADRLMTQRCKERLNSLAISVMNTWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRDKYGTLARLAAQAGFDWVHYESKSHVHCSVKA
------------------------------------------------------------SRCKDKLNTLAVSVMNQWPGVRLRVTEGWDEEGHHAARSLHYEGRAVDLTTSDKERSKYGMLARLAVEAGFDWVYYESRTHIHCSVKS
---------------------------------------------------------------------LAIAVMNMWPGVKLRVTEGWDEDGNHFKESLHYEGRAVDITTSDRDRDKYGMLARLAVEAGFDWVHY------------
-------------------------------------------------------------------------VMNLWPGIRLRVTEGWDEDGHHSEESLHYEGRAVDITTEDRDRNKYAMLARLAVEAGF-----------------
---------------------------------------------------GTGADRLMTQRCKEKLNTLAISVMNQWPGI-LRVIAGWDEENSHLDNSLHYEGRAVDLTTSDRDHSKNGKLARLAVEAG------------------
------------------------------------------------------------QRCKDKLNALAISVMNQWPGVKLRVTEGWDEDGHHSEESLHYEGRAVDITTSDRDRSKFPYICRLFL---YMFSFFI-----------
TPFVLKQHVPNLSETTLGASGQPEGKVSRGDPEFKKLVTNKNPNIIFQNSEGTGADRVMSKRCSDKLNNLASLTMEQWPGVRLRVVEAWDEDETHPEDSLHYEGRAVDVTTSDKDKSKYGMLARLAVEAGFDWVHYEYRSHIHCSVKS
-PLIYNERFPNEPENAPGSAGAAELRIRRSDPGFKRLVQNNNPDIIFRDEENTGADRMMTYRCKQKLDMLAILTMNYWPNVKLRVIDAWYEQNRYSRSALHYEGRAVDITTSDRDRNKLGMLARLAIQAGFDWVYYESHLHVHASVQ-
------------------------------------------PSSISRSEGGIGFGRLV--RCKEKLNILAVSVMNQWPGLRLLVTEGWDEDHMHAPESLHYEGRAVDIMTSDKDRSKIGMLARLAVEAGFDWVFYESRNHIHCSVKS
VPFLKGEYVPKMSEQTIGASGPVTGRIRADTPRFRELVPNWNTDIEFRDEEESNEDRFMTPICRARLDYLAILVANQWARVKLKVLEAWDDGNDKANDPLHYEGRAVDITTDDADRNKYPILARLAVVAGFDWVKYDGKV-VHCSVKS
----LGEHVPARTEA--DASGAAAGVVAAGSPEFDALVRLDDPTVVVKDEEGSGADRMMTPRLAELVGVLAAHVAQAFPGRRLRLTEAWDPDGEHSHSSLHYEGRAADLTVDDRDRAKLGRLAALAVQTGFDWVLHEND-HVHVSVRA
TPLVFKQHVPNVPENMLAASGLPEGKLDRGHKRFRDLVPNYNSDIVFKDEEGTGADRLMTQTMTE-----------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------HHSDESLHYEGRAVDVTTSDRDRAKYGALARMA----------------------
TPLVFKQHVPNVSENTLPASGLSEGRITRYDSRFRDLVPNYNTDIIFKDEEGSGADRLMTQS--------------------------------------------------------------------------------------
TPLVFNQHDPNINENSKSASGPPEGRITRNDEKFKDLVPNYNPDIDFKDEEGTGADRLMTQFQSDRRSSPGAAGCRRVPG--------------------------------------------------------------------
---------------------DIDYPVTRDKEDFKRLTQYPASNIVFDNEERSGADRVMSKALLDHLRTVQRMVQDEFSGVKLKVLEAWDETGDHPAGSLHYEGRAAKLTLSDGDAAKLPRLAAFCICDGAGYVE-------------
---------------------AIDSAITRDSEDFRQLVQYPASHILFADEESSSADRVMSKRLYTALLRVDKHVREQLNA-RLRITEAWDEDGDQAENSLHYEGRAAKLELS--GSSDLTSLAKYCICADIDYVE-------------
--------------------GGIRDVILRNSARFRKLVRNADTEVVFENDD----CRRTTARAKSKLDVLASRVRQEWAGRKLKVIKAWTDQRAQDPASLHYEGRALRLQLDNNDRSMLSRLAGLALASGFDWVSYPLNDYIHASV--
--LVLGEYQPGDLENSRSASGPIDTSDLVNHVPHRGLVKVNNTKIFFD-KEN---AKWMTKTLRKRLLDLCQHIENQWPDVYIRVRDTWNPRKYQTMDHFHWAGRGIDMSLSERDI-HPGKLAQLALSAGFDWCIYKYVNYLHCSVK-
--LVYKEFQPVELENSYGASGPIDATDLVDRVPHKRLVRINNTKIIFI-KEN---ARWMTKTLRKHLLDLNQHIENQWPGVYIQVHDTWNPRNFRTMDHFHWAGRGIDMSLSEKDF-HPGKLAQLALSAGFDWCIYKYTNLLHCSVK-
-----GEYQPVQMEHTTDGSGPLDARDFHHSIPNRRLVRVESPHIVFDSEE----ARWMSEGCRDRLLKLSTQIQNTWANVKLRVLRAWI----------------------------------------------------------
-------------------LGDPKMSIRRNEPDFHILTQMWGDSVLFSSEEANSADRLMVPKLATKISLLGKYVNAEWPSKKLLVVEAWDERGKHGDASLFYEGKAAQLALSNKDDAENTRLEQLAICTGFQYVSLKSDSSIEV----
--MEFGTSIPDGPEA--DASGVILEFILPQCPEFKALQHLTATETVKPDPPSNGDPSVMSKRLRRHITTLASVVRGVFGDAYVRVLEAYVEPADISKASLHNVGRAARITIDDFASDRLGVLGGLAVEAGFDYVAYTSRDSLYVSVI-
--------------------RPIQLHIARGSKRYKDLVVYQATYLHFASSD----CRIMSSRLHTRLSSLAQDY--YWRYIKLLVLKAWTPYPSLDNTSLHYEGRSVRIHVTSR---NVTRLLKMAVSAGFDWVMYDKKGYARMSV--
--FVPGEYQPKQLESNLEASGPLDTRDFHNQFPNKRLVRVDSPYIIFDSEE----ARWMTKGCRDRLIKLSTQIHNTWAKVILRVIRSWVKDDSHEDSPLLYEKN-------------------------------------------
-----------------------------------SLRKLFGDTIIFLSEEPSESDRYVTPELAEVLTKLAKFVEAEWPDRKLLILEAWDEAGSHGNQSLFYTGRAARVALSKTDQNTFQRFTQLLQCSDADFF--------------
----------------------------------------------------------TAPELDAAVKKLWERV------GPLRITSGTRCPSHNVPRSRHLRGRAVDVAAD---SGLLRAICRAAEECGFNQIPYQEKGYVHLGV--
----------------------------------------------------------LHPDLLEKLERLILSG------VKIKITSGYRCEKHNVPNSKHMKGMACDITSP---E--LEKAYEIVQKLGFSYVIDKLKKYIHME---
----------------------------------------------------------LHEPLYGALRELARRW------GGVRVTSGYRCPSHNVPGSLHTRGRAVDLACP---ASRQGELLALAKELGFDQRPYPSRGFVHLGWR-
----------------------------------------------------------MDPRYLKLFDALEDRW------GPLRVTSGYRCQRHNVAHSLHTKGLAADVACV---KARQPELGEMAKALGFNQVLYPMRGFVHLGVK-
----------------------------------------------------------VDPGALEALKALEARV------GRLSITSAYRCRSHNSPRSLHMAGRAFDVACP---SWRQDALVTMARQAGFTEIKYPRRGFVHLGYK-
-----------------DVVGPALGQIHRENEEMSRLYEYQFNDFKFLHERRWSANRVMTPRLAFRMRRLAIIAKDGLQGSKIIVERAFADKIDRGESSLFLEGRGARLTFLDNPDTNIPLLGHAAVCAGFDFVRNVDNVAIEVFV--
----------------------------------------------------------LHPDLLEKLEKLRQEM-----KIPIVITSGYRCEKHNVAKSLHLFGQAADIAIP---ARIMDQVCWLAKTAGFDQVPYHSRNFIHLGVF-
----------------------------------------------------------LWPPLVECLEKLRSLW-----KEPIVLTSGYRCPNHNVANSLHVEGRAADVVVM---HRYQPLFCELAERAGFTSIPYGKRNFIHLAIK-
----------------------------------------------------------VHRKLVSLLQRLRERQ-----GGPLVVTSGYRCAPHNVANSLHRRGLAADVAVA---PAAQRAFCEMAKEAGFAKAAYPERSFVHLEVL-
----------------------------------------------------------LQPELLSRLEALRGRW-----G-PLRITSGYRCPRHNVPRSRHMKGAAADVVVS---RNRQELFCAVAREEGFESIPYRDRGFVHLAV--
----------------------------------------------------------LHPTLLENLEKLIA-------GLKIKITSGYRCEKHNVPNSKHMKGMACDITC-----NDIEKAYEMAQKLGFSYVIDKLKKYIHME---
------------------------------APCFREFAC-RASDTILIDDE-----------LVVLLQCIREHF-----GAKVHITSGYRTAAYNSKNSQHIQGRAADFWVEGV---PVATVAAYAEKLGRGGIRYPKDGWVHVDTRP
------------------------------SPSFKEFRCSDNTDPIFIDSE-----------LVEILQKIRNHF-----GKPVNITSGFRTASKNAKFSQHLYGKAADIWISGV---TVEQIAAYAETLNRGGIRYPKEGWVHIDTRA
------------------------------SKNFKEFRCKDGSDPIFIDSE-----------LVRILQKVRDHF-----GSPVIINSAYRTAAYNAKFSQHQYGKAADIYIQGI---LITKLAEYVETLNKGGIIYPIKAFVHVDVRA
------------------------------SPAFREFRCRDGTDTILIDEG-----------LVVLLQCIREHF-----GKPVAITSGYRTASHNSRSSQHLLGRAADIQVQDT---DPLAVAAYAESLGWGGVRYPVRGWVHVDTRP
------------------------------SPHFCEFRCKDGSDPVFIDTA-----------LAELLERIREHF-----GKPVTITSAYRTPAHNAKFSQHLYGRAADIRVQDV---SVEDVAAYAESLDRGGVRYPAKGWVHVDTRA
------------------------------SANFAEFRCKDGTDPIFIDDV-----------LVKLLQNIRNHF-----GKAVTITSAYRTAAHNATYSQHCYGMAADIRIQGV---DVETLATYAETLNTGGIRYPVKGWVHIDTRA
------------------------------SPNFKEFAC-KGSDVVLLDDE-----------LVVLLQCIREHF-----GKPVHITSGYRTAAHNSKSSQHLLGRAADFYVEGV---DVATVAAYAETLSRGGIRYPKDGWVHIDTRA
------------------------------SRSFREFACKDGTDPLFVDSE-----------LVQVLQAIRDHF-----GAPVVITSGYRTAAHNAVYSQHQYGRAADIRVSGV---PVEQLAAYAETLGTGGIRYPAKGFVHVDVRK
------------------------------TPHFTELACRCCGRLL-VQSE-----------LVHKLETLRQLV-----KKPVLVNSGYRCPAHNAVNSYHLKGMAADIHVPGL---AVVELSRLAEQAGFNGITYPKQSFLHVDVRG
------------------------------SAHFVELSCQCCGRLL-IHPY-----------LINKLEAFRQLA-----GKPVLVNSGYRCPAHNETNSYHLKGMAADIQVPGV---AVAELSRLAEQAGFGGIVYQSQGFVHVDVRD
------------------------------SAHFAELACRCCGKLV-IHLE-----------LVYKLEDLRRLL-----DKPVLVNSGYRCPTNNVVNSFHSKGMAADIRVPRM---AVKEIAHLAEKVGFGGIIYASQ--VHVDVRD
------------------------------SKNFKEFDCSHGDSVVKLDSR-----------LLEKLQLLRDKL-----NNPINVTSGYRTPECNSSNSYHMKGMAADIYSPGY---TPAQIAKAAEEVGFTGIIYST--FVHVDVRP
------------------------------DKNFQEFQCLGGSQLVKLDHR-----------LIEKLQQLRDQV-----GSPVIVTSGFRTPEHNSLNSQHLLGRAADIQVPGY---SPEAIAQIADALGFTGVIYAT--FTHVDVRT
------------------------------AKNFQEFQCRGGSQLVKLDHQ-----------LLEKLQQLRNQV-----NAPINLTSGYRTPEHNSPNSQHLLGRAADIQVPGH---SPEAIAKMAEKIGFAGIIYST--FTHVDVRT
------------------------------SKNFKEFQCGDGGYQVRLDSQ-----------VLKKLQELREQT-----GRPVLINSGYRTPSYNSPRSQHLLGKAADIMVPGM---ELESLARVAEGIGFGGIIYRT--FIHVDVRS
----------------------------------------------------GKQVAPIDPKLLETLWGTQAFIGI---SQPLEILSGFRTAATNARKSLHLEGRAADIRLT---QLDADVLGGLIRSFRQGGVFYKNGGWIHAD---
----------------------------------------------------NDQVHPMDPGLYDILAKIQARTEA---KSPFQVISGYRSPATNAKHSLHMEGKAMDIYLE---DVALEHVRAAALDLGMGGVYYPQSRFVHVD---
----------------------------------------------------QNLVVQNDRRL---LY--QRA------GDGVKVNSGY-TT---ARESFHTKARAVDYLI----NATLSEIARVA------AVLY--NNF-------
----------------------------------------------------GKATAKIDPKLLEMLWSTQAFIGL---LQPLEILSGYRSPESNARQSLHMVGKAADIRIA---GLNEEVLGGLIKSFRGGGVYYPRGGWIHAD---
----------------------------------------------------SGSVGAIDPQLFGLLFSLRRELET---DTPFQVISGYRSPATNAKHSLHMDGMAIDIRLP---GVSLADLRDAATSLKIGGVFYQQEDFVHVD---
----------------------------------------------------RNEETQINPKTLDRLYDIGQTVKR---NYPFEIISGYRALQTNGKDSQHTHGNAIDFRIP---GVRTETSRDVAWCTGSGGVYYKQDGFVHID---
----------------------------------------------------TGDITEVDTDLLNLLVSIRDRLDIS-PNQPFDLICGYRSPLTNATHSQHLLGKATDIAMP---GVPLARLRMAAEFNQQGGVYYPEDGFIHVD---
----------------------------------------------------SGKVGMIDPQLFDLLFQVRRELGT---DQPFQVISGYRSPATNARHSLHMDGKAIDIRLA---GVPLADVRDAAKSLQGGGVFYESDQFVHID---
----------------------------------------------------DGAMVQIDVGLLNLMYAMQEWAQS---GRSITINSAYRTPRRNARNSLHMRGKAVDFTMR---GVGIGELEQMAKYYNVGGIIY--NSFVHLD---
----------------------------------------------------TGEKAPIDPALMDFLFDLFYRTGL---PPTVQVLSGYRSPQTNARESFHMQGKALDFRVP---ALPGPALAEIAKTMQRGGAFYPGTGHIHID---
----------------------------------------------------TNEMTKMDLRVIEYLNRLDNTLGG---NNEIHIISGYRSPAYNAKDSLHMKGRAIDLAIP---SFGLDQIRRSALTLAAGGVYYPQPGFVHID---
----------------------------------------------------SGDVGVMDPDLFHLLHRVRQTLQT---QRPFEVISGYRSPHTNARRSLHMDGKAIDVRLP---GVSLSDLRDAAISLRAGGVYYAREQFVHID---
----------------------------------------------------TDDVKEIDLRTIDIMAASHNLLDV---NEPYMLLSGYRSPKTNAKNSLHMRGQAADLRLA---SRSVSQMAQAAEACRAGGVKYQRSNFVHMD---
----------------------------------------------------TDQTTEMDLRVVEYLNRLDNSLGG---NNEIRIISGYRSPEYNAKDSLHMKGMAIDLAIP---GFGLNQIRRSAIALAAGGVYYPQSGFVHID---
----------------------------------------------------SGAIGHMDPTLFDLINKVRASLKT---DVAIEVISGYRDPHTNAKRSLHMVGQAMDLRLK---GVPLDELRDAAKELALGGVYYPKDGFVHMD---
----------------------------------------------------TGESTRMDPRLYDMLHALSLACGG---NT-FEIISGYRSPTTNARRSLHMDGKAIDIRLV---GVDTARLRDAALALGGGGVYYPDSDFVHID---
----------------------------------------------------SGDVGTIDPELFGVLYQLRTVLRS---DNPFQVISGYRSPNTNAKHSLHMDGKAIDIRLP---GVPLDDLRDAAVSLQAGGVFYRSENFIHID---
----------------------------------------------------TNEVTEMDVNTLEFLNLVDKKFGG---NNEIHIISAYRSPLYNAQHSLHLAGRAIDISIP---GKSIASIREAAVDLHMGGVFYPNSGFVHID---
----------------------------------------------------SGDTHPVDPRLLDVMAATHRRLGA---KGAIHIVSGYRSPTTNAQGSLHMSGKAVDIRIP---GATTRVVGRAAKSLKVGGVTYPGSKFVHLD---
----------------------------------------------------TNQHTEMDLAVIEYLNMVDKVLGG---GREFRIISGYRSPEYNAKQSLHMEGKAIDIAVP---GVSLAVLRDLAAGFRCGGVYYPHSGFVHLD---
----------------------------------------------------NNQVHKMDPQLFTKFYRVQQNLCLR--NTEIQIICGYRSAASNASNSYHIRGQAIDFRID---GVPLAKLRDAVEALNDGGVFYPRSNFIHMD---
----------------------------------------------------TDQVHRMDPSLFMKFYQLQSDLGLR--TAQIDVICGYRSAATNASNSYHIKGQAIDFKIP---GVPLARLRQAAENLDSGGVYYPYSNFIHVD---
----------------------------------------------------TNQVHRMDPNLFTKFYKVQQNLGLR--NTEIQIICGYRSAASNASNSYHIRGQAIDFRID---GIPLAKVRDAVDALQNGGVFYPRSNFVHMD---
----------------------------------------------------DNQMHAININLINLLFAQQQYLDL---GRPLVLHSGFRTRRHNAKNSQHLSGNAGDFHIE---RASLSELAALARRFRVGGIIY--PTFIHND---
----------------------------------------------------TDGVIDINIGTIDIMAAAHNLLET---SEPYTLLSGYRSPETNARNSRHMVGEAADLQMQ---SRSVTQVFNAARSCNAGGVRYSRSNFVHMD---
----------------------------------------------------SGEVGHMDPKLFDLLYRLKLTLGS---RESFQVISGYRCPTTNAKRSLHMDGKAIDVRIA---DTPLADLRDAALSLGVGGVYYPHDQFVHLD---
----------------------------------------------------SGKVAEIDPQLLLLLGQLNSKLDN---SKELHIISGYRAPESNAKRSLHMDGKAIDIRLP---GTDLRNLQKAAMSLKGGGVYYASSQFVHMD---
----------------------------------------------------DGAVVNIDAGLLNLLYGLQEWAIA---GKPITVNSAYRTARRNARNSMHIHGRAADLTMR---GISLRQLADMAAHFKAGGIIY--DSFIHLD---
----------------------------------------------------QNVAAPMDKRLFDLLYSLKTTLNV---DDEIHVISGYRSPKTNAKKSYHMRGMAMDIAIP---SVKLKTLREAALSLKLGGVYYPNSGFVHVD---
----------------------------------------------------ANQVKRIDPRLFDQIFRLQVMLGS---KKPIQLVSGYRSPHTNAKQSFHTKGQAMDFHID---GVTLANVRKAAMRMRAGGVYYPRSNFVHID---
----------------------------------------------------QNEAAPMDKRLFDFAYQLRQSLSF---EDELHIISGYRSPKTNAKKSYHMKGMALDLALP---GVKLADIREAAIELKLGGVYYPSSGFVHID---
----------------------------------------------------SNHIKPIDPALFDQLYRLQGLLGT---TKPVQLISGYRSLSTNAKNSYHTKGQAMDFHIE---GIALNNIRKAALSMGAGGVYYPRSNFVHID---
----------------------------------------------------TGAIHPIDPHLLDLLVSVQRKLGT---KESVHVVSGYRSPATNATHSLHTEGKAIDIRVP---GHATRMIGRAARSLRAGGVTYPESDFVHID---
----------------------------------------------------NDEIHPIDIKVIEYLYDVSKKCSH---DREIHVLSAYRSPSTNAKQSYHLFGKAIDFRIP---GISLHHVRNTALSLHKGGVYYPKSGFIHID---
----------------------------------------------------EDVVAEIDPRLFHLLFGLQRWVET---GRLIDLTSGYRTPEHNSPTSEHLNGRAADIKIP---GVQPGAVVSMARFFEMGGVIY--NSFTHVD---
---------------------------------------------------QTGTC-QMQLRFLDALQLLRSAL-----GFPFIITSGYRSPHHS-SPGTHALGCAADIGVYGERA---YELVQAATSLGMTGIGVM-GRYIHLD---
---------------------------------------------------GCGSQ-KMDPQFMERLEELRMAY-----GKPIIVNSAYRCPNHN-GSGPHTTGRAVDVQVSGEDA---HTLMALAMHHGFTGIGVS-SRFIHLD---
---------------------------------------------------GCGSQ-EMDPEFMERLEDLRGAY-----DKPMPVTSAYRCPNHN-GPGPHTTGMAVDIQVAGEDA---HKLMTLALYHGFTGVGVR-ARFLHLD---
---------------------------------------------------GCNSTGEMDSAFMTELVTLRQQF-----GRPMALSSAYRCPKHP-APGEHCTGLAVDVRCRGEDA---VQILRLAMNLKFTRFGIS-ARFLHLG---
---------------------------------------------------GCGME-KMDADLLHLLDEARDLA-----GAPFSLTSAYRCPKHNVPTSAHVRGYAVDIRCVDSHS-R-FVILQALLEVGFRRIEL-APTWIHVD---
---------------------------------------------------GCGME-KMDADLLQMLDEARDLA-----GIPFPLSSAYRCPKHNVPTSAHTRGYAVDIRCVDSHS-R-FAMLQALLEAGFRRVEL-APTWIHVD---
---------------------------------------------------GCGIE-HMNQDLLMMLDEVRDRA-----GIPLVLSSAYRCPAHNVDDSAHTRGYAVDIKCINSHT-R-FLILQAALEVGFRRIEL-APTWVHLD---
---------------------------------------------------GCSLN-NISYTLVLMLEDARKLA-----NMPFKINSACRCEAHNVKDSAHVKGLAVDISIPSDGA-R-FVILSALLNVGFCRVLI-YPTFIHVD---
---------------------------------------------------GCGLN-NISDALLDKLDEARDIS-----GVPFSINAGTRCKKHNEPDSAHLYGYAADISAKTSQQ-K-FAIISSLLKVGFVRIGV-YDTFIHAD---
----------------------------------------------------CGQC-EMQDEFLQRLDRARGIA-----GVPFVINSGYRCKSHNSKCSSHMIGWAADIKATDDKS-R-GHILYGLYMAGFTRIGI-RKDFIHVD---
-----------------------------------------------------NRQGLVHPQIVSNLAWVQAWL------APIVATSGLRTEVTNAHQSQHLP--AVDFWVPGA---NSEDVARMLEWARTGGVFYRSSKHIHLD---
-----------------------------------------------------GETVAMDPQLFDLLYDVKTHLGDS---DAREVLSAYRSPQTNAKNSLHLTGQAVDVRFPDL---SIRYIRDAAVALGRGGVYYPRSNFVHLD---
-----------------------------------------------------DDIVPIDPKLLDILYDLRRSIGNE---DAVEILSAYRSPKTNAKKSLHMRGQAIDIRLSNT---RTRRVRDTAVALGRGGVYYPNSNFVHLD---
-----------------------------------------------------GDKTQIDPRLFDTLYDVQQRLDGE---GALQIISGYRSPHTNAKRSLHMSGRALDIRLAET---PTSVVCKTALQLGRGGVYYPKSDFVHID---
-----------------------------------------------------GDTIAMDPRLLDLLYDLKVTLGDP---DARHVLSAYRSPATNAKKSLHLKGQAIDIRFPDL---STRHLRDTAIELSRGGVYYPRSNFVHLD---
-----------------------------------------------------DDVVAIDPQLFDILYDVKLRLGDP---DATQILSAYRSPRTNAKKSLHMTGQAVDVRFPDL---STRHLRDTAVALSRGGVYYPRSNFVHLD---
-----------------------------------------------------NRVGVPNVEMLQLAAWAQVVL------TVFEVTSGLRTHHTNARNSRHLP--AMDIKPLGV---NIDQLAKILQYPAFGGVVYRSHVHFDIR---
-----------------------------------------------------DKAVQMSPVLFDILYGMQGFFALH---NQHRVISGYRTRLTNVGDSRHMRGEAADIEFPGV---PVNYMGRLALYLQGGGVFYPSRGFVHVD---
-----------------------------------------------------NLSAPMDKRLYELLYQLNKTLNVS---DEYHVISGYRSPKTNAIKSYHMRGMAIDIAIPDV---KISHLRDAAISLKLGGVYYPKSGFIHVD---
-----------------------------------------------------NDVKTIDPRTVDIAAASHRLLDTS---EPYMLLSGYRSPATNARNSLHMRGMAADLRLKSR---SVGQIYSAALSCHAGGVKYARSDFVHMD---
-----------------------------------------------------NQTIAMDPKLLDLMFQMQHRLHTN---ETIQVICGYRCPKTNASHSMHMEGKAIDLDLASK---PLARIRQVAVELKQGGVYYPKSNFVHVD---
-----------------------------------------------------NETHKMDKDLFDQISLIQTELGVE---AEVQIISGYRSPATNAKKSYHMLGQAIDFRLDGV---SLKKVRDIAREMKVGGVYYPSSNFVHID---
-----------------------------------------------------NESAPMDKRLFDFAYLLKESLGYD---DELHIISGYRSPKTNAKKSYHMKGMALDIAVPGV---KLAEVRSAALALKLGGVYYPNSGFVHID---
-----------------------------------------------------NIAAPMDKRLYDLLFHLQENLKTK---DTIHIISGYRSPQTNAKKSLHMEGKAIDIAIPGI---RLDRLRDAAKELKLGGVYYPQSGFVHVD---
-----------------------------------------------------GQVHPIDPKLFDLMNSVQRKVGGK---GPIHIICGYRSPSTNATQSLHTQGKAVDIRLPGH---ATRHVGRAALSLKAGGVMYPESDFVHID---
-----------------------------------------------------GSIHPIDPVLFDFLHIVNSRLGGR---QPVEIVCGYRSEKSNAKNSLHMIGQAIDIRIPGR---SVAEIAQVAESVQRGGVRYRRSGFVHLD---
-----------------------------------------------------DKQIAIDTRTLDITAAAHALLDST---EPYMLLSGYRSPETNAKNSLHLKGQAADLRLNSR---SVNQIFKAAQACRAGGVKYSGSNFVHMD---
---------------------------------------------------------IPSHEVIAHLKNLCT-WLREKASQPIIINSGYRSPQLNAPTSNHLTGCAVDIRTSGYEQA----LINYAKQQEFDELLIEKGVWLHFAV--
---------------------------------------------------------IPSHEAIANLKRLCT-WLRERTGRSIVINSGYRSPQLNAPTSNHLTGCAADIRTSGMEQA----LLAYARQQEFDELLIEKGVWLHFAV--
---------------------------------------------------------IPSHEAIANLKRLCE-WLREKASHPIIINSGYRSPQFNAPTSNHITGCAVDIRTSGYEQA----IIDYAKNQDYDELLIEKGVWLHFAV--
---------------------------------------------------------IPSHEVIANLKRLCE-WLRKRGEDPIRINSGYRSPQLNAPTSNHLTGCAVDIRTNGMEQA----LIAYANAQDYDELLIEKGVWLHFAV--
---------------------------------------------------------RCRQEHVTALTALVDNVLRTWWGKPITVNSGYRCLELNSKTSQHMKGEAADIDTGDRQQNK--LLFEYIRNLPYDQLIDESNAWVHVSY--
---------------------------------------------------------VPNSFQKANMENLINHLLRQMWGKPIIVNSGFRCIKLNAKNSEHMSGCAADITTGNKADNK--KLFDMIRSLEWRQLIDESGSWIHISY--
---------------------------------------------------------RCKKEHVVNMTALVDNVLREAYRKPITVNSGFRCPALNSATSDHMTGRAADITGGSPKENK--RLFYLIQGLPFKQLIDEKNSWIHVSY--
---------------------------------------------------------RCGSDIEANLTALVDNVLREWYGKPIVVNSGYRCPALNATTSQHMSGQAADIDTGDRQQNK--LLFEHIRNLPFDQLIDESNAWVHVSY--
---------------------------------------------------------YPTAEAIHNLTKLVENVLREKYGKPIRVSSGYRSAILNATSSQHRLGEAADITVGSKEENR--KLFEIIRELPFDQLIDEKDSWVHVSF--
---------------------------------------------------------TPTGEVVHNLTELVENVLREKYGKPIRVSSGYRSAVLNATSSQHRLGQAADITVGSKEGNR--RLFEIIRELPFDQLIDEKDSWVHVSF--
---------------------------------------------------------TPTPNAINNLRLLCENVLRRRFGV-IRITSGYRCEELNKPNSQHLLGQAADIHLSNKEVAM--KMFNFVARLDYDQLLFEHGNWLHVSY--
----------------------------------------------------ANKIKSIDPKLFDQLYRLQGLLGTN---KPVQLVSGYRSLDTNAKHSYHTKGQAMDFHIEGI---SLSNVRKAALSMRAGGVYYPSSNFVHID---
----------------------------------------------------ANKVKAIDPRLFDQLFRLQGLLGTR---KPVQLISGYRSVDTNAKHSYHTKGQAMDFHIEGI---SLSNIRKAALSLRAGGVYYPSSNFVHID---
----------------------------------------------------AAEVTPMDPRLFDVLHSVAERLEAS---EAFVISSGYRTPEHNSTVSLHMSGMAADFRLPGR---DAFGVARMAAQMQVGGVLYRREGFVHLD---
----------------------------------------------------QNRVKTIDPKLFDQIYLLQMMLGVN---KPVQLISGYRSLMTNAKQSYHTLGRAMDFHIEGI---ELSRIRKAALKMKAGGVYYPNSNFIHID---
----------------------------------------------------AGAVVQMNPTLLDILCGVYGWFGIE---RPIVVTSGYRTPATNARNSMHLVGRAADIRVPDV---PTEYLARLGMYLRGGGVYYATKQFVHVD---
----------------------------------------------------AAEVTPMDPRLFDVLSSVATRLEAT---EPFTISSGYRTPEHNSTVSLHMSGMAADFRLQGR---DGYGVARLAAQMQMGGVYY-RQGFVHLD---
----------------------------------------------------SGDIHRIDPRLLDLMQHLHHKTGNS---KEFQVVSGYRSPATNAKNSLHMQGKAIDIRLPGV---PLHVLRRAAMSMHAGGVYYPKSNFIHID---
----------------------------------------------------TGDSTLMDPKLLDLLYRLQSSVGRV---GEFQVISGYRSPKSNAKRSLHMQGKAIDVRLPGT---ELKELRKAALALKAGGVFYPKSNFIHVD---
----------------------------------------------------TGQTHPIDPKLLDILWAIQGEMGRK---GVYEVISGFRSPQTNAGHSLHMQGKAVDIRFPGI---DTDQIHQCAVEMRTGGVYYAKADFVHLD---
----------------------------------------------------TDQAMNMDKQLLLLLNELQQTFGEH---NPIHVISAYRSPKTNAKKSYHMKGQAIDIRIPGV---ELKDLHKASLDLKAGGVLYTRSNFIHLD---
----------------------------------------------------SGQVGVIDPQLFGLLFELRRTLGSE---SPFQVISGYRSPVTNAKHSLHMDGKAIDIRLPGV---ALADLRDAAMSLGVGGVFYAREDFVHVD---
----------------------------------------------------QNEVHPMDRRLFDHLTQIQKLIGTE---NEVQIISGYRSPQTNAKKSYHMLGRAIDFRLDGV---KLSTVRDAALSLEAGGVYYPGSNFVHID---
----------------------------------------------------SNLVHNMDPKLFMKFYQIQSRLGLR---SEISVICGYRAPATNASNSYHMRGQAIDFRIDNV---ALNRVREVAQSLKNGGVYYPRSNFVHVD---
----------------------------------------------------ENKSAPTDPRLLDLLWEIDQNTRSK---NPIYTMSGYRTEKTNDPGSFHMRGMAMDITQDFL---DPEEVYRVARKLGRGGAFYPKTPYVHVD---
----------------------------------------------------RNEVFNIDRKLFDQLFLLQHKLGRK---GEIQLISGYRSPATNAKHSYHTLGQAVDVRIPGV---QLAHLRKAALNLKVGGVYYPSDNFVHLD---
----------------------------------------------------SGEVGSIDPQLFDLLFETRRELGCT---QPFEVISGYRCAATNARQSLHMEGRAIDIRIDGV---PLADVRDAAMSLQAGGVFYPRSKFVHLD---
----------------------------------------------------VNEVAPIDRGLYELVYQLAEKLDYH---KDIHLISGYRSMKTNAKRSYHTKAMAVDIAMPGV---ALSDLRKAALSLQGGGVYYPRSGFVHVD---
----------------------------------------------------RNESVPMDKRLYDLLFKLKESLNVE---QEFNVISGYRSPKTNAKKSYHMKGMAMDIAIEDV---NLSDLRDAAIELKLGGVYYPRSGFIHVD---
----------------------------------------------------RNETIDMDTGLFDQLSAIQKVIGCD---TQVQIISGYRSPATNAKKSLHMLGKAMDFRLEDV---PLIEVRKAALSLKAGGVYYPGSNFVHID---
----------------------------------------------------RNETINMDKRLFDHLMAIQKTIGSN---SQVQLISGYRSPATNAKKSLHMLGRAIDFRLEGV---PLIEVKKAALSLKVGGVYYPKSNFVHID---
----------------------------------------------------RNEVYSMDAALFDQINSIQGLIGSD---AEVQIISGYRSPATNAKKSYHMLGKAIDFRLDGV---KLSEVKKAALSLKAGGVYYPKSNFIHID---
----------------------------------------------------QNVAAPMDKRLFDLLYTLKSTLNTD---KEIHVISGYRSPKTNAKKSYHMQGMAMDIAIPGV---NLKTIRDAALSLKLGGVYYPKSGFVHVD---
----------------------------------------------------QNLAAPMDKRLYDYLFKLQQLVEYQ---DEIHVISAYRSPKTNAKKSYHMKGMAMDIAMPGV---KLAHLKDAAKSLKLGGVFYPSSGFIHVD---
----------------------------------------------------RNEVAKMDKRLFDAITEIQANLGHK---GQVRIISGYRSPATNATKSYHMKGQAIDFNLEGV---SLSKVRKAAIDLQLGGVYYPKSDFVHID---
----------------------------------------------------ANKVKSIDPQLFDHLYRLQGMLGTT---KPVQLISGYRSLGTNAKHSYHTKGQAMDFHIEGI---QLSNIRKAALKMRAGGVYYPRSNFVHID---
----------------------------------------------------REEVTPMDKRLFDHIDGIQNLLGIQ---AEVLLISGYRSPATNAKRSYHTLGQAIDFRLDGV---DLKQVRDAAFELKLGGLYYPGSDFIHID---
----------------------------------------------------TDKIQPIDPQLLDILHVLRTQITEN---QPFHIISGYRSQETNARHSYHILGKAIDIRLPGC---CLPELRDAARKLEMGGVYYPRSEFIHVD---
----------------------------------------------------AGQAVQMSLVTLDILAGIQGWLGIN---SPLHTNSGYRSPLTNAKNSRHMYGMAWDGRVPQV---STESLARFAVYLKGGGVFYQEKNFLHID---
----------------------------------------------------RNEVHPMDKRLFDHISLIQKELSVE---TEVQIISGYRSPATNAKKSYHMLGQAIDFRLDGV---SLKRVRDVSRELKLGGVYYPGSNFVHID---
----------------------------------------------------RNEVHTMDKYLFDQISLIQSELGVE---SEVIVISGYRSPATNAKKSYHMLGQAIDFRLDGV---NLKQVRDAAISLKAGGVYYPRSNFIHID---
----------------------------------------------------AGQAVQMSVVLLDILCGIQGFLGHS---IPLLTTSGYRSPATNVRSSMHIQGRAWDGRMQGV---PADLLARIATYLQGGGVLYQGRGFLHVD---
----------------------------------------------------ADKAVQMSPVLFDILYGMQGFFNQH---RVIVLNSGYRTRLTNVGDSRHMRGEAADIEFPGV---PVNYMGRLALYLQGGGVFYPSRDFVHVD---
----------------------------------------------------RNEVHPMDRRLFDQISQIQKLIGTD---AEVIVISGYRSPLTNAKKSLHMEGKAIDFRLDGV---KLSAVRDAAISLKAGGVYYPSSNFVHID---
------------------VVGPVRQLIYPGTPEFDQLPPNDNEDIFFVQEI----ARHMSPRMAQVLDILAQTVKQLLSGLRLAVRNGYEDPGSETSPELFHEGRMVDLALYNNNAADMGTLAALAWRDRLDWVR-------------
-----------------------------------------------------GFPAEPQPGLVSRIEALRQAV-----GAPVIITSGVRCEERNVAWSFHKRGAAADLYSP---GVPVGTLAALAKDCGLNVLPYYSSGYVHVEI--
-----------------------------------------------------GWPCEMNPALLDKIEALRCAC-----GSPVIITSGVRCEARNVPWSFHKRGDAADLYCP---GVPVGDLAQMAQALGMNVLPYYGSGYLHVEI--
-----------------------------------------------------GWPCAMRPELLEKIEALRCYF-----GRPVIITSGVRCEARNVSWSFHTRGCAADLYCP---GIGVGDLAQTAKELGMNVLPYYSSGYIHVEI--
-----------------------------------------------------GWPARMNPVLLERIEALREYY-----GLPVVITSGVRCEGRNVAWSFHKRGDAADLYCP---GVAVGDLAQTAKDLGMNVLPYYASGYIHVEV--
-----------------------------------------------------GWPAEMAPELLEKIEALRYAF-----DRPVIITSGVRCDQRNIPNSWHCFGHAADLYCP---GIPYTEVARVARTLGLGVIEYPGQAFDHVEI--
-----------------------------------------------------------------------------------KLASNYRCPPHNAVNSLHLKGMAADIRVLEM---TAKEITHLAEKAGFDGILYPSQCFVHVDVR-
------------------------------------------------------------EEVVTNLSALCTHVLEPLRRGRVIVTSGYRCEALNALRSQHLKGEAADIHVTGLEM--CRKYVAVLRHTDFDQMILARKRWIHVSYRR
------------------------------------------------------------EEVMANLHALCEQVLEPLRRGRVIITSGYRCEALNARHSQHQRGEAADIHVTGTEM--CRKYAAILRRTPFDQLILAKKRWIHVSYRR
------------------------------------------------------------DDVIENLKALAVNVLEPLRQGRVIITSGYRCQQLNVANSQHLRGEAADIHFTTPEI--RDKYIRVLLKTNFDQIILPVKRWIHVSYKR
------------------------------------------------------------TEVVENMKALAMNVLEPLRQGRVIITSGFRCKQLNVANSQHLRGEAADIHITTREM--LDKYTRILLKTDFDQLILPIKRWIHVSYKR
------------------------------------------------------------AEVIENLRELCRCVLEPLRRGRVIVVGGYRCEAVNAEHSQHLRGEAADIHVTGLEM--CRKYAAILSQTDFDQMILIKKRWIHISYRR
------------------------------------------------------------TVEVERLRALCVNVLEPLRHGVLRITSGYRSERVNAVRSQHRLGEAADIHVSSLEV--AQKMCHFVEHLDFDQLILTGARWIHVSYKA
------------------------------------------------------------ATQIARLKALCEHVLEPLRNGAIRITSGFRSERLNNSLSQHTFGEAADIYVPNRER--GLEMFHFIQHCTFDQLLLTPSFWLHVSYKS
------------------------------------------------------------PEVVNNLRLLCEHVLEPLRRGVIRITSGYRCEQLNKPNSQHLLGQAADIHLSNKEV--AMKMFHFVENLDYDQLIFDGARWLHVSYRA
------------------------------------------------------------PCACLALQNLAVGLLEPLRLGPIAILSGYRNEKVNVVTSQHLKGEAADCYVAGPEK--LLDVLQC-SGLVFDQAILGRRRFLHLSLKI
------------------------------------------------------------QTVISNLQALCTKVLEPLRRGKVIVTSGYRCTALNSEQSQHLSGEAADIYVSGRQM--CQKYVNILQHTDFDQMILHLKRWIHISHRR
------------------------------------------------------------EELLENLQRLCEEVLEPLRLGRILITSGYRCRELNVFNSQHLRGEAADIFVSSTEM--AMRYAEFLKHTNVGQILLKHKRWLHVGVGR
------------------------------------------------------------EEVVENLRALCTEVLEPLRRGRVIITSGYRCQKLNVWNSQHLRGEAADIYVSDTAM--AMRYGRIFEHSAVQQVLLKRKRWLHVTFRR
------------------------------------------------------------EEVVENLRALCTEVLEPLRQGRVIITSGYRCKKLNVWNSQHLKGEAADIFVPNTET--AMRYGNILQHSAVQQLLLKQKRWIHVGYKR
------------------------------------------------------------AEVMENLRALCTEVLEPLRRGRVIVTSGYRCQELNVWNSQHLKGEAADIFVPDTAT--AMRYGHILRHSAVQQLLLQQKRWIHVGFRR
------------------------------------------------------------DRQRQALRALAENVLEPLRRGPIVVSSGFRSPAVNVPGSQHLRGEAADIVIGDVDR--GLHICHHIRHLDFDQLIFPTPRWLHVSYTA
------------------------------------------------------------LQQEQALRNLALNILEPLRRGPVIVSSGFRTRQVNAPASQHTRGEAADIVINNTDR--GLRMYSFITRLDFDQLILPTPRWLHVSYTT
------------------------------------------------------------KSLICNVNGLIDNVLDPLREGPVTVTSGYRCEVLNSKTSEHMKGMAADIV--TKEE--NKRLFNLI----LEIPFTKNFSWVHVSYDS
----------------------------------------------------FAE---MDPVVLDILYAYS---GVTR---PLMVTSGFRHFISNALASWHPKAGAVDFYVPGV---PVEQTARFGQWLAGGGVLYLKKNFTHVD---
----------------------------------------------------SDSVIGIDRRTIDIMAASHNLLDTTE---PYMMLSGYRSPKTNAKNSLHMKGQAADLRLSNR---STGQIAKAAKSCASGGVRYSRSNFVHMD---
----------------------------------------------------NNEMIGIDTRTIDILTATHRLVDVNR---PYMLLSGFRSPQTNARDSLHMRGQAVDVRLEGR---SVSQVASAAERCSAGGVRYSGSNFVHMD---
----------------------------------------------------VDQVKSMDLRTVDIMAAAHNLMDVNE---PYMLLSGYRSPQTNARNSLHMQGQAADLRLASR---SVSQMANAAIACRAGGVKYYRSNFVHMD---
----------------------------------------------------TGDRFAIDPDLFDILHQLQYRLRTNQ---EFHVVSAYRSPATNAKNSMHTHGKAIDIRLPGR---KLSDLRAAALSLQMGGVYYPSSNFIHLD---
----------------------------------------------------TGDIYEMDRELMDLLTDLAASVRPMG---TFNIISGYRSPKTNARKSYHMRGMAIDVSMDGV---DLMDLHKAALRLKRGGVDYPRSGFIHVD---
----------------------------------------------------SGEVGNMDPQLFDLLHTLRHTLGCSA---PFQVISAYRCPATNARRSLHMDGKAMDVRIEGV---ALADLRDAALSLQLGGVYYPREQFVHVD---
----------------------------------------------------TDGVKSMDLRTIDIMSAAHNLMDANE---PYMLLSGYRSPQTNAKNSLHVKGQAADLRLSTR---SVSQMARAAAACNGGGVKYSRSNFVHMD---
----------------------------------------------------RDAVAPIDRRTIDIMAAAHNMLDVDE---PYLLLSGYRSPQTNAKNSLHMQGQAADLRLGSR---SVRQIAAAAAACKAGGVKYSGSQFVHMD---
----------------------------------------------------TNDVKHIDARTIDIMTAAHNLMDTTE---PYMLISGYRSPKTNAKNSRHLKGEAADLHMNSR---SVNQIAKAAQACRAGGVRYTSSSFVHMD---
----------------------------------------------------TDQSTNIDLRTIDIMAASHNLLEVNE---PYMLLSGYRSPQTNAKNSLHMKGQAADLRLASR---SVSQMAKAAMACRAGGVQYYRSNFVHMD---
----------------------------------------------------NGQTLQIDTRTIDIAAATQNLLDSSQ---PYTLISGYRSPQTNARNSLHLQGQAADLRMQGR---SVSQMARAAASCNAGGVRYSGSNFIHID---
----------------------------------------------------TGEAVQMDLRTIDIMSAALNLMDTTE---PYLLLSGYRSPRTNARNSLHMRGQAADLRLTGR---STAQMANAALACRAGGVRYNGSNFVHMD---
----------------------------------------------------NDQVKAIDLRTIDIMAASSNLLEVNE---PYLLLSGYRSPQTNAKNSLHMKGQAADLRLSTR---TVSQMAQAAQACKAGGVRYYGSNFVHMD---
----------------------------------------------------TNDVIRIDARTVDIMAASHRLMDVSE---PYMLLSGYRCPKTNARNSLHLKGQAADLRLKSR---SVGQMAKAAEACASGGVRYSRSDFVHMD---
----------------------------------------------------NNKMMNIDARTVDIMAAAHNLLDTSE---PYMLLSGYRSPETNARNSLHLKGQAADLRLRSR---SVGQMFKAASACKAGGVRYSSSDFVHMD---
----------------------------------------------------ANEVHPIDPDLLDTLDALQQRLDTQA---TFEVISGYRSPETNAVYSLHMEGEAIDIRVPGR---DLSQVRDAALSLQKGGVYYPRSQFVHVD---
----------------------------------------------------TGDIGVIDPQVFDLLHSVQQALGSKG---AFEVISGYRCPATNATKSLHMEGRAIDIRLPGV---PLADLHQAALSLRAGGVFYPREQFVHLD---
----------------------------------------------------TGQAIGFDPRAIDIAAASHRLLQTNE---PYMMLSGYRSPQTNARNSLHMVGKAADLRLKSR---SVSQMYKAAAACNAGGVKYSRSNFVHMD---
----------------------------------------------------TGTVGQIDPALFDLLHALRLQLRQTT---AFEVISCYRCPETNATRSLHMDGKAIDIRIPGV---ALADLRDAALGLQLGGVYYAREQFVHVD---
----------------------------------------------------TGSVGVIDPQLFELLHRVRSLLGTES---AFEVISGYRCPETNARHSLHMDGRAIDVRLAGV---PLKELRDAAMTLQAGGVYYEQDRFVHLD---
----------------------------------------------------SDDCHRIDLALLNRLSEMQEKLDSQH---PFEVLSAFRSQETNATGSLHLQGRAVDLRLQGR---RAVDLYRCALSFGDGGACYAKRNFVHVD---
----------------------------------------------------RNASTDMDPRLYDQLAAIYDFVDARN---PITMVSGYRSPVTNAKKSYHMTGQAIDFFIEDV---PLSKLRKAAVELQAGGVYYPKSGFIHVD---
----------------------------------------------------ENEVASLDLALIDQLHHVQSKLETNR---EIMLVSGYRSPKTNAQESLHMMGKALDFYIPGI---NHRHVHKATLAVSTGGVYYRKSGFIHLD---
----------------------------------------------------TDGIKSIDLRTIDIMAAAHNLMDVNE---PYMLLSGYRSPKTNAKNSLHMRGQAADVRLASR---SVNQMAKAAVACRGGGVRYSGSNFVHMD---
----------------------------------------------------TDEVTNMDLRTVDIMAASHNLLDVNE---PYMLLSGYRSPKTNAKNSLHLKGQAADLRLASR---SVHQVARAAVACGGGGVRYSGSNFTHMD---
----------------------------------------------------NSKIKAVDARTVDILAASHGLMDVDE---PYMLLSGYRSPETNAKNSLHLQAQAADLRLKSR---SIQQMAAAAKSCSAGGVTYRGSNFVHMD---
----------------------------------------------------NDQVKSMDLRTIDIMAAAHNMLDVSE---PYMLLSGYRSPKTNAKNSLHMKGQAADLRLSSR---SVSQMARAAMSCRAGGVQYYRSNFVHMD---
--------------------------------------------------------------------------------RP-VVSSWFRSARGGSSSSGHRKGMAV-IILK-KGS--KKEYERVKNLESFDQLYYP-RGHLHIGFK-
--------------------------------------------------------------------------------RP-QVSSWYRSSRGGSDSSAHQNGLAV-IILK-KGS--YKEFQKIMKSLSYDQLYYP-RGHLHIGFR-
--------------------------------------------------------------------------------CPMHISSGYRCEEGGVADSQHLKGEAADIYTFG---SC-RLLEALQQRLNFDQAYYRRRGFIHLSLK-
--------------------------------------------------------------------------------RVI-VVGGYRCEAHGAEHSQHLRGEAADIHVTGL--EMCRKYAAILAT-DFDQMLEPQKRWIHVSYK-
--------------------------------------------------------------------------------EPITINSGYRTAEKGVYGSQHIKGEAADIRISGD--AM-RVVSAVLKGIPYDQCFYTRRNFVHVSYS-
--------------------------------------------------------------------------------WSDLITSGYRSKEGGVDTSQHCRGEAADRYIVP-----IEVIRTLLAGLDFDQAVYPS--FVHLSYT-
--------------------------------------------------------------------------------QPMYIMSGYRSEEGGAPSSQHMKGEAVDIYTVD---RN-RLLEDLVARLNFDQALYRTKGFIHLSLK-
--------------------------------------------------------------------------------APIAILSGYRNEKGGVVTSQHLKGEAADCYVADG--PE-KLLDVLQCGLVFDQALYRRKRFLHLSLK-
--------------------------------------------------------------------------------KPIAITSGYRSPEGGVPSSLHVKGEAADCYVPD---PK-ELLDVLLYKLPFDQALYKRKKFLHLSFR-
