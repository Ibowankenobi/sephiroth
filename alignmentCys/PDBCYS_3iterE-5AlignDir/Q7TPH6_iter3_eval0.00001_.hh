SLEDYSVVNRFESHGGGWGYSAHSVEAIRFSADTDILLGGLGLFGGRGEYTAKIKLFELGPDGGDHETDGDLLAETDVLAYDCAAREKYA-FDEPVLLQAGWWYVAWARVSGPSSDCGSHGQASITTDDGVIFQFKSSKKSNNGTDVNAGQIPQLLYRLP
TREDFSIVNRFESHGGGWGYSGHSVEAIRVMADTDILLGGVGLFGGRGEYTALIRIFDVGTDGGDQETDGEMLTETEEIPYECGARQKYPLFEEPILLQANRWYVVWAHVSGPSSDCGSSGQSVVTTEDQVVFYFKSSKKSNNGTDVNAGQIPQLLYRVI
TKEDFSIVNRFEGYGGGWGYSGHSVEAVRFCPDTDILLGGFGLFGGRGEYSAKIKVYDLGPKGGEYELDGEWMAESDDITFDCGAREKFPLFEEPVPLTAGNWYVAWARVNGPSSDCGSSGQGTVTTEDQISFQFMSSKKSNNGTDINAGQIPQILYRMP
AQGEYQVVNRFDNFGGGWGYSGHSVEAIRFAADTDIVICGFGMFGGRGEYSCKLKLYDLGSDGGGYEKEGTLISETKEVPYECAARSKHHLLPKPLNAAAGRWYLVWARIAGPSSDCGSCGQASVTTEDQVVFSFKSSKKANNGTDVNSGQIPAILYRLV
TKEDFSVVNRFESHGGGWGYSGHSVEAIRFMSDTDILLGGFSLFGGRGEYMGKIKVFDLGIGGGDQEGDGELLAETDEVVYECGARQRFPLFSQPLRLQANRWYLAWARVNGPSSDCGSGGQSTVTTEDQVTFTFKPSKKSNNGTDVNAGQLPQILFRVI
SREDFVTVNRFESHGGGWGYSAHSIEAIRFMPDTDILLGGYGLFGGRGEYTAKIKLFDIGMDGGDQENDGELLAESEEIPYECGPRQKYSLFYEPIPLQANRWYVAWAKVSGPSSDCGSGGQGMVTAEDQVMFYFKSSKRSNNGTDVNAGQIPQLLYRIV
VVEDYQTVSRFENHGGGWGYSGHSVEAIRFMCDCDILLGGVGLYGGRGEYTAKIRLYDIGPDGGDQEGDGEVIFESDEVFYECPSKDRFPMFDSPVPLIAGRWYVAAATINGPSSDCGSAGQAMVI--DDVGFHFKTSKKSNNGTDVNAGQLPCLLYNV-
MKEDFLTVNRFESHGGGWGYSGHSVEAIRFMCDTDILLGGYGLFGGRGDYTAKIKLYDIGLDGGDQEADGELLAESDEIVFECAPRDRFPLFETPVPIVANRWYLAWACMNGPSSDCGSSGQAMVI--DEVGFHFKSSKKSNNGTDVNAGQIPCLLYNI-
PKKEFLHINRFASLGGGWGYSGHSVEAIRFSVDCNIALVGFGLFGGRGEYTVKLKIYDIGMEGGERETDGELLAETDELTYECAPRQKNPLLDDPITLFANRWYVACARITGPSSDCGSSGMCSVTSDEQITFTFKSSRRSNNGTDINAGQIPEILYRT-
---------------------------------------------------------------------------------------------------AHRWYVAWCRISGPSSDCGSSGQAMVTTEDQVLFYFKSSKKSNNGTDVNAGQIPQLLYKV-
TKDDFTMVNRFESHGGGWGYSGHSIEAVRFMSDTDILLGGFGLFGGRGEYYGRIKLFDLGPDGGDNEGDGELIGETDEVAFECGAREKYALFDEPIPIHANSWYVAWARISGPSSDCGSTGQSVVTTDDQVIFKFKSSKKSNNGTDVNAGQIPQLLYRLP
TLQDFSAVNRFESHGGGWGYSGHSVEAIRFMADADIILGGFGLFGGRGEYTAKIKLLDIGVDGGEQESDGEVIAETDDIPYECGPRQKYPLFEEPLLLQGNRWYIAWARISGPSSDCGSNGQAVIVTEDQISFYFKSSKKSNNGTDVNAGQIPQLLYKVI
SKEDYNAVNRFDSHGGGWGYSVHSVEAIRFTADADIILGGFGLFGGRGEYTAKIKLFDLGTEGGEHEGDGDLLCETDDMTYECGSREKYALFEEPILLTANNWYVGWARVSGPSSDCGASGQTMVTTDDQLP----------------------------
---EYNTVCRFENFGGGWGYSSYSVEAIRFMCDSDVIIAGFGMYGGRGEYTCKLKLFDQGYDGGANEKDGVLISEVEEVPFECPSKCKFNTLPSPVTISAKRWYLVLAKISGPSSDCGSSGQSSVTTSDNVVFHFKTSKKANNGTNVNSGQIPSILYRTV
--DGYSVANRFDGTGGGWGYSAHSVEAIQFKVSKEIRLVGVGLYGGRGEYISKLKLYRQIGNEA-DELYVEQITETDETMYDCGAHETATLFSQPIVIQPNHWHVVSAKISGPSSDCGANGKHHVEC-DGVTFQFRNSVVSNNGTDVNVGQIPELYYQV-
--NGYVRVNRFDGYGGGWGYSAHSVEAIQFRASRDIRLLGIGLFGGRGEYIAKLKLFRLVGAEF-DEKSVELVAESDEMLYECAPREKAHMFPRAVIANANHWHVICAQVNGPSSDCGASGRANVIAE-GVQFFFRNSRMSNNGTDVNVGQIPELLY---
----YQRINRFDGYGGGWGYSAHSVEAIQFRTSRDIRFFGVGLYGGRGEYIAKLKLYRLCPPD-YNEQNVELLSESDEMLYECAAHEKAQLFSRCVLIKSNLWHVICAQINGPSSDCGSNGQSNVIGDDGVQFFFRNSRMSNNGTDVNVGQIPEFLYI--
------------------------------MCDADVLLGGIGVFGGRGEYSVTVCVYEEVGD-AEAPGDGDVLVETDRIPYECGSREKFHIFNEPIPVTAGQWYTVTAKVTGPSSDCGSHGEQNVTTSDNIKFTFRRSKRSNNGTDVSAGQIPELLYCLP
-VDDYHIVSRFNSHGGGWGYNAGSIEAILFSPDQDILLGGFGLYGGRGQYNVEVKVLEVGDSP--DEGEGTLLVSAEEKGYTCERNKTFRLLERPVVLLAYHWYAVHCMISGASTDAGSSGLGETTGPDKVVFKFKGSRKSNNGTDVSSGQIPEILYRIP
------QVNRFDSFGGGWGYSNQSLDAISYRTDRDVLVHGIGVFGGRGRYTAQLKLIEQEKATDRHEHDDDQLFD-----YSTLAETKIQQF----------------------HYCGADGRSFVKADYGVGFKFRTSELSNNGTDVRFGQIPSILYTL-
GLEDVTHVSRYSSQGGGWGYGGSSPDAIGFKVNQPIAICGFGLYGQDSSFNATMKLFV---DGKA----EEPLATKTTYSSNCPGRNYAALFDEPVTVEPHQLYIAWALIQSPSSICGSSGQSVVE-EDGVQFNFVSTHHGSNGTDVSSGQVPAILFC--
SPEDFMVFERFGSEGGGWGYGGGSPDAISFTCSDEAYLCGVGLFGSQGTFSAQFKVFP---EEDT----SNVLFESFDYELGV-GEKYYSVLDEPLKLEAGPRYAVYVKIRSASSACGSSGKSTVA-IEGLTVTFQASTHCNNGTDVGSGQFPALMLV--
-------VKRFATLS-----------ESKFWVDRQILVSGFGIFG-SAAWQTEVEI----------------------------T---KP-FRKPVPVLPDVSYTAEAV-----TYSGAKGIEVATVPEKINFHFESSSRDSNDVPHCEGQLPEIHFF--
-------INRFQQVESRWGYSGTS-DRIRFNVNRRISIVGFGLYGGPTDYQVNIQIIESD--------KRITLGQ-NETGFSCDGTANFRMFKEPVEILPNVSYTACATLKGPDSHYGTKGLKKVTQEAKTTFFFFSSPGNNNGTSVEDGQIPEIIYY--
-------VCRFRDVAERWGYSGTS-DRIRFTVSRKIYIVGFGLYGGPADFQANIQVISVD--------NGIVLGQ-NDTGFSCDGTDKFRMFKQPIEIKANQSYVACATLNGHDSFYGTRGHRRVIKDGQTVFIFTFAPGNNNGTSVEDGQIPELIYY--
-------VKPFPVVF-----------ITNFEVDREILVIGFGIM--SEALQATVEI----------------------------T---KS-FKKPVTVPPNTSYVANAD-----IYRGKRGIETATVPEKVNFRFQNLQENNHDCG-CKGLPLEIHFF--
--------TRFMSDTSEWNCEGTNVDSIKFSVDRDIRAVGVGLYGNPADYCVDLELLDSH--------NDSLRAF-EGTYFP-EKPEKHDIFDEPVLLEAHESYTIRVLLKGLPTHRGTEGKRDVLCEAGVRFRFDVDEASFNGTTTADGQIPEVIFK--
-------INRFAKVESRWGYSGTS-DRIRFSCSRRIFVVGLGLYGGPSDYQVTVQLIQTD--------TNEILAH-NETAFNSDGNDSFRMFKEPVETKPNVSYTASATLRGPDSHYGTSGQKKVVYDEKLTFNFSYATGNNNGTSVEDGQIPEIIFY--
--------HRFQSSAYRWRYRGRC-DSIQFAVDKRIFIAGLGLYGGKAEYSVKIEL--KR--------QGVVLAQ-NLTKFVSDGCSNFSWFEHPVQVEQDTFYTVSAILDGNESYFGQEGMTEVQ--GKVTFQFQCSSDSTNGTGVQGGQIPELIFY--
-------CIRYGAVYESWYC-GEP-DAISFTCNEKISLH-LLVYGSEGVYDVTCSLTMD----------IEKVTR-KT-RLKTESQHTYP---TPVDIGPKKKYSLVVKLDGLDTYQGKNGQSTVTCS-GVRFSFSKSKFSRN--------IPGILF---
-------FMQLDTKLK-----GRP-DAIMFETSKPVWFH-VVVYGSQSI-NIHVKIA-R----------LTIIRELGQ-TFESESEEMHD---TPFETVPGSRYNVVVTIKFRHLFRGIAGKRSLTFK-DSEITFYESGCSSN--------IPGFLL---
-------CTRLGALEKRWRT-GEL-EKLDFKVSKKIEIH-IVVYGGKSNYKVSAWLHDT----------DEVIADCHS-KLTTPEGITYR---KPAIVQANKRYCIHLQMKGAKTYWSEEGTTNVTCH-DVVFTFLDSQEYGN--------IPGIIF---
-------CSRFKSCCGDWFDTRKH-DGIKFMTDRRMYIKGVGLYGAEGDYKTTVELKEQ----------DRILAQTKA-TYSSTHDLVCDLFDESIAIEKNIVYTITVLLKGPCSLSGTNGQREVLVE-GAKFTFLEYTSPNG-TSLSAGQIPEIIF---
-------CHRFQSSAYQWRYRGRC-DSIQFCVDKRIFIVGFGLYGSSADYSVRIELKRL----------GRVLAENHA-KFFSGSSNTFHHFEQPIQVEPDTFYTASAILDGSESYFGQEGLSEVNVS-NVTFQFQCSSESTNGTGVQGGQIPELIF---
-------VTRFMSKEDGWSYSGGE-DSLCIKVSNQVRLVAVSLFGSGNTYTVELAVKEG----------QQVYQQEKR-TFSSELMNPTNSFDHPVTILPNTKYCLRAWINGPNSYYGNNGQSVVKCE-DVEFTFMGSATSSS-----------------
-------CHRFQSSAYQWRYRGRC-DSIQFAVDKRIFIAGLGLYGSKAEYSAKIELKRQ----------GVTLAQNLT-KFVSGSSSTFPGFEHPVQVEQDAFYTVSVVLDGNESYFGQEGMTEVQCG-KVTFQFQCSSDSTNGTGVQGGQIPELVF---
-------CHRFQSTAYQWRYRGRC-DSIQFMADKRIFIAGYGLYGSAANYKANIKLSHN----------GVVLSEETA-GFFSGCSKTFSWFKNPVQCEPGLFYTASAILDGNESYFGQEGLSEVVCD-GITIQFQCSIDSTNGTGVQGGQIPELVF---
-------CHRFQSSAYQWRYRGRC-DSIQFAVDKRIFMAGFGLYGSAADYTVKIEIKQS----------GKVLAENTV-KFFSGSSNTFPWFKHPVQIDPDTFYTASAILDGAESYFGQEGVSEITLG-TVTFQFQCSSESTNGTGVQGGQIPELIF---
-------QSRFCQIES-WGYSGTS-DRIRFMVNRRIFVVGFGLYGSPTEYQVNIQVSDS----------LKVMGQNDT-TFQCGTNNTFRMFKEPVEILPNISYTACATLKGPDSYYGAKGLRKVTHESKVTFQFTYASGNNNGTSVEDGQIPEIIF---
-------QNRFQQVES-WGYSGTS-DRIRFVVNRHIFVVGFGLYGSPTDYQVNIQITDS----------GTVLGQNDT-GFSCGSDATFRMFKEPVEIQPNVSYTACATLKGPDSHYGTKGMRKVIHESKVIFQFTYAAGNNNGTSVEDGQIPEIIF---
-------VTRFTSVSLPSYRTYNE-HSICFEVNTEIELSAVILFGSGNTYK---------------------------------------TFDQRVVIHPNTTYTLRAWINGPKSLCWQTGRRVVRNG-DVEITFMDSPSSENRTRVEGVQFPVFHF---
-------VSRFQRVEG-WGYSGTP-DRIKFTVDRRIYVIGFGLHGSPYEYQVTIQICGT----------GKILASNDT-SFSCGSCATFRLFKEPVEISPCVTYIASARLKGPDSHYGTKGLRRIVHQSVITFQFTYAAGNNNGTSVEDGQIPEIIF---
-------CNRFQQVES-WGYSGTS-DRVRFSVNKRIFVVGFGLYGSPTDYQVNIQITDS----------NTVLGQNDT-GFSCGSASTFRMFKEPVEVLPSVNYTACATLKGPDSHYGTKGLRKVTHESKTCFTFCYAAGNNNGTSVEDGQIPEVIF---
-------CRRPAVCTA-WGPVALG-DRVRLLVNSRTVWGGLPVYHKPRVTSTKITITDS----------NTVLGQNDT-GFSCGSASTFRMFKEPVEVLPNVNYTACATLKG-DSHYGTKGLRKVTHESKTCF-FCYAAGNNNGMSV-TGQIPELIF---
-------CARFQSTERFLYSGKRA-ETVAFTVSASVRLHGVRLYGDHACYRVNLELFTM----------SPEHKHTKN-GCYNELIQTDSKFDTPVMLKAEIEYQLSALIVGPPSCYG------------------------------------------
-------CHRFQSSAYQWRYRGRC-DSIQFAVDRRVFIAGLGLYGSKAEYSVKIELKRL----------GVVLAQNLT-KFMSGSSNTFPWFEHPVQVEQDTFYTASAVLDGSESYFGQEGMTEVQCG-KVAFQFQCSSDSTNGTGVQGGQIPELIF---
-------QARFQQTES-WGYSGTR-TASGFTVDRKIFVLGFGLYGSPSDYDVNIQITGT----------GKVLGSHDT-SFNSGSSAMFRLFKSPVEVAPNINYTACATLKGPDSHYGTKGLRKITLDCRVTFQFTYAAGNNNGTSVEDGQIPEIIF---
-------QSRFQQIES-WGYSGPC-DRIRFVVTRKISLWALGSMAVQLSIQSTFRSRIT----------GKVCGTNDT-TFQCGSGSTFRMFKEPVEILPNQNYTACATLRGPDSYYGTNGLRKVTHEAKTCFTFCYAAGNNNGTSVEDGQIPEVIF---
-------CHRFQSSAYQWRYRGRC-DSIQFAVDRRIFVAGLGLYGSRARYGVRIELKRL----------GVPLAQNLT-EFVSGSSNTFAWFEHPVQVEQDTFYNVSAILDGNESYFGQE----------------------------------------
------TVCRFQHTKSRWGYFGTS-DRIRFSVDRRIFLIGYGVYGEPAIYEVSVELIHTA--------SGKVIAT-NATSFSCDGKYTYRMFKELAEILPNTIYTASATFKGPCSYYGTKGLRTVLGNTKVKFEFSSELGDNNGTSTDEGQIPELIF---
------TVCRFQQTENRWGYNGNS-DRIRFNVDRRIFLVGYGVYGGPAEYDVVIELIHTA--------SGKVIAK-NSTSFSSDGNYTYRIFKEPVEILQNTNYTASATFDGPDSHYGTKGLKRVTKHGQVKFLFNYVAGDNNGTSIEGGQIPELIF---
------TVCRFQHTKSGWGYNGTS-DRIRFSVDRRIFLIGYGVYGGPAIYEVLIELIHTA--------SGKVIAT-NSTNFSCDGKYTYRMFKELAEILPNTIYTASATFKGPYSHYGSKGLKTAMGNGKVKFEFSIETGDNNGTSSDEGQIPELIF---
------VVARFQQTESRWGYSGTS-DRIRFAVDRKIFVLGYGLYGGPSDYDVNIQLVQTG--------SGQVLGS-HDTSFNSDGPARFRLFKSPVEVAPNVNYTACATLKGPDSHYGTKGLRKVTRGSKVTFQFTYAAGNNNGTSVEDGQIPEIIF---
------NINRFQQVESRWGYSGTS-DRIRFSVNRRIFIVGFALYGGPTDYQVNIQIIHTD--------SNTVLGQ-NDTGFSCDGSSTFRMFKEPVEILPNVNYTACTTLKGPDSHYGTKGLRKVITGTKTCFTFCYAAGNNNGTSVEDGQIPEIIF---
------VVSRFSEYASRWGYSGTC-DRIRFTVSRKIYVVGLGLYGSSTEYRVMMQIIHSE--------TNNNCGQ-NETSFSPDGPKSFRMFKEPIEIKPNQFYTASATLNGPDSWYGTKGLAQVQGEKKTTFTFFYAACNNNGTSVEDGQIPELIF---
------NVCRFRDVAERWGYSGTS-DRIRFQVNRKIYVVGFGLYGSPADFQANIQIVSVE--------NGVVLGQ-NDTGFSCDGNKTFRMFKQPIEIKANQSYVACATLNKHNSYYRRKGHRRVTDSNGNVFMFLFAPGNNNGTSLDNGIIPELIY---
------SITRFQQVESRWGYSGTS-DRVRFTVSRRITVVGFGLYGGPTDYQVNIQIVEYE--------KNQIVGQ-NDTGFSCDGANIFRMFKESVEILPNVCYTACATLKGPDSHYGTKGLKKVINTSKTCFYFFSSPGNNNGTSVEDGQIPEIIF---
------TVNRFPQTESRWGYSGTT-DRIRFTVGQRIFVVGFGLYGGPSEYEVHLQIIHLA--------TKKICGS-NTTTFCSDGDDTFRMFKEPVEILPNTSYIASAKLKGMDSYYGTKGLRRVTNGEKVVFQFSYAAGNNNGTSVEDGQIPAIIF---
------TLTRFLQVESRWGYSGTS-DRIRFQVNRRIFVVGFGLYGGPADYTVNIQIIHAD--------TEEMMGQ-NDTSFSCDGDSTFRMFKDPIEIQPCENYVASATLKGPDSHYGTKGLRKVVSNGKVMFQFAYVAGNNNGTSVDDGQIPELIF---
--------HRFQSCAY-WRYRGRC-D--QFAVDRRVF---------------------KR--------QGMAMAH-RIIKYFSDGSSTF-WFDYPVQIEPDTFY------DGNESYFGQEGMTEV-QCGKVTFQFQ--------------QIPELIF---
--------HRFQSTAY-WRYRGRC-D--RFMADKRVF---------------------SQ--------NGKVLSE-ETAGFFSDGSKTF-WFKHSVQCEPGIFY------DGNESYFGQEGLSEV-VCDGITIQFQ--------------QIPELIF---
----------------------------------------------------------------------EEVATTDEVLFECPPNDIAPLFTKSILIRANIWHVISAKISGPSSDCGSSGHARVE-ADGVVFNFRNSTLSNNGTDVTVGQIPEMYF---
-------CLRFQTATNQWRYRGRC-DSIQFLVDRRIYVAAYGLYGRSSQYTVKLELKKG----------DEVLESRKAVLTSTGCSEPILHFDHPVQIEPEVKHTASVIMEGKNSFFGQEGMTEVSVANIVNFYFTPSADSKNGTGVQGGQIPIIAF---
-------CHRFQSCSNQWRYRGRC-DSIQFAVDKRVFIAGFGLYGGSAEYSAKIELKRQ----------GVLLGQNLSKYFSDGSSNTFPCFEYPVQIEADTFYTASVVLDGNESYFGQEGMTEVQC-GKVTFQFQCSSDSTNGTGVQGGQIPELIF---
-------CRRFQSCSNQWRYRGRC-DSVQFCADRRVFLAGFGLYGGACEYRSRIELKRG----------GKALAHCDTRFHSDGSSSTFRYFEHPVQIEADTYYTASAVLEGSESYFGQEGMSEVTV-GCVTFQFQSSSDSTNGTGVQGGQIPELVF---
-------CHRFQSCSNQWRYRGRC-DSIQFSVDRRVFIVGFGLYGGASDYTVKIELKRL----------GRILAENNTKFFSDGSSNTFHYFAHPVQVDPELFYTASAILDGAESYFGQEGLSEINA-GCVTFQFQCSSESTNGTGVQGGQIPELIF---
-------CHRFQSCNNQWRYRGRC-DSIQFSVDKRIFVVGFGFYGGAAPYNVKIELKRL----------GRILAEYNTNFFSDGSSNTFHYFENPIQIEPESFYTASAILDGGESYFGQEGLSEVTV-GCVTFQFQSSSESTNGTGVQGGQIPELIF---
-------VHRFQSSSNQWRYRGRC-DSIQFAVDRRIFIAGFGLYGGGAEYKVKIELKRG----------GKSLAENHTKFFSDGSSSTFATFPHPVQVEPHVYYTASATLCGNESYFGQEGLTESTS-NDVTFQFQCSPESTNGTGVQGGQIPELIF---
-------CNRFQQV--RWGYTGTS-DRIRFNVNRRISIVGFGLYGGPTDYQVNIQILESD--------K--TLGQNDTGFSCDGTETTFRMFKEPVEILPNVSYTACATLKGPDSHYGTKGYKMVTQETKTSFFFFSSPGNNNGTSVEDGQIPEIIY---
------ECNRFQQVESRWGYSGTS-DRIRFSVNKRIFVVGFGLYGGPTDYQVNIQIIH-NT----------VLGQNDTGFSCDGSSNTFRMFKEPVEILPNVNYTACATLKGPDSHYGTKGLRKVTHEAKTCFTFCYA-GNNNGTSVEDGQIPEIIFY--
----------------------------DFGLTKRQVCGRAGSVRRKARYSLKIKLKCLGV----------VLAQNLTRFMSDGSSSTFQWFKHLVQVEQDTFYTASTVLDSSESYFGQEGLTEMQY-GKVTLQFQCSSDSTNGTGVQGGQIPELIFY--
------ECNRFQQVESRWGYSGTS-DRIRFTVNRRISIVGFGLYGGPTDYQVNIQIIE-KQ----------TLGQNDTGFSCDGTANTFRMFKEPIEILPNVCYTACATLKGPDSHYGTKGLKKVVHESKTVFFFFSS-GNNNGTSIEDGQIPEIIFY--
------RCHRFQSSANQWRYRGRC-DSIQFAVDKRIFIAGLGLYGGKAEYSVKIELKRQGV----------VLAQNLTKFVSDGCSNTFSWFEHPVQVEQDTFYTVSAILDGNESYFGQEGMTEVQC-GKVTFQFQCSSDSTNGTGVQGGQIPELIFY--
----------------------------QFAADRRVFVAGLGLYGGKAKYSVKIKLKCLGV----------VLAQNLTRFVSNGSSSTFLWFEHPVQVEQDTFYMTSTMLDVSKSYFGQEGLMEVQC-RKVTFQFQCSSNSTNSTRVQGRQIPKLIFY--
------RCHRFQSSANQWCYRGCC-NSIQFVADWRVFVAGLGLYRGKAEYSVKIELTRLGV----------VLAQNLTRFMSDSSSSTFQWFKHPVQVEQDTFYTASAMLDGSESYFGQEGLTEVQC-RKVTFQFQCSSDSTNGTGVQGGQIPELIFY--
------ICHRFQSCANQWRYRGRC-DSIQFCVDKRIFVVGFGLYGGAADYNVKIELKRLGR----------VLAENNTKFFSDGSSNTFHYFENPIQIEPECFYTASAILDGSESYFGQEGLSEVYM-GTVTFQFHCSSESTNGTGVQGGQIPELIYY--
------SINRFQQVESRWGYSGTS-DRVRFSVNKRIFVVGFGLYGGPTDYQVNIQIIHTDSN---------TVLGQNDTGFSCDGASTFRMFKEPVEVLPSVNYTACATLKGPDSHYGTKGLRKVTHEAKTCFTFCYAAGNNNGTSVEDGQIPEVIF---
------CINRFQQVESRWGYSGTS-DRIRFTVNRRISIVGFGLYGGPTDYQVNIQIIEYEKK---------QTLGQNDTGFSCDGANTFRMFKEPIEVLPSVCYTACATLKGPDSHYGTKGLKKVVHESKTVFFFFSSPGNNNGTSIEDGQIPEIIF---
------VVSRFPGIENRWGFCGTS-DRIKFMVDRRIFVVGFGLYGGPHEYKTQIKIIHCGTSK--------TLAEHD-TSFVCDGNSRRVCFKEPVEILPGITYIAAALIRGPDSYYGTKGLRRVNTNDDVTFQFTYAAMNNNGTSVEDGQIPEIIYY--
--------HRFQSSAHQWRYRGRC-DSIQFCTDRRIFVAGFGLYGNAAKYTVRLELKRS----------GQVLAQLDTVFVSDGSSRVFPFFEQPVQIDADVYYTAGMVLEGNESYFGQEGMPEVQ-VHNVTFQFQCSTESTNGTGVQGGQIPEILF---
--------HRFQSCAYQWRYRGRC-DSIQFAVDRRVFIAGFGLYGGSAEYSAKMELKRQ----------GVALAQRLVKFFSDGSSSTFPWFEHPVQIEADAFYTASVVLDGSESYFGQEGMTEVQ-SGKVTFQFQCSSDSTNGTGVQGGQIPELIF---
-----VVVSRFQRVEGRWGYSGT-PDRIKFTVDRKIYVIGFGLHGGPHEYQVTIQILHCGT--------GKVLANND-TSFSCDGSGTFRLFKEPVEITPCVTYIASACLKGPDSHYGTKGLRRIVSSGVVTFQFTYAAGNNNGTSVEDGQIPEIIF---
-------INRFQQVESRWGYSGTS-DRIRFAVNRRLFVVGFGLYGGPTDYQVNIQKIQ---------HNANTLLRKNDPDSCADGSFFFSLFFVCFSLLYRVYGSWCKLIKGPDSHYGTKGLRKVTHESKTCFTFCYAAGNNNGTSVEDGQIPEIIF---
--------SRFSCQNRTWNTGNGSPDAICFSVDRPVLIAGACIYGGGNEWRYEFDILAVGELPQIHRWKPIAEVHGSFGQEDCFSDVAEVKFDRPIAIKENTKYALRLKNHGARTNNGDGGVTQVKGPDGTTFTFSDSSLSFNGTNHTRGQIPQILY---
--------SRFTSQNRGWSTGNGTPDAVCFSVDKPVMIAGFCVYGGVGYYNYELELLEVGPDGNSQRWNSLELVKGSYGHLDLAKDIAEIKFDRPVPVKEGVKYAVRLRNHGNRTVNGDGGMTHVKCPDGVTFSFSLCRLSSNGTSQTRGQIPQILY---
--------NRFTSQGRSWNTGNGSPDAICLTVDKPVVLVGVCVYGGGGIHEYELEVLHPGDSAHSHRWTSLELVKGTYSTDDSPSDIAEIRLDKAVPLKEGVKYAVRLRNYGSRTANGDGGMTTVQCSDGVSFTFSTCSLSSNGTNQTRGQIPQILY---
--------SRFSRQNRTWNTGNGSPDAICFSVDRGVLIAGACIYGGGNEWRYEFDILDDTGAVGELPQIHRWIAEVHFGQEDCFSDVAEVKFDRPIAIKENTKYALRLKNHGARTNNGDGGVTQVKGPDGTTFTFSDSSLSFNGTNHTRGQIPQILY---
--------NRFTKQGRSWNTGNGSPDAICFSVDKGIVVVGFSVYGGGGIHEYELEVLVDDSHAGDSAHSHRWLELVKYTTDDSPSDIAEIRLDKVVPLKENVKYAVRLRNYGSRTANGDGGMTTVQCPDGVTFTFSTCSLSSNGTNQTRGQIPQILY---
--------SRFTSQNRSWNTGNGSPDAICFTVDRNVSVIGVGVFGGGGSYTYELELLELASSIDDAAQTLSWLAVVRYGPEDSINDVAEIKFDRPVPIKENTRYTFRLRNQGGKTHNGDGGVLSIKGPDGTTFNFSACLLSFNGTNHIRGQLPYILY---
--------SRFARQGKSWNTGNGSPDAVCFSVDKGVVV-GAMLYGGGGQHDYELELLDSSEDIGDGSHTQRWLELVKYYPEDCVNDAAEVKFERPVPIKEGVKYAVRVRNHGGRTANGDGGLPTVKSADGVTFTFSSCSLSSNGTNQTRGQIPNILY---
--------SRFTRQTRSWNTGNGSPDAICFSVDRGIAIAGVGIYGGVGMYDYELELLDDQNTGNDPSHTQRWLDFTRFGPDDCVNDIVELKFDKPVPIKENMKYAIRLRNRGGRTSNGDVGLSVVKGPDGTTFTFTACSLSFNGTTQTRGQIPHILY---
-----------------WSTGNGSPDAICFTVDRGVALVGACVFVGSGLYDYTIELLHDMRTSSEESNATHWAADGSFSSGDAHHEMVNIKFNKPVLLKDEIRYALRLCCEGARTASGDCGLPAVTGPDGTTFQFSSCSLSFNGTSIARGQLPCLIYY--
-----------------WNTGNGSPDAICFTVDRGVMLVGVCVYGGPGNYEYSIELLNDVRSSPEEMNPAHWVAHGAFSGAECQHDMVQLKFDRPILLKEEIRYAIRLCNHGGRTANGDCGLPSVKGPDGTTFRFASCSLSFNGTTLARGQIPCLVYY--
-----------------WNTGNGSPDAICFSVDRGILIAGVGIYGGAGVYDYELELLDDQNNTGNDPSHTQWFTRGSFGPDDCVNDIVELKFDKPVLIKENIKYAIRLRNRGGRTSNGDGGLSVVKGADGTTFTFTACSLSFNGTTQTRGQIPHILYY--
-----------------WNAGNGSPDAICFWVDRGIAIAGCGVFAGIGNYEYELELLAEKKQLEFDEPHGNWVTRGSFGPDDAMNYVADLKFDHPIRIKERVKYAIRLRNYGGRTNNGDMGLSCVKGHDNTLFSFNTCSLSLNGTTQSRGQIPYILYY--
-----------------WNTGNGSPDAICFQVDRGISIAGVGVYGGIGHYEYELELLEDQSSSGSESTHTQWITRGSFVPEDFAPDIIEIKFDRAVPIKENIKYAIRLRNHGGRTNNGDGGLNSVKGSDNTTFTFSTCSLSFNGTTLTRGQIPVILYY--
------VCHRFQSCSNQWRYRGR-CDSIQFSVDKRIFVVGFGLYGGAADYNVKIELKRLG----------NVLAENNTKFFSDGSSNTFHYFENPIQIEPECSYTASAILDGGLSFFGQEGMSE-ATVGSVNFQFQCSSESTNGTGVQGGQIPELIFY--
------VCHRFQSCANQWRYRGR-CDSIQFSVDRRIFIVGFGLYGGAANYNVKIELKRLG----------RTLAENDTKFFSDGSSNTFHFFENPIQIEPECCYTASVILDGNESFFGQEGMSDVLM-GNVTFQFQCSSESTNGTGVQGGQIPELIFY--
------VISRFQRIDARWGYNGMP-DRIKFTVDRKIYVTGFGLHGGPYEYACTIQILHCGT--------GKVLAHHD-TSFTCDGTATFKSFREPVEIMNGVTYIASACIKGGDSHYGTKGLRRVVPSGSPTFQFTYAAGNNNGTSVEDGQIPEIYF---
------TVCRFQHTESKWGYSGTS-DRIRFSVDRRIFLIGYGVYGSMATYEASVELIH--------TASGKVIA-TNATSLSCDG-SKYTMFKELAEILPNTVYTASTTFKGPFSYYGTKGLRTVMVDGKVKFEFSNETGDNNGTSIDVGQIPELIF---
-----QVCHRFASCAYQWRYRGRC-DSIQFSVDKRIFIVGFGLYGGAADYDVKIELKRL----------GRVLAENSTKFFSDGSSNTFQFFETPIQIEPECFYTASVVLDGTESFFGQEGMSEVSV-GTVTFQFQCSSESTNGTGVQGGQIPELIFYGP
-------------------DRPRCCLRGRFSVNKRIFVVGFGLYGGPTDYQVNIQIIHTDS--------NTVLGQNDT-GFSCGSASTFRMFKEPVEVLPNVNYTACATLKGPDSHYGTKGLRKVTHEAKTCFTFCYAAGNNNGTSVEDGQIPEVIFYT-
-------------------------------------------------------------------------------FVSDGSCSTFLWFEHPVQVEQDTFYTASAVLDGSESYFGQEGLTKVQC-GKVTFQFQCSSDSTNGTGVQGGQIPELIFYA-
-------ISRFQQVESRWGYSGTS-DRIRFSVNKRIFVVGFGLYGGPTDYQVNIQIIHTDS--------NTVLGQNDT-GISCGSSNTFRINEEPVEVLPNVNYTAC-----------------------------------------------------
-------GARFARCDRTWNTGNFGPDAIAFSVDRGIAIAGAMVYTGSGSYDYQLELLFDTVDL--Q-PQHKWLES-VSGSYDQEADLAEIKFEHPVQIKEHARYALRLCSQGARTCSGDSGLPTVRGPCGTNFHFYACDLSFNGTTPARGQLPCILYYS-
----FSTGSRFGKIGKTWNTGNFGPDAIAFTVDRGISIAGAGVYSGSGSYEYQLELLDSHHSTHSHSHSHRWVLESIVGSYDQTARMAELRFNRAVLLKENHRYALRLCSQGARTLSGDCGQSSLRGPGTVTFRFYPCDLSFNGTTPARGQIPAILYYS-
-------ASRFTSQGRSWNTGNGSPDAICFSVDKGVVVVGFSVYGGGGIHEYELEVLVDDSHAGDATHSHRWLVKGTYTTDDSPSDIAEIRLDKVVPLKV------------------------------------------------------------
------QVERFGNSNSCWSYSGYNIDGISFTCDKDIYVKGIGVYGDKGKCSCTASY-AYGKNG-------GELFTNPTVSTQWGNKHIEILWPNEVYLRRGQPLTVFIKFKGGSTSSGSRGKSTVTVQ-DCKFTFCEGIKSDNGTSVSSGQIPTILFSL-
------------------------------------------------------------------------------------------VVEHPVQVEQDTFTTVSAILDGNELSYGQEGMTEVQC-GKVTFQFQCSSDSTNGTGVQG-----------
------------------GYEDGQ-DELVFTVDKPVQLLGVGLCGTEGGLTVEMELYEVDPE-----DYNRELATCCSQSFTKADGSVLRMFPDPAPLQPGKYYMLSALIKGSESYCCEECLETVIA-GGVTVAFQQ-WESPNGTSEQRGQFPELYIR--
------------EGTNQWRYRGRC-DSIQFIVDRRIFVAGYGLYGGQSRYNVRMELKK------------------------------------------------------------------------------------------------------
-----QRCHGFQPCAYQRHYRVRC-DSILFAVHNRLLLEGVHLYVSSSQH--------------------------------------------------------------------------------------------------------------
-----HSCHRPTACTYQGPYRGRC-DRIQFAVHKRVFIWGFQLYGSSAD---------------------------------------------------------------------------------------------------------------
-----HPCRRPPAGAYQGRYRGLC-DSIQFAVYKRVFIVGFSLYGSSAE---------------------------------------------------------------------------------------------------------------
----------LQQGESRWAY----------SINKSIFVAGFGLCGGPPDYHV-IQIIHVGSTP----------GL-NDRGFADGSTSTFWMFKEPADCSPH--HTACTMLKGLDLSLGTKGLPKVTHEPQDRLYRLLCGQEQQGRSVKDSVISEVIFYT-
--GDLHSFLRFKVVSNGWCQAATNWEAITWRPNRSIMVAGFGTYGGQQNFFVRYKYLLQNVA--SEENVVEVMSS----EI-DDQTKIYPMFEGMLEVPAGTDLTIITRTYGARLYYGYNGTSYKSFDDRDLFEITASNLSSNGTSVDSGQIPSLYYYVV
----------------DWNSNGEM-DAIDFEANKAFQLHGILLFGSSGSYTYVVEVRISITDG------NRCLLHI--PPISVGKNELFEKFDKPCHIAPSQRYQISVKMKGQKSFSAYFCDSYTQC--DFTIKFFDSAYTGNGTCTSYGQIPGLVCFIP
-PEGKNVLYRFANVLAGKSYPKHCLDSLTLSVSKNIALHGIYIFNSPIQFRVSMKITENAN------GQQEVFSVTNRTVFTNGESRVFLDIIPPVDIQAGMKYDMVMDLDINTTFYGVEGLPELKD-NEITFKFMANECGLNGTNHKTGQFPFLVY---
----------------------------------------------EGSYKVNLKILEDG----------AVKAI-PNKEFVSDGRQKYHRIIPPYHFMADVVYTVEMVMKGPTSFYGKSGKEMVTE-EDVTFTFIPNDNGLNGTNTSIGQFPGFVFE--
-------QNRYQLTYEQWNLKET--DAISFRSKKDIWLSGVGLYESTSHLRFKVQMSIGLA-LINCIFSKKVIYED---TFDVLPNIDYMCFNKAIKIEFDQPYTIALKPNQSCSYYGANGKLET-----EEFQFSEPSFSDNSTTINQRVIPHFYY---
-------QNRYQLTYEQWNLKET--DAICLRSKRDIWLSGVGLYEPDSELTFQVQIFEG-----SNLNSKKLIYQD---KFVIKNDMEFLYFTKAIKIEYDKPYTIALTPNQSCSYYGANGKFET-----DEFQFEEPTFSDNSTTINQGVVPQFFY---
-------ENRFGQFLDNWTLDGI--NAITFQQNQDIWLIGFGVFESPSPPKLTVCLSEG--------------------------KIQFIYLEKALKLNPKYQYTLSITSDQTYTYAGFNGCVDT-----KDFTYFESQISNNGTNTMMGQFPYLVY---
--------------EEGWVYEELS-DEVVISVSAPCQLMGLGLCGTVGAFTVDAEVAQV--DPHDFSWDERRLGAVSQ-SFSK--ADSHRLLPKPIALHPGAFYMISAIIKGSQSSCCEECLSTVVA-SGVCVTFHP-WESPNGTTESRGQFPELYIRV-
------SCKRFSYIDVKWGRKT---GRIDFYVDIQILLTGLLLYGGHHTNTQSLSVYAAL----EDSDNDIVLTDV-TKQVHVEPEKTFNKFPKSIRLEPDVRYSVFVQKDGWESHWGDAGMASVVTQ-DVNFYFLTTYKSEQ-TNRARGLIPGLLFT--
-----FDVARFTSTSSSWIYEDLV-DSITFAVDRTIYVTGVGLYGDKGSYSAQVAHLYGKAARFRDSPPPNVIYT----QWHNTSRYIHKEFHKAVPLHPGEFLT---------------------T-QQLSNQHLSNALAVIGSPLGTAQLST------
-----HSVGRFNSTSSSWSYDEMM-DAISFKADADIYITGIGLYGDKGSYSARVGVTLGQSASFNSTNSVNV-------SWSNTARYIHRDLPEPFQLGANEIMTVFTKISGPSSSSGYKGKESASA-EGVNFKFMTSRNSDNGTSLNSGQIPT------
---------------------------------------------------------------------------------------------------------IRLRNYGGRTCNGVHGKASVKGPDGTKFNFTHCPQSSNGTTVRRGQLPSILYR--
-----RDVNILRSQSRNWEYRGDV-DAIEFEVNKPVSLCGILLYGNQYSYDVEIKIFSL-----------------------------------------------------------------------------------------------------
----YRRVLRFRKTLRERQVSDEK-NDIAFESSVPVTLHGVTVYTPKE--QCETQIVCIDV----FDHNKSSIASVDSHYFEHSSRKVHDFLDQPIRIAPNKRYTISTKTSWVKSFYGTRGRDCVQFRDG-TISFYNAALSESSTTVDKGQIAGLLL---
-----QTPNRFRRTAASWDVSNGACDAIALSVDVGITLHGVGVYCAHHSFICEVLL--NGGDAAHEQWNEKVVGTLANKFEPCQREVSLLRLTKVIKLQPGCTYAIRLQINGGKTFCGEGN---------------------------------------
----------------------------GFKTDSDVRLLGVGLHLG-----AQVKLLRLRCDEDEDASSAEI----DLKNLEDIDNYAGRSLMSPIRIPANVWHVIILNISGANTYDG---RDRVES-AGVVFEFRNSPPST-GTDVGSGQIAGIYFR--
---DIKFFKRFKDFKNNWSVGKDQWDAIMLVPSVDVIIYGVGVFEEGEEFGYKYIIKDS--------SDSQTLTASQIVPHEPEHSHIFRKIPEGIKVGAGQYFNFIQWVSCRKCYYSESGSESIENPDAGIFTVKGSPNSSNSTELNRGIIPGFLYAL-
---DIKFFKRFQHAENGWSSVDGEYDAIILKPLIDVIICGVGIYEEGGELKYKYKITDE--------NDDIVISETSLVPFQPDHKHEFVNYPEGIIVKAGQHFNFMQQISCSESYYSHQGDQTIPNTDMGIFTFQDSQDSSNSTTVESGIIPGFLYIT-
------------------------------------------------------------------------------------------RVARAAALNPGKFYMLSALIKGAESFCCEECLDTVIT-AGVTVHFHP-WESPNGTSEHRGQFPELYIRL-
------SQSRFGKSQQGWVLSET--EALTFEINKDIWLKGFGMYEVNGNLNLDIKLFQGGNT------SGKVLHE-ETIQISRDLNYDHLNLQKPLKLETKQKYTITMKPNRDCSYFGYNGCYQT-----SDFKYYDSILKNNSSGVLLGQFPVFYF---
-----RKFRRFSDVRDSWYIGRERWDALVFQPNQNVRVYGIGLFEPGTSYKYHVEDHEG-NNLFSSPLPNEPLSEENQSILDIESHIIWYKFSQGITVRANQRFNVQTWMSGDRCYYSETGEGNIQNEDMGLFRISDSQYCSNSTRVNRGILPGILYTI-
-------------------------QAATFRTNRDIRLLGIGLYVGEGKSRA----------------------------------------------------------NGTAWDYGINGKHNVTA-GGVAFDFRNSSLPENYTTADMGQIPELYFL--
-----IVCKRFPSPQLNIGQGKK-FDAISFTVSKPVYLCAVRLMGNGSNYMVKLSVCCK----------ETVCSQVAEKTYQAASSYRFDKLDKPLFIEPGVHYTIKAESNA------------------------------------------------
