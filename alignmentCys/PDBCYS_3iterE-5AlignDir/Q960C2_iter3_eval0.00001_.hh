GTQACINAAHTVSGIIGDLDTTI-FATAGTLHSD-SFADHREHILQTAKALVEDTKVLVTGAAGTQDQLANAAQNAVSTITQLAEAVKRGACSLGSTQPDSQV-VINAVKDVASALGDLINCTKLAS-S-QDLKESARV-VLNVSSLLKTVKAVEDEHTRGTRA-EATVEAISQEIRALEH
GTQACINAASTVSGIIGDLDTTIMFATAGTLHAEDTFADHRENILQTAKALVEDTKTLVAGAASSQEQLAVAAQNAVSTIVQLAEVVKYGAASLGSQNPEAQVMLINAVKDVASALGDLIHATKAASPSMAHLKDSAKVMVTNVTSLLKTVKAVEDEHTRGTRALESTIEAIAQEIRALSS
GTQACINASSTVSAIISDLDTTIMFATAGTLHSEGRFSDHREHILKTAKALVEDTKILVAGAAGTQDQLAAAAQNAVTTILQLAEAVKHGAASLGSNQPDSQVMVMNAVKDVAAALGELINATKLASPAMNDLKDSAKVMVMNVTSLLKTVKAVEDEHTRGTRAMEATVDAITQEIRSMQF
GTQACINAASTVSGIIGDLDTTIMFATAGTLHAQDKFVDHRESILKTAKALVEDTKTLVAGAASSQEQLAVAAQNAVTTILQLAEVVKLGAASLGSHNTEAQVLLMNAVKDVASALGDLVQATKAASPAMAYLKDSAKVMVTNVTSLLKTVRAVEDEHARGTRALESTVEAIWQEIRAFDS
GTQACIDAASTVSGIIGDLDTTIMFASAGTLSAEGSFADHRENILKTARALVEDTKTLVSGAADSQEQLAIAAQSAVGTITQLADVVKLGAATLGGDDPDGQVLLINAVKDVASALGDLISSTKMAAPTMLPLKNSAKMMVTNVTSLLKTVKSVEDEATRGTRALEATIDAIVQEARILDN
GTQACINAANTVSGIIGDLDTTIMFATSGSLNSSRKFPAHKDAIVKTAKALVEDTKALVAGAASNQEQLAVAAQNAVRTIVNLSDAVKTGAVSLSSENSETQVLVIHAVRDVAAALTSLIQATKNASPAMGHLKEAAKVMVGNVARLLKTVATVEEKNQQGTRAVEAAVEAIGFEMRQFEH
GTQACITAATAVSGIIADLDTTIMFATAGTLNAEETFADHRENILKTAKALVEDTKLLVSGAASTPDKLAQAAQSSAATITQLAEVVKLGAASLGSNDPETQVVLINAIKDVAKALSDLIGATKGAAPSMYQLKGAAKVMVTNVTSLLKTVKAVEDEATRGTRALEATIEYIKQELTVFQS
GTQACINAHTKVQGIIGDLDTTLMFVTSGALNPEESFAEHRENILSTAKALVEDTKQLVAGAAGGQEKLAGAAQSASQTISKLADVVKSGASSLGADDPDTQVILINAVRDVASALADLINSTKDAAQAMFHLKASAKAMVTNVTSLLKTVKSVEDEAAKGPRAIEQTINSIKQELKSLQS
GTQACINAASTVQGIVSDLDTTVMFATAGTLNPEETFADHREDILRTAKVLVEDTKRLVASAQASQDVLATAAESSVGSVSKLTDHVKLGAAAMGSDDSDAQQMLLNAARDVASALGSLINSTKNASPAMEPLKSSAKTMVSNVSSLLMTVKTVEDKTMRGTRAMESSNEAIKQAIMVLNS
GTQACIDGCTILSGLIGDLSTYSMFASAGTLESEDSFVNYRDEILLAAKSLAEDCSKLTASSSFTQEQLADDIHKIIDRISCLSNSIKLGASALPVAELDTQVMLLNSAMDVAVALNNLLNATKSASPSFAELKTNAKSLVESVSSLIKTVTTVGTSSARGLRALESTINAISHELKSDKP
GTQACITAASAVSGIIADLDTTIMFATAGTLNREETFADHREGILKTAKALVEDTKVLVQNATSSQEKLAQAAQSSVTTITRLAETVKLGAASLGAEDPETQVMGGGGQEDVNRLVGDIASREAQIHCAARICSDCPQVMVTNVTSLLKTVKAVEDEATKGTRALEATIEHIRQELAVFSS
GTQACINASSTVSAIISDLDTTIMFATAGTLHAAGRFSDHREHILKTAKALVEDTKILVAGAAGTQDQLAAAAQNAVTTILQLADAVKHGAASLGSGQPDSQVMVMNAVKDVAAALGELINATKLASPAMNDLKDSAK-MIRELCLTETSTRSV----MAGYRHTPAGADHHQEQYL-YTH
GTQACINAANTVSGIIGDLDTTIMFATAGSLNPQENFGNHREAILKTAKALVEDTKALVAGAASNQEQLAVAAQNAVRTIVNLSDAVKNGAVSLSSDNAEAQVMVIHAVRDVAAALSNLIQATKNASPAMGYLKEAAKIMVTNVTSLLKTVKTIENEHQRGERALEAAIEAISQEISLYDS
GTQACITAASAVSGIIADLDTTIMFATAGTLNREETFADHREGILKTAKVLVEDTKVLVQNAAGSQEKLAQAAQSSVATITRLADVVKLGAASLGAEDPETQVVLINAVKDVAKALGDLISATKAAAPAVWQLKNSAKVMVTNVTSLLKTVKAVEDEATKGTRALEATTEHIRQELAVFCS
GTQACITAASAVSGIIADLDTTIMFATAGTLNREETFADHREYILKTAKALVEDTKMLVSGAGASQEKLAQAAQSSVSTITKLADVVKLGAASLGSEDPETQVVLINAVKDVAKALANLISTTKAAAPSMLQLKSSAKVMVTNVTSLLKTVKAVEDEATKGTRALEATIEHIKQELTVFNG
GTQACITAANAVSGIIADLDTTIMFASAGTLNSEESFADHRENILKTAKSLVEDTKMLVSGAASSQDKLSQAAQSSAKTITQLTDVVKLGAASIGSDDPETQVVLINAVKDVAKALGELISATKCAAPSMYQLKSAAKVMVTNVTSLLKTVKAVEDEATRGTRALEATIECIKQELALFQS
GTQACINAASTVSGIIGDLDTTIMFATAGTLNPEEVFSDHREAILRTAKALVEDTKALVSGAASSQEQLAVAAQNAVRTIVQLSEVVKSGAAALTSSNSEAQVLVINAVKDVAAALSHLIQATKSASPAMNSLKEAAKVMVTNVTSLLKTVKTVEDEHQRGTRALEAAIEAIGQEIRAYDS
GTQACITAASAVSGIIADLDTTIMFASAGTLNPEDSFADHRESILKTAKALVEDTKLLVAGAASSQEKLAQAAHSSAKTITQLTEVVKLGATSMGSEGPETQVVLINAVRDVAKALAELIGATKCAALSMYQLKSAAKVMVTNVTSLLKTVKAVEDEATRGTRALEATIECIKQELTLFQS
GTQACINAANTVSGIIGDLDTTIMFATSGSLNSSQKFPAHRDAILKTAKALVEDTKALVAGAASNQEQLAVAAQNAVRTIVNLSDAVKNGAVSLTSENSEAQVLIIHAVRDVAAALTSLIQATKNASQAMGHLKEAAKVMVTNVACLLKTVKSVEDKHHQGTRAVEAAVEAINFEIRQYDI
GTQACINAANTVSGIIGDLDTTILFATSGSLNPAGDFTTHREEVIKIAKALIEDTKALVSGAASNQEQLAVAAQNAVRTMVMLCEVVKTGALSLSADNTEAQVSVMHACRDVAAALSLLIHATKNASPAFEKMKFVTKTMISNVSSLLKMVKSVEDREHKGTLALEAAEEAIYQEIQAC--
GTQACINAASTVSGIIGDLDTTIMFASAGTLSPEEAFADHRENILKTAKALVEDTKTLVSGAASSQEHLAAAAQSAVDTITKLADAVKSGAASLGPQDAEAQVLLINAVKDVAVALGELIEATKNASPSMVQLKNSAKQLLL---------------------------------------
GTQACIHAAATISAIIGDLDTTILFASAGTLQSDDTFSDHRENILKTAKALVEDTKTLVGGAAGTQEQLADAAQNAVTTIVQLCEVVKLGAMSLGSGSPEPQVLVLHAARDVAAALRDLAAATTAASADMQRLKHAAK-------------------------------------------
GTQACTEAINKIQGIVGDLETSAMFATAGALTTEESLSEACHQIVETAKTLVDDTKRLVSSAGGTQEQLAEAATKAVNTIQTGADQVKKGASSLTSDDVEAQLLLLNAVRDVANAMGNLIDATKVASPAMENLKEVAKKMVGEVSRLLKTVKNVESEAARGVQSLEKTIDSIESDLVEFQS
-----ITCTEAISGVLEDLDTTMSESNNLELENEQKFSDYRENILKTSKTLVENTKSLVAGVGVSKEQLDDSSKNAVSTVLKLVELVKSGAASL--DDREVQIQLITAVKNVASSLGELIKGC-FETPSMDELKDKAKVLVTNVTSLLKTVKSVDDERTKNINVMESTVELIEKDLQ----
NTQACLQSASTVSGIIADLDTTILFASSGTLHGEEGFGTVRDSIVRTARALVDDTQSLVSGTGEDQTRLATTAHVAVERVTQLADVVKRGAAVIGPGQPDTQVEVLSACRDVATGLRDVLLAARDASPVHEQVRNNVQLTLSNIGALLQKVKTIADDENRGIQALVSAAKYCRDQ------
GTQACIDACVKVEDTVTDLETTLMFASAGSLMPDTTFTAEQGQIRDKAKQLVEQTKALVTATAESQDDLAAAAEKLNTNFDALVESLKLGAASLGPDQVDAQVLLLNAARDVGATLAELLTTTKSVSAAVKKLGEQARTMATNISGLLKTVSGIDDGAQRVTQALESAMQAIEREET----
GTQACLQSASTVSGIISDLDTTILFASSGTLHPPENFTPLRESIARTARALVDDTQSLVSGTGEDQARLANTAHIAVERITELADVVKRGAAVIGPGQPDTQLSV----------------------------------------------------------------------------
GTQACLTAAAAVETQLADLETMALFATAGSVAAEESFREHQPPMLEAARGLVDRAKRLISSAASSQGDLADAAENCQSMYNDLVDKLKLSLDSLASAGPEAQVLLINAARDVGNSLADLLNSTRKASPAMQALGHQTKHMANSVTALLRTVQTVENEQGRVTTALKAAQGAIETEASVFC-
-----------------NTDGTGIPSRAGSMHNAENFTPLRESIARTARALVDDTQSLVSGTGEDQARLASTAHTAVERITQLADVVKQGAAVIGPGQPDTQVELLTSCRDISNALRNVFLSSSKAHPVYDEVRSNAQLVISNIGQLLQTLRSIEEDERRGIR------------------
GLLACQAANTHLDDMISELETTTIFAQAKQL-VDTDNSLYKDQLLLSVEKLTDLIDGFSKCNKLTQDELALMLNESVSVMSVFKENVQTASITS--NDSNSQMQLLALSKDILTSMQELVKSSGKATAYMDSVKQAIAVQQVASKAFKD-YTV-SDDSQRASRTADNVIEEINSACQIMND
ADRDIENACNEVNHVTTDLDAASLFAAAGQIDIDDGHQNIQEQVGKLAQDLKDSKNQLAEASGKTIEDVGTSAKATASINQKLAHATKVCAA--LTSDSTTQQNLLSAARTVSSNLQQTISASKNAQGNKAILDKSSQELEESIDSLANL--VQSSTTTKGISELEGVSSEIRKQLAAYDS
GDRDIETAINNITHVTTDLDAASLFAAAGQVDVDEGYQSIQEEIAKLTEEVLQANAQLVDASTKTVEDVGFASKALANANDRLANAVKICAA--LTTDNGTQQSLLNGARNVSSSSQASVVASKHTQANKNAQKSATKQLEESIQSLSGL--ALSSTTTKGIRELDSVSAEIRKQLSVYDS
GDRDIENALNTINRITTDLDAASLFAAAGQVEADEGYSQVQEEIEKQSQEIHDANKNLVAASEKTIEDVATNAKYIAELNEKLATTSKVAAA--LTSDPSAQQAILNAARNVSAAVQTAVSASRSTHANKNQLQATSKNVDECVDTLKSL--TSSSTTTKGIKELDQIAAEIRKQLSNYDS
GDRDIETALNEVNHVTTDLDAASLFAAAGQIDVDEGHQDIQTEIAKLSQELKNANHQLVDSTNKTIEDVAFSSKAIASVNQKLSHASKVCAA--LTSDSATQQNLLSSARAVSSNLQQTISASKTTQANKTILNKASKDLDDSIESLSSL--VQSSTTTKGISELENISSEIRRQLATYDT
GTAACETAINNVQGIVGDLQTMAMFAASGSL-PDPKDNAHKEDILKRAKEIVENTKALVTGAASSQEALATAAKSSVDTLSHLSDACKATSISS--RDSNAQELLLNAVKDVAAALADLIGSTKTAAPAMEGLKENAKGMVNNISQLVK-VKS-EDEASRGVRALESAIEAIGTELKVLES
GEILSDAAIDQIRKVISDLDGYSLFAAAGQLENDSSQKNLQKDTITQAKLLIVSSSQLVGSSRGTQEHLGSATTKVANTVSSLVKTAKDIAS--VLADTTSQQDILSASKALSISSQQMVLATKDAQTAFRSLGKSAEAVAEAVGQFLTSVYTAISDAGKGIKELEKSIVQVANYHEKPDT
GEIQCDAAVESIKKMICDIDAYSLFAAAGQLEANQHYNQVQKDVVTQAKLLIVSGSQLVGSSKGTQENLGAATTKFAEQVTKLVGSSKEVAS--SLRDTQSQQDVLSASKALSISCQQLVLAGKDAQTAFRSLGKAAEGVAEAVGQLISSVYIAINEAGKGIKELEKASVVINSYIDKPDT
GEILSDAAIDNIRKVISDLDGYSLFAAAGQLENDSSQKNLQKDIILQSKMLIVSGSQLVGSSKGTQEHLGAATTKVASTVADLVKTAKDIAS--VLSDHESQEDILGASKALSISCQQMVLATKDAQTAFRSLGKSAEAIAEAVGQFLTSIYTAIADAGKGIKELEKSIVQVSSYHEKPEA
--------------------------------------------------------------------------------------------------------------------------------------KDESVMVTNVTSLLKTVKAVEDEHTRGTRALESTIEAISQEIEVL--
GADACTTAITKLNRLVADLDTAALFARAGTLQESSAFQTAQEAVMQAEKGIVEDMQTLIKTTSTDQDALAISAQNCFIRATELTESAKNAAASLLSSNPEGQVQLLTSTRDVISGLVRLLNHGKSISTLMKALNDTAVQVIETISGLSNALRSVSDM------------------------
GADACGTAATNMGRLVADLDTAVLFARAGTLVDTCAFQSAKEAVMQATKGVVEDMQTLIANSTGSQDALSVSAQNCLLRATELTEAVKSAAAN-VPVAPESQVQLLSCARDVAKSLVQLLTQAKAVTPLLKALNETAVSVVENISGLTRSIRALSDQ------------------------
---------------------------------------------------------------------------------------------------------------------------------------SSPVMVTNVTSLLKTVKTVEDEAARGTRALEATIEAIKQEMRILSS
GEIQSDAAVESIKKIIRDIDASSLFAAAGQLESTNRLKSLQKDIVTGAKLLIVSGSQLVGSSKGSQEHLGSATTKFAEMVSHLAMGAKEIASIL--SDTASQQDILSASKALSISSQQLVLAGKDAQTAFRSLGKAAEGVAEAVGQFISSIYFAISEAGKGLKELEKAHLVINSFVERPD-
--------TSKIDTIKQDVETDILFAQSGTLALDEHFEDLMSEVAKYSRTLTENIRSLVQAVESTDSQLKDACESSGESIDNLADTIRATSKFIAKEGPEAQVALLNVTKSLVAALSDLINATRVINENVEKLKHSSTETVRNVTRLIQTVQVIREDQSRGPSELMSAISAIEN-------
-----------------------------------------------------------------------------------------------------KVELLTSCRDVATALRSVFLSSGKVQPIYDEVRSNAQVVISNVGQLLQTLKSVEEDERRGIRSLELAANYCREQAKL---
