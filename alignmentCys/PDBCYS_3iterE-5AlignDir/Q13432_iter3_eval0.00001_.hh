KQPIGPEDVLGLQRITGDYLCSPEENIYKIDFVRFKIRD-DSGTVLFEIKKP-PNAGRFVRYQFTPAFLRLRQVGATVEFTVGDKPVNNFR-IERHYFRNQLLKSFDFHFGFCIPSSKNTCEHIYDFPPLSEELISE-IRHPYETQSDSFYFVDDRLV-HNKADYSYS
KPHISPEDVLGLQKITSDYLCTPEENVYKIDFTRFKIRDMESGTVLFEITKPPGNPYTNVNIHLKPCFIRLLYCVSTVEFTVGDKPINNFRMIERHYFREQLLKSFDFEFGFCIPSSKNTCEHIYEFPQLSEDLIREMILHPYETQSDSFYFVDNKLVMHNKADYSYS
KPHISPEDVLGLQKITSDYLCTPEENVYKIDFTRFKIRDMESGTVLFEITKPPPNAGRFVRYQFTPAFLRLRQVGATVEFTVGDKPINNFRMIERHYFREQLLKSFDFEFGFCIPSSKNTCEHIYEFPQLSEDLIREMILHPYETQSDSFYFVDNKLVMHNKADYSYS
SAVITPEDVLGLQKITENYLCSPDENIFSIDFTRFKIRDMETGTVLFEITKPPPNAGRFVRYQFTPAFLRLRQVGATVEFTVGDLPIENFRMIERHYFRERLLKSFDFEFGFCMPRSKNTCEHIYEFPPLSEDSIQEMILHPYETQSDSFYFVDNKLVMHNKADYSYN
KQPIGPEDVLGLQRITGDFF-PPDSFLFEESFQGWRWAAAATVTPSPQSSEPY-NAGRFVRYQFTPAFLRLRQVGATVEFTVGDKPVNNFRMIERHYFRNQLLKSFDFHFGFCIPSSKNTCEHIYDFPPLSEELINEMIRHPYETQSDSFYFVDDRLVMHNKADYSYS
-TVITPDDVLGLQKITKNYLCSPEENVHMIDFTRFKIRDMETGTVLFEITKPPPNAGRFVRYQFTPAFLQLRQVGATVEFTVGDTPINNFRMIERHYFRDQLLKSFDFEFGFCMPSSKNTCEHIYEFPPLSEDIMREMILNPYETQSDSFYFVDNKLVMHNKADYSYS
-GGAGGAAARTEEELGRKALIGPDDVLGLQRVTSVCLCCWDKAYLEFYYSKYCRGSERMLRDESLHGLISLSFFLSRVEFTVGDKPINNFRMIERHYFRDQLLKSFDFEFGFCIPSSKNTCEHIYEFPQLSEDLIREMILHPYETQSDSFYFVDNKLVMHNKADYSYS
-RGSITEDTLRLTCSTDNYLCEVDDNIYGIDFTRFKIRDLESDTVLFEISKPSVTAGRFVRYQFTPQFLNLKTIGATVEFVVGDTPVQSFRMIERHYFRSQLLKSFDFDFGFCIPNSKNTCEHIYEFPKLSQDTMQKMINSPFETKSDSFYFVNNKLVMHNKADYAYN
-----------------TTSASPEDNVHMIDFTRFKIRDMETGTVLFEITKPPPNAGRFVRYQFTPGVMKRTSMADRVEFTVGDTPINNFRMIERHYFRDQLLKSFDFEFGFCMPSSKNTCEHIYEFPALSEEIMHEMILHPYETQSDSFYFVDNKLVMHNKADYSYS
------KQHLCLSLLFSDYLCKPEDNIYSIDFTRFKIRDLETGTVLFEIAKPCISAGRFVRYQFTPAFLRLRTVGATVEFTVGDKPVSNFRMIERHYFRERLLKNFDFDFGFCIPSSRNTCEHIYEFPQLSEDVIRLMIENPYETRSDSFYFVDNKLIMHNKADYAYN
-GE-TPDDVLGLRSITKDYLCSPQANIYEVEFVKFKIRDMDTDTLLFEIEKPEPSMARYVRYRFSRDFLALKRVGATVSFRVGDQGVKDFRMIERHYFKDKLLKSFDFVFGYCIPGSDNTCEHIYEFPRLSDDLIEEMVNCPYETRSDSFYFADGKI-----------
------MSLLLTTSLLADYLCKPEDNVYNIDFTRFKIRDLETGTVLFEIAKPHTSAGRFVRYQFTPAFLRLRTVGATVEFTVGDQPVNNFRMIERHYFQDKVLKNFDFDFGFCIPNSRNTCEHIYEFPQLPDDLIRLMVEHPYETRSDSFYFVDNKLIMHNKADYAYD
-AVITPDYVLKMTKITDDYLCAPEANVYDIDFTRFKIRDMESGAVLFEIAKPGLNAGRYVRYQFTPQFLKLKTVGATVEFTVGGRSVKNFRMIERHFFQDKLLKTFDFEFGYCIPYSKNTCEHIYEFPTIPPDLVNEMIMNPFETRSDSFYFVDNSLVMHNKADYAYN
-SLITPDYVLQLNKIADDYLCTPDANIYEIDFTRFKIRDLESGAVLFEIAKPSPNAGRYVRYQFTPQFLKLKTVGATVEFTVGSKSVKNFRMIERHFFKDRLLKTFDFEFGYCIPFSKNTCEHIYEFPTIPPDLVNEMIQHPFETRSDSFYFVDGCLVMHNKADYAYN
-QHITPEMVLRLPTITDNYLCSPEANTYDIDFTRFQIRDLETGTILFEISKPPSNAGRFVRYQFTPQFLKLKTVGATVEFMVGSRPAKNFRMIERHFFRDKLLKTFDFEFGFCIPNSKNTCEHIYEFPTLPPELVAEMIANPFETRSDSFYFVDDQLVMHNKADYAY-
---ITPEDVLKLKQITNSYLCAPDANIYDIDFTRFCIRDLESGTVLFEIAKPLPNAGRFVRYQFTPHFLKLRTVGATVEFVVGEKPVNNFRMIERHFFRDRLLKTFDFEFGFCIPHSKNTVEHIYDFPALSSEAMDEMINHPFETRSDSFYFVENRLIMHNKADYAYN
-NQITPNDVLALPGITQGFLCSPSANVYNIEFTKFQIRDLDTEHVLFEIAKP-AESARYVRYRFAPNFLKLKTVGATVEFKVGDVPITHFRMIERHFFKDRLLKCFDFEFGFCMPNSRNNCEHIYEFPQLSQQLMDDMINNPNETRSDSFYFVENKLVMHNKADYSYD
-NKVTPEDVLRLSKITDCYLCTPEANIYQIDFTRFKIRDLGTGIVLFEIAKPTPNAGRFVRYQFTPHFLKLKTVGATVEFTVGAQPIHNFRMIERHFFRNKLLKTFDFDFGFCIPNSKNTCEHIYEFPVLDPELVKEMISHPFETKSDSFYFVDDKLVMHNKADYAYD
-VPVTPEMVLRLPTITDKYLCSPEANIYDIDFTRFQIRDIETGAVLFEITKPPANTGRFVRYQFTPQFLKLKTVGATVEFLVGPKPVNNFRMIERHFFRDRLLKTFDFQFGFCIPNSKNTCEHIYEFPTLPADLVSEMIANPFETRSDSFYFVDNQLVMHNKADYAYN
-DYITPDDVLRLTKATTAYLVRPEANIYDINFVRFKLRDIATKQTLFEVSKPEPTNGRFVRYQFTPQFLKLKTVGATVEFTLGDKPVNKFRMIERHYFRNRLLKSFDFEFGFLIPMSKNTMEHIYEFPDLSKEQMEQMIDCPYETRSDSFYFVDDKLIMHNKADYSYN
---IRPEDVKRLARSTENYLCEPTANVYNIDFTRFKIRDMETGATLFEIAKPDPNAGRYVRYKFTPEFLKLKTVGATVEFTVGDKPVTNFRMVERHYYHEKLLKSFDFEFGFCIPNSKNTCEHIYEFPTLTPDEMQEMIEHPFETKSDSFYFVDNKLIMHNKADYAYN
---IRPEDVTRLGKMTENYLCKPSANVFGIEFTRFKIRDMESGDVIFEIAKPDPRRGRYVRYQFSPKFLDLKSVGATVEFVVGDKAINSFRMIECHYFREKLLKSFDFDFGFCIPNSKNTCEHIYEYPELNPDLKEEMINSPFETRSDSFYFVDNKLVMHNKADYCYN
-PHISPEDVLKLD--TESYLCPLQANIYDIEFTRFKLRDLDSNQVLFE-SKP---ESRFVRYHFPIPFLDLKTLGATIEFTVGDREVKDFRMIERHFFRDKCLKSFDFNFGFCMPTSRNTCEHIYEMPVLLQSEKEELISHPYETKSDSFYFVNGKLVMHH-------
-TTISPEDVLTLSKITDGYLCAPDANIYNIDFIRFKIRDLDTGIVLFEIAKPSLNSGRFVRYQFTPKFLKLKLVGATVEFTVGNKPVNKFRMIERHYFRNKLLKTFDFEFGFCIPHSKNTCEHIYKFPKLDPESIKEMIQNPFETKSDSFYFVEDQLIMHNKAEYAYN
-NNVTPDDVLKFNKITDTYLCSPDANVYDIDFTRFKIRDLETGTVLFEIAKPPPNAGRFVRYQFTPQFLKLKTVGATVEFTVGARPVNHFRMIEKHFFRDTLLKTFDFEFGFCIPFSRNTCEHIYEFPSLPPNLVEEMIAAPFETCSDSFYFVDNHLVMHNKADYAYN
-KAITPEDVLRLTKITETYLCDPDANIYDIDFTRFKIRDLESGAVLFEIAKPPPNSGRFVRYQFTPQFLKLKTVGATVEFTVGGRPVNRFRMIERHFFRDKLLKTFDFEFGFCIPYSRNTCEHIYEFPVLPPDLVNEMIASPFETRSDSFYFVDDFLIMHNKADYAYN
---ITPSDVIRLNRITKDYLCGPDANIYDIEFTCFKIRDLDSGIVLFEIAKPDCSGGRFVRYQFTPQFLRLKHVTATVEFTVGDRPVNRFRMIERHFFKDQLLKSFDFEFGFCIPNSTNTCEHIYDFPQIPEDLVQKMINCPYETRSDSFYFVEDRLIMHNKADYAYD
-DTIRPEHVLRLNRVTENYLCKPEDNVYSIDFTRFKIRDLETGTVLFEIAKPCISVGRFVRYQFTPAFLRLRTVGATVEFTVGDRPVTGFRMIERHYFRERLLKTFDFDFGFCIPSSRNTCEHIYEFPQLSEDVIRLMIENPYETRSDSFYFVDNKLVMHNKADYAYN
-RDITPDNVLGLRAVTEDYLCKPEDNVFNIDFTRFKIRDLETGTVLFEIAKPPISAGRFVRYQFTPAFLKLCTVGATVEFTVGDRPINSFRMIERHYFQGRLLKNFDFDFGFCIPDSRNTCEHIYEFPQLPDDLIRQMVAHPYETRSDSFYFVDNKLIMHNKADYAYN
-NDLRPEHVLGLSRVTENYLCKPEDNIYGIDFTRFKIRDLETGTVLFEISKPCASAGRFVRYQFTPAFLRLRKVGATVEFTVGDKPVKSFRMIERHYFRDRILKSFDFDFGFCIPNSRNTCEHMYEFPQLSEELIRQMTENPYETRSDSFYFVDNKLIMHNKADYAYN
-GPITPEDVLGLRVATRGYLCKPEDNIYNIDFIRFKIRDLETSTVLFEIAKPPASAGRFVRYQFTPAFLKLRTVGATVEFTVGNRPLNNFRMIERHYFRDHLLKSFDFDFGFCIPNSRNTCEHIYEFPQLSESLVRQMVECPYETRSDSFYFVDNRLVMHNKADYAYN
-RDITPDEVLGLRAVARDYLCKLEDNIYNIDFTRFKIRDLETGTVLFEIAKPPTSAGRFVRYQFTPAFLKLRTVGATVEFTVGDQPVTNFRMIERHYFQDRLLKSFDFDFGFCIPNSRNTCEHIYEFPQLPEDLIRLMIEHPYETRSDSFYFVDNKLIMHNKADYAYN
-EAVRPEHVLGLGRVTEHYLCRPEDNIYNIDFTKFKIRDLETGTVLFEIAKPSSSAGRFVRYQFTPAFLRLRTVGATVEFTVGDKPVSNFRMIERHYFRDRLLKNFDFDFGFCIPCSRNTCEHIYEFPQLSEDLIHLMIENPYETRSDSFYFVDNKLIMHNKADYAYN
-DAITPDDVLRLQKITENYLCAPDANVHDIDFTRFKIRDMESGMVLFEIAKPPPNAGRFVRYQFTPQFLKLKTVGATVEFTVGAKPVSKFRMIERHFFRDKLLKTFDFEFGFCIPNSKNTCEHIYEFPSLSQDLCDEMIANPFETRSDSFYFVDDKLIMHNKADYAYN
-DNISPEDVLKLPKVTENYLCEPEANVFQIDFTRFKIRDMDSGTVLFEIAKPPPNAGRFVRYQFTPQFLKLRTVGATVEFTVGDKPVSQFRMIERHYFRERLLKSFDFNFGFCIPNSKNTCEHIYEFPELAPDQIQEMIDHPYETKSDSFYFVDNNLIMHNKADYAYN
-AFITPDDVLRLNKITDDYLCEPEANIFDVEFTRFKIRDLDTDQVLFEIAKPTASAARFVRYQFTPAFLRLKHVGATVEFIVGNKPVNKFRMIERHFFRDRLLKSFDFEFGFCIPNSKNTCEHIYDFPQLSESLINEMISCPYETRSDSFYFVEDRLIMHNKADYAYN
-----------------GYLCKPEDNIYNIDFIRFKIRDLETSTILFEIAKPPSYVSSISRHTHQSLFVKLFSLSNRVEFTVGNRPLNHFRMIERHYFRDHLLKSFDFDFGFCIPNSRNTCEHIYEFPQLSESLVRQMVECPYETRSDSFYFADNRLVMHNKADYAYN
------------------YLCTPEENIYNIRFTRFRIRDLENGMV---TR-PPGTSNFFLR--GTCII-EMFSL-CRVEFTVGSKAVKNFRMIERHFFRDTLLKSFDFDFGFCIPNSRNTCEHIYEL--------CEMISHPYETQSDSFYFVDNKLIMHQK------
-SNVSPEDVLHLTKITDDYLCSANANVFEIDFTRFKIRDLESGAVLFEIAKPPPNAGRYVRYQFTPAFLNLKTVGATVEFTVGSQPVNNFRMIERHFFRDRLLKTFDFEFGYCIPYSKNTCEHIYEFPNLPPDLVAEMISSPFETRSDSFYFVENRLVMHNKADYAYD
-DTIRPEHVLRLSRVTENYLCKPEDNIYSIDFTRFKIRDLETGTVLFEIAKPCISAGRFVRYQFTPAFLRLRTVGATVEFTVGDKPVSNFRMIERHYFRERLLKNFDFDFGFCIPSSRNTCEHIYEFPQLSEDVIRLMIENPYETRSDSFYFVSDFLGIHQGFSLSYS
-GNISPEDTLRLSCSTENYLCSVDDNVYGIDFTRFKIRDMESDSVLFEIAKPTPNAGRFVRYQFTPEFLRLKTVGATVEFVVGDNPVQSFRMVERHYFRNQLLKSFDFDFGFCIPNSKNTCEHIYEFPKLSQETMQKMIDAPFDTKSDSFYFVNGKLVMHNKADYAYN
-ATINPEEVLRLNKITEHYLCSPEANIYGIDFTRFKLRDMDSGAVLFEVAKPPPHAGRFVRYQFTPQFLKLKTVGATVEFVVGEKPVNKFRMIERHFFRDKLLKSFDFEFGFCIPHSRNTVEHIYEFPTLTPDEVEDMISHPFETRSDSFYFVEDQLIMHNKADYAYN
--------------------------VRKRNFKHLHVFKYECNCYLFFFRKPKPNAGRFVRYQFTPAFLRLRQVGATVEFTVGDKPVNNFRMIERHYFRNQLLKRFDFHFGFCIPSSKNTCEHIYDFPPLSEELISEMIRHPYETQSDSLYFVDDWLVMHNKADYSYS
KEVITPADVLSLPGITQGFLCSPQANVYNIEFTRFQIRDLNTQQVLFEIAKPE-DAARYVRYRFGPSFLRLKTVGATVEFKVGDYPVENFRMIERHFFKDRLLKCFDFEFGFCIPNSRNNCEHIYDFPQLSQTLTL--------------------------------
--RYTPEDVLELEAPTESFLCPLSANKYGIEFLHFSISDFESKRKIFEIGKDPEGAYRTIRYEFSEDVLRLPAIATELEFSVGPHEVHNLRMIERHYFRDEIVKSYDFNFGFCIPSSVNTWEAIYAMPPLDDELISDMVAQPYATVSDSFYFVGDELIMHNKAEYKYI
--FYTPEDVLCLSAPTKDFLCSLNANTYGIKFLRFSIADYESKRKFFRIGKDSESKYRTIRYDFSEDVLRLPSIVTKLKFSVGNEEIRGLRMIERHYFRDKIIKSFDFTFGFCIPGSVNTWETIYSVPPLDEDLIRNIVLHPYETSSDSFYFVGSKMIMHNKAEYKYT
--PVLPEQALRLTRPTDGFLCPLSANKYKIDFREFEIKDYDSGESLFHVKRDEEAAVRTVRYTFPTSFLGRKTIRTALVFSVGPDPVPNFRMIERHYFKDELVRSYDFNFGFCIPMSVNSWEAIYDMPELTKEREEEILASPFEMRSDSFYYVGDTLVMHNKAEYQY-
--WVTPEVVTKFSCATEDFLCPASANTYGIDFIGFIVRDADTGRKLLEITKEPEAAQRNIRYQFGPRFLELEHVGTKLEFTVGDKPVHNFRMIERHYFKDKILKSYDFTMPYCMPNTVNTWEVIYELPELTAEEKKAMIEAPWETKSDSFYFIDDILVMHHKADYSY-
--VITPEYVKALTQPTDQFLCRISDNWPKLRFGGFKIRDMISGITLVEVQDD-DPKTRVIKYHLGPDFLRLSTVGLTLRFSNGEKAINEMQMVERHYFRGKVIRSYEFKFGFVIPGSTNEWEFIYDLPELDQETKIQIVQAPWEVKSDSFFFADGQIIIHNRAEYNYC
--TITPEEVLGLQAPTTQFLCEPSANIYGIEFVAFKLRDMESGNVLVNLGPGDDNSGRFVRYQFRPDFFDLKSVGASVTFCVGDKPVPNFRMIERHFFGNKLIKSFDFDFGFCIPESTNTCEHIYELPKLPKADIDAMIATPYATKSDSFYFVDGKLVMHNKAEYAYD
--KFTPDDVRAFTKPTEQFLCPLSANHYNIQFLDFRIRDVETNKVFFQISREAEIEMRTVRYHFGNNFFDIKTIGTSLTFSVGQKPVKNFLMIERHYFREKLIQSYEFKFPFCIPNTTNNWESIYDVPSLSEKEKEEMILNPWETKSDSFYFVGDELVMHNKAEYNYA
--QITPEIVRSYTKTTETFLCPLKANHYNIQFLSFRIRDMDTNKIFFQIQKEAEEEWRTVRYHFGPEFFKIKTIGTQLQFSIGKKPIKNFLMIERHYFNDQIIQSYEFKFPFCIPDTVNDWESIYDVPILDEKLVQEMINSPWKTKSDSFYFVENELVMHNKAEYNYS
--EITPSDVINLKAPTTQYLCKTTDNKFGIEFTAFKLRDLDRGLILFEIAK-DDDSGRFVDYRFTPDFLRLRTVGATVTFRVGDEPVPNFRMIERHFFRNKLLKSFDFDFGFCIPASNNTCEHIYQFPTLTEAEIEDMIASPGETKSDSFYFVDGKLVMHNRAVYSYD
---VTIDQVRQMKNASEKLYCRLSDN-KIITFGHYMIKDYDSKEVLVEINED-AGDDRTIQYSFGQKFLEYKTLSLKLEFNVTDEPVKDLLLVERHYFRDRLLINFEFKFPFCMPNSKNDCEFIYDLPQLTEEEKKDMIENPWETKSDSFFFHEGRLIIHNKAAYSYE
--DITPEQVLQFTSPAADFLCPVTANIYNIEFYSFVIRDDETKRVMFQVERDAQDALRTIYYRFPPSMLRSTRISASLVFGVGDKPAPNFRMIERHYFRNTLVKSFDFTFGFCIPHSRNTWEALYDMPKLNDEWLKAIESGSNEMVSDSFYFVDGRLVMHNKAFYSYN
--DVTPEMVLQHTGPTQDFLCPITANTYNIEFYSFYIRDADTKQVMFQVERDDQRVARTIYYRFPPSMLRKQRVSATLLFGVSDKPVPNLRMIERHYFRNTLVKSFDFTFGFCIPHSRNTWEVMYDVPQFDGEWLEAIQQGSNEMVSDSFYFVNGKLIMHNKAFYDYS
--KYTPQDVLKYNKPTEAFLCPLNANTVGIQFLEFKIRNCDSGEVLFHTSAETEDSLRSINYKFDKAMLRSKGIGTTLTFKVGPKPVKNFRMIERHYFKEKLLKGFDFNFKFCIPNSQNTWECIYSMPELTEEEIIEIENSPYQVKSDSFYFVDDEMIMHNKAAYAYV
--TINQEYVRNLQEITKEFLCPISANIYNIQFLKFRIRDMESGQVLFEVERDNQDEARRIKYHFGPQFFELKTIGAQLTFSVGDKIVKNFTIIERHYFRNILLRSYEFQFPFCIPNSTNTWEHIYTIPEIAEDVKQLMIENPNDTKSDSFYFVGDTLIMHNKAEYDYS
--NITPEVVLSYDKPTRGYLCPLSANTYGIEFLKFEIKDYDTNRVVYQVAREDENLIRSVKYTFPASFLKFKTVRTLLEFCVGPQPVGNLRMIERHFFKDKLVRSYDFEFGFCIPNSTNSWEAIYDVPEHSTAMVNDYVTHPFAHKSDSFYFVDNNLIMHNKAEYQYV
--QVTPAYVRNLNGITQDFLCPVSANTYNIQFLKFRIRDMDSGQTLFEVERDDQDEARRIKYHFGPQFFELKTVGAQLTFSVGNKPVKNFTIIERHYFKDHLLRSYEFQFPFCIPDSTNTWEHIYTIPEIDEAMRQEMIDNPFQTKSDSFYFVGEQLVMHNKAEYDYS
--QVTPAYVKSLTGPTANFLCPVNANIYNIQFLKFRIRDMDSGQVLFEVERDDQDEARRIKYQFGPQFFELKTVGAQLTFSVGKDPVKNFTIIERHYFRDHLLRCYEFQFPFCIPNSTNTWEHIYTIPEIDEAMRQEMIDNPFETKSDSFYFVGDQLVMHNKAEYDYS
--PVTPEQVLAFTAPTERFLCPLTANKYGVEFYQFTIRDIEQNKVLFEVGEAPAAAARTIRYRFAPSFLRKTAVGAKLVFGVGDQPVPKFRMIERHYFRNVLIKSFDFEFGFCIPHSTNTWEAIYDMPSLNPEWEEAIVASPFEMASDSFYFVGNELIMHNKAYYAYD
--QVTPEQVLQFSAPTSEFLCPFTANTYNIEFYRFTIRDMETGKVMFDVERNPHRTMRTIYYNFPPSMLRRRAIGAKLVFGISDFPVRNFRMIERHYFRDTLIKSFDFTFGFCIPHSTNTWEAIYDMPALSEDWIRAIIDNPNETVSDSFYFVENKLVMHNKAFYAYD
--EVTPEHVLQLTAPSTGFLCPITANTYNIEFYSFVVRDADTKQVMFEVERDAQQAARTIFYRFPPSMLRKQRVSATLLFGVGDKPVPDLRMIERHYFRNTLVKTFDFKFGFCIPHSRNTWEAVYDVPQFNAEWLEAIEKGTNEMTSDSFYFADGRLIMHNRAFYDYS
--EWTPDECLKLTAPCEGYLCPMSANTYGIEFLEFRIREMEGQQRLYHVKKPDEDLYRLIQYAFPSEFLKARTIGTSLVFSVGDKEVQSFRMIERHYFRDKLLKSFDFTLGFCMPNSRNTWEVIYTLPELDAETEKAMIDNPYETKSDSFYFVGDKMVMHNKAEYAYT
--PRCPDEVKALTGPTETFLCSLKDNVYGIKFGAFKIRDMSSGTVLVDVREEDDDKVRLVKYHFGPDFFMLRTIGLTVGFSIGDQPIPNFYMVERHYFRNKVIKQYEFKFGFCMPNSQNEIEMIYDLPELAEEEKQEMIENPWETKSDTFFFVNDSLVIHNRAEYNYS
--QITPEVVLSYTKPASGYLCPLNANTFGIEFLKFEIKEYDTNTVVYQVAREPEDMIRSVKYTFPASFLKFKTVRTLLEFCVGPQPVSNLRMIERHFFKDKLVRTYDFEFGFCIPNSTNSWEAIYDVPEYPAGVINDYVTSPWAHKSDSFYFVDNKLIMHNKAEYQYV
--KVTPEIVRTFTNPCDKFLCQPSE----IKFIKFILRDADDNFILFDFDNDD--Q--VLSYQFTPQYFDIKTLGATKIFKTFDKPLKNLLLIDRFYFKNNLIKEYEFRFPFCIPHSTNTWESMYDQPALGANLKDQMLLNPWETKSDTFLFIDGKLANHERIEYDFS
--PVTPQYVLRLPGYTDDYMCSPEDNIYNISFSRFKIRDLEGGCVILDLKRHCLDAGRFIQYHFSPAFLNLREIGAMLEFTVGGKAVNKFRLIEKHYFRDLLLKTFDFEIGFCIPHSRNTCEHIYCLPDLDPHTIEEMINHPFETRSDSFYFANNTLIMHHKAEYSFS
-----------------------------------------------------PNAGRFVRYQFTPQFLKLKTIGATVEFVVGDKPISNFRMIERHYFRDRLLKSFDFNFGFIIPDSKNTCEHIYEFPQLSTDEIHEMVDNPFETKSDSFYFVDNRLIMHNKADYAYN
--TMTPDEALSLRAPTDGFLCPLSANVYGIDFLEFEIKDYDSGESVFHVKKDGERAIRTVRYTFPEKFLRFETVRTALVFGVGETPVPDFRMIERHYFRDELVRSYDFNFGFCIPNSVNSWEAIYEVPGLEDERIAEFVDHPFDMKSDSFYFVGDELVMHNKAEYQYQ
---------------VSDFLCALSANKYQIEFLEFTISDYVTKNIIFMVGRPPENAYRLIKYEFSEDVLRLPSIQTSLVFSVGEQEVPNFRMVERHYFRDQLVKSYDFGFGFCIPASTNTWDAVYQVPPLDEDLIEEMIANPFETKSDSFYFVDDELIMHNKASYKY-
---------------------------------------LPTDDTFFEIKQPDPNLGQSIHYNFPKNFLAAKTIAAKLCFAVGCKELKGFRMMENHYFQGRLLKSFDFNFGFCIPNTINTWEHIYDVPFLDKKTQEKMIQSPGETVSDSFYFVDNKLVLHNKATYSFS
-------------------------------------------------------ETRTIRYQFGPHFLELETLSLKLEFYVGDEPIKDLLLIERFYFRDKLLSNFEFQFPFCMPKSKNECEFVYDLPILNEEEKQEMIDSPWDAQSDSFFFVNGQLIIHNKAAYNYS
-PIITPEDVLRLNKITDDYLCEPDANIYDIEFTRFKIRDLDTDQILFEIAKPTTSAARFVRYQFTPAFLLLKHVGAT-------------------------------------------------------------------------------------------
--TIKLSDVIHLKQPTEGYLCPTNKNDYDIQFLSFCLRDLDSNSTLFSVSRPTCALRRWVRCKFT-AF-----LTSRVEFSVGDNPIEEFRMIENHYFRKKLLKSFDFNFGFIIPNSSNTVEHIYELPKLSNK-----------------------------------
--TISPEDVLGLQKITENYLCSPEDNLYNIDFTRFKIRDMETGTVLFEITKPP-------------------------------------------------------------------------------------------------------------------
------SEVLRHEAPTKNFLCPLSANEYGIEFLSFVIQDYDSKLTIFEVSRDRPDSLRKINYELSEDFLRLPNISTTLVFSVGQEPLSDFRMIERHYFRDQLI---------------------------------------------NFYFVGDKLIMHNKAFYKY-
-------------------------------------------------------------------------------------------------------------------------------------RVEEMINNPNETRSDSFYFVDNKLVMHNKADYSYD
-QQVTTEYVRSLNGITQDFLCPVNANIYNIQFLKFRIRDMDSGQTLFEVERDQQDEARRIKYHFGPQFFELKTVGAQLIL----------------------------------------------------------------------------------------
----------------------------------MKMKDGDKGRVLWQQTTTNEVEAR-----IPKRILKCKSVCRELNFSS-KELIENFSLVQRIYFKGRLMEEWPFHFGFVMPNTTNSWENVIEAD----------------------------------------
----------------------------------MNLRDAETGKVLWQSTEPKKEKAH-----VPKNLLKCRTVSREINFTS-SVKIEKFRLEQRVYLKGTIIEEWFFDFGFVIPDSTNTWQNMIEAA----------------------------------------
----------------------------------MNLRDASSGKVLWQGDSPGFEEAR-----VPKKILKCKAVSREINFSS-LEVMEKFRLEQRVLFKGKCLEEWFFEFGFVIPDSTNTWQSVIEAA----------------------------------------
----------------------------------ICLRDADSGRVIWQGREPDIEEAR-----VPQNILKCRAVSREFSFSS-KEAIQNFRLEQKVLFKGRCLEEWNFEFGAVLPASTNTWQSTMEAA----------------------------------------
----------------------------------MNLRDAENGKIMWQSYDPGVEEAR-----VPKKILKCSQVSREINFSS-EQELTNLRLQQNIYFKDKLIEEWSFEFGFVIPSSTNTWQSIIEAA----------------------------------------
----------------------------------MNLRDADSGKILWQSTDPEIEKAH-----VPRKILKCKSVSREINFSS-KEQLENFWLEQEVFLRDQSIENWSFEFGFVIPGSTNTWQSLIQSD----------------------------------------
----------------------------------MNLRDADSGKVLWQGTEPGVEEAR-----VPKKILRCRAVSREINFSS-EEQMNRFRLEQRVLFKDKCLEEWFFEFGFVIPQSTNTWQSLIEAA----------------------------------------
----------------------------------MILRDADSGKIIWQENKPDVEEAR-----VPIKILDMRAVSREINFST-IESMENFRLDQKVLFKGRIMEEWFFEMGFVGANTTNTWQSTIEAA----------------------------------------
----------------------------------MILRDADTGKIIWQENKPDVEEAK-----VPVKILSLRAVSREINFST-VEAMENFRLDQKVLFKGRIMEEWFFEMGWVSPNTTNTWQSTIEAA----------------------------------------
----------------------------------MILRDGETGKTMWQGAEPGVEEAR-----VPKKILKCKSVSREINFAS-EEEMENFRLEQKVYFKGQCLEEWSFEFGFVMPNSVNTWQSMIEAA----------------------------------------
----------------------------------MNLRDADTGKVLWQGSEPGVEEAR-----VPKKILKCKAVSREVNFSS-AEEMHKFRLEQKVLFKGKCLEEWYFEFGFVIPSSTNTWQSLIEAA----------------------------------------
----------------------------------MNLRDAENGKILWQSNDPGSEEAR-----VPKKILKCKSVSREINFSS-QEQMDKFRLEQKVLFKGRCLEEWYFDFGFVIPSSTNTWQSIIEAA----------------------------------------
----------------------------------MSLREVDTGKLLWQNSDPDVEEAR-----VPKSILNCRAVSREINFSS-IEPLENFRLEQKVLFKGRALEEWTFDFGFVIPNSTNTWQSIIEAA----------------------------------------
----------------------------------MNLRDAESGKVLWQGDDSHKEEAR-----VPKKILKCRSVSREINFSS-EQEMKRFRLEQRVFLNSEILEEWFFEFGYVIPGSTNTWQNLIEAA----------------------------------------
----------------------------------MELCDANSEEILWKSNKPD-EEAH-----VTKKILQCRKVQRKINFSS-AEKMERFCLKQKVLFKEHCLEEL---FG---PNLT-TWKSSILAS----------------------------------------
----------------------------------MNLRDADNGKILWQSSDPGQDEAR-----VPKKILKCRAVSREINFSS-KEAMDKFRLEQRVLFKGKCLEEWFFEFGFVIPGSTNTWQSVIEAA----------------------------------------
----------------------------------MNLRDADTGKILWQHNEPDAEEAR-----VPKRILKCRVVSREMNFSS-IESMDRFRLEQKVLFKGRCLEEWFFDFGYVIPNSTNTWQSVIESA----------------------------------------
-------------------------------LNWMNLRDADTGKMLWQGTDPDVEEAR-----VPKKILKCRAVSREINFSS-VESMEKFRLEQKVLFKGRCLEEWFFEFGFVIPNSTNTWQSLIEAAPES-------------------------------------
----------------------------------MSLRDAESGKVLWQSYEPGKEQAR-----VPKSILKCRAVSREINFTS-AEKINKFRLEQRVYLKGDIIEEWFFDFGFVIPQSTNTWQSLIEAAPEA-------------------------------------
---------------------------KGFQVNWMNLRDADSGKILWQGNEPDVEEAR-----VPKKILRCRAVSREINFSS-AEPMERFRLEQKVLFKGRCLEEWFFEFGFVIPNSTNTWQSLIQAAPESQ------------------------------------
---------------------------------WMNLRDAETGKILWQGTEDEVEEAR-----VPKKILKCKAVSRELNFSS-AEQMEKFRLEQKVYFKGQCLEEWFFEFGFVIPNSTNTWQSLIEAAPESQ------------------------------------
------------------------------TIDGMNMRDASTGEMLWEMNSPSAEQAR-----VPARILQCPVVSRELTFSS-VEKVANLRCEHRVYLDGDPIEVWDFDFGFVMPGSRNTWQTTVEAA----------------------------------------
------------------------------KIVSMKMKNASNGRVLWESDDNLVKDVK-----FPREMLQCSEVSREIVFKS-EEPIQDFQLLQKISLNGQEIESLYFKFGFVIPQSQNSWDQVIQAD----------------------------------------
------------------------------RLEHMNMRDADSGQILWESADEGQEEAH-----VPREILACKSVSREVRFSS-VEKMDDFKLLQKIYFQGSCFEEWFFKFGFVMPNSTNSWQSTIEAA----------------------------------------
------------------------------KLISLTLKNSSNGKVAWTSKDTDAVDAH-----LPKEMLGYPAVGREIVFST-EEAIKEFRIQQDILVHGQNIEQWNYKFGFVIPGSENSWETIVEAA----------------------------------------
------------------------------QIQKVIMKDSETQQVYWDS--T---MIT-----LDPKIITSKAATRSIQFST-KEKLQDLILIQDAYLHDQKIEHFVFKFGFVMPETVNTWDSTIVNR----------------------------------------
------------------------------KIEKMKMASKSN--VLWES-NSNGEPIK-----IPKEILKCKQVQREIVFSC-GQALKNLSLVQQVKLHGKDIERWDFDFGFVIPNSVNSWESVIEAA----------------------------------------
----------------------------------MSMADGATGKQIWKSTDSHESEVH-----IPSSILKCSSVARCLEFST-IDAIQHLNLHQRVLLGDECFEEWHFDFGFVIPNSTNSWDMEMQAA----------------------------------------
----------------------------------MNMRDAETGEMMWEKHAPSEEEAR-----IPADVMNRAAVSREMTFSS-AAAIDDFRLRQRVYLGEHCIEEWDFDFGFVIPGSVNTWQTTIESA----------------------------------------
----------------------------------MDMRDADTARLLWESGEGKHEKAT-----VPRRILECSAVSREINFTS-QEEIRQFRLEQRVFLAGACIEEWFFDFGFVIPGSTNSWQQTIESA----------------------------------------
----------------------------------MNMRDAYSGRILWEQQG-ARELAR-----VPRSILSCRAVSREVNFSS-EQKMEHFRLEQRVLLHDQPFEAWNFQFGFVMPGSTNSWQQVIEAA----------------------------------------
----------------------------------MNLRDADTGKIFWQKEDSAREEAR-----VPKKLLKCRAVSREIEFWS-EDALDDFRLEQRVHFNGTVIEEWHFKFGFVIPDSTNTWQNTIEAA----------------------------------------
---------------------------------WMNLRDADSGKIMWQGHDPDGHEAR-----VPKKILKCKAISREVNFSS-LEPMHQFRLEQRVLFKGRCLEEWFFEFGFVIPNSTNTWQSTIEAAA---------------------------------------
------------------------------KLNWMNLRDADSGKILWQGSEDGEHEAR-----VPKKILKCRAVSREINFSS-IEQMERFRLEQKVLFKGRVLEEWYFEFGFVIPNSTNTWQSIIEAAPE--------------------------------------
----------------------------------MNLRDGETGKILWQSTDDGEHQAR-----VPKKILKCRIVSREINFSS-AEQMEKFWLEQKVYLKNEVIEGWQFEFGFVIPNSTNTWQSVIEAAP---------------------------------------
-----------------------------------------------------------------------------MNFTS-AEEIEQFRLEQRVYLDGSCFEEWRFTFGFVIPGSTNTWQQSIESAGRGNMLDASTI-----------------------------
---------------------------------------------------------------------------------A-QEPIEDFKLEQRVLYKDQHLEEWLFDFGFVMPGSTNTWQSVIEAAPQSQMMPASAL-----------------------------
-----------------------------LTINHMNMRCADTGRLMWQSEEWDEQKAR-----LPVAILKCSAVSREINFSS-VEEITKFRLEQRVFLAGSCIEEWIFNFGYVIPGSTNTWQQTIESAG---------------------------------------
----------------------------GFQVNWMNLRDADTGVVLWQGTEDL-S--------LP------------------DKE-----------HEGRCLEEWYFEFGFVIPNSTNTWQSLIEAAPESQ------------------------------------
--------------------------------NELTMGDADTGNKLWVCQDSQINSEKEIQANIPKEILQCKSVSRDINFSS-AHTIKKLSLKQIFSLNGQPIEQFSFDFGFVIPGSTNNWQSIIQAS----------------------------------------
-----------------------------IGITGFTIRNPSTGKIYFNRNTPDVPPTNGICYILPTEFLRASAIACQIVITNGDEPISGIKIVEEHILQGVPLQRYDFDLMFLAPRTQNTWETLYE-PEFKHNFEEQALAGPWIIIT-SL-YISDKLMLRNRAQLKF-
-----------------------------IGITGFTIRNPDTGKIYFNRNTPDIPPTNGICYILPAEFLRASTIACQIVLTNGDEPIDDIKIVEEHLLQGVLLQRYDFDLVFMAPRTQNTWETIYE-PEFKHKFEEQAVAGPWVIIT-SL-YVRDKLMLRNRAQLKF-
----------------------------------INLIDNDTGVVLWYTNEPGIEEAI-----MPRKVLDCRAMTRTIHFSS-EHRINHLKLKHREFLQGCCIEEWDSEFGTVLAKTAETWETTFIPS----------------------------------------
--------------------------------NWMNLRDAESGKVLWQSTEDSEHEAR-----VPKTILKCR------------------------------------------------------------------------------------------------
---------------------------------------------------------------IPAVILKCSAVSREINFSS-AEEITQFRLEQRVYLEGSCIEG---------------------------------------------------------------
