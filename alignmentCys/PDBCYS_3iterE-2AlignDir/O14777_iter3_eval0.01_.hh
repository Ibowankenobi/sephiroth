DPRPLNDKAFIQQCIRQLCEFLTENGYAHNVSMKSLQAPSVKDFLKIFTFLYGFLCPSYELPDTKFEEEVPRIFKDLGYPFALSKSSMYTVGAPHTWPHIVAALVWLIDCIKIH
DPRPLHDKAFVQQCIRQLCEIVHETAVS------SLETP-CTELVEVFNMTAFYLCKNIKLRENFFFFPLPALYHLV-FPVSSSRSSGEVQAVRELWVKILNSF-WLLLCLKTA
DPRPLNDKAFIQQYIRQLCEFLSENDYAFNVSMKSLQAPSVKDFLKIFTFIYRFLCPSYELPDAKFEEEVPRIFKDLGYPFAISKSSMYTVGAPHTWPQIVAALVWLIDCVKLY
DPRPLHDKAFVQQCIRQLSEFLLAYNYGTTANVKLLQSPSVREFTKIFSFIYQFLCPLYE-PSSKIEEEIPKIFKELGYPFALSKSSMYTVGAPHTWPQIVGALIWLTDCVKLF
DPRPLHDKAFIQQCIKQLCEFLVENGYAHNVTVKSLQSPSVKDFVKIFTFIYGFLCPSYELPDSKFEEEIPRVFKELGYPFPLSKSSMYTVGAPHTWPQIVAALIWLTDCFKLY
DPRPLNDKAFIQQCIRQLFEFLIENGYAYSVSMKSLQSPSIKDFLKIFTFIFDFISPSYELPDSKFEEEVPRILKDLGYSFTLPKSSMYTVGAPHTWPHIVAALNWLIECVKML
DPRPLNDKAFIQQYIRQLCEFLTENAYAYSVSMKSLQAPSVKDFLKIFTFIFDFLWPSYELPVTKFEEEIPRILKDLGYPFALPKSSMYTVGAPHTWPHMVAGLNWLIDCVKLY
DARPLHDKSFVQQCIRQLHEFLTEQNYPGTLSTKTLQSPSTKEFVKMFEFIYRQLDPTFEMPNSKVEEEVPAILKALRYPFVLSKCSMYSVGAPHTWPQALVALMWLIDNVKIN
DPRPLHDKAFIQQCIKKLCEFLSENAYVHNVSMKSLQSPSVKDFLKIFTFIYKFLCPTYELPDSKFEEEIPKVFKDLGYPFALSKSSMYTVGAPHTWPQIVAALVWLIDCVKLY
DTRPLHDKSFVQQCIRHLHEFLTEQGFPGILSAKMLQSPSTKEFVKVFEFVYRQLDPTFEMPNSKVEEEVPAILKALRYPFVLSKSAMYSVGAPHTWPQALGALMWLIDQVKIN
DPRPLHDKAYVQQCIRQLCEFLSEKGFPGSMTVKSLQSPSTKEFLKIFEFIYCLLDPTFQMPTSKVEEEVPRILKDLGYPFVLSKSSMYSVGAPHTWPQVLGAVVWLIDTVKIF
DPRPLHDKAFIQQCIRQLCEFLNENGYSQTLTVKSLQGPSTKDFLKIFAFIYTFICPNYENPESKFEEEIPRIFKELGYPFALSKSSMYTVGAPHTWPQIVAALVWLIDCVKLC
DPRPLHDKSFVQQCLRQLCEFLGEIGYPQPVSMKSLQSPSTKDFLRIFSFMNSTIDPSYQLPDSKFEEEIPRIFKDLGYPFPLSKSSMYTVGAPHTWPQIVGALIWLMDQIKLL
DPRALHDKAFVQQCIKQLYEFLVDRGFPGSITVKALQSPSTKEFLKIYEFIYNFLEPSFQMPTAKVEEEIPRMLKDLGYPFALSKSSMYSIGAPHTWPLALGALIWLMDAVKLF
DPRPLSDKGYQQKMIRTLTEFLTEFNYPHPISNRILSAPPMKEFKRIFLFIYKYVNDSSKVQEKKPEEEIPKILKMLGYPFNISKSSMFSVGSPHTWPNLLGALCYLIELIRYC
DPRPLNDKTFIQQCIRQLCEFLTENGYAHSVSMKSLQAPSVKDFLKIFTFLYGFLCPSYELPDTKFEEEVPRIFKDLGYVFSLGRSGRYIAVSPCFW-----------------
EPRPLSDKGYKQQSIKELMEFLVYNQFPKAISEQRLKGPSTREFVEIFQFVYERLDPCFPW-KLKAEDEIPTLLKAIGYPFSVKPSLIISCGSPHNWPKLLGALRWMIEL----
DPRPVSDKGFQQACIRTVIAYLAAHGFEYAITPKVLASPTTKDFTNVMMFLYRQFDPVLP-KTFKLEEEVPQLYKRLRYPFQISKSNLTAVGSPHTWPSLLVALTWLVEL----
DPRRIGDKAYQANCIRTLIAYLSTHGYDQPLTPKMLANPMGKDVINIMHFLMRQVDPHCK-NQGKIEDEVPALFKRLKYPYTISKSALFAVGSPHTWPGLLAALVWLTEL----
DPRPLNDRAFQQECIRSLTGYLLTHGYNVPISTKLLVSPASKDVLNIVQFLFQKVDPNLK-LSGKVEEDVPVVFKRLGYPFQISKSALYAAGSPHTWPGLLAALVWLIQL----
DPRPLSSKDYQASCIRAVITYLSTHNFPAAVAPKTLASPTGKDFATIVTFLFQQVDPTFR-IQGKVEDEVPVFFKRLNYPFQISKSALFAVGSPHSWPAVLAALTWLVEL----
DPRKVSDKSFQAICVAKLVEFLTEKGYPHKLSPEILKAPPRKDFFQIFEFLYSMLTPRYRI-GKKPEEEIPKIFKELGYPFMISKTAMYALGSPHTWPTILAALVWMF------
-----RDKGYQQRCIKKLMEFLIENSYPHQVSTKNLMSPSSNDFIRVFQFIYQFIIPNVAI-GKKYEEEVPNIFRSLGYPVTISKSFMFSVGTPHTWPILLQALTWLTEIVQL-
DPRPLSDKTYMADSIRNLMSFLTQHGYSTAVNVKMLSSPSVTDFQRMFKFLYAHLDASYPF-AGKLEDEIPVLFKRMKYPFTISKSSLFAVGSPHTWPTLLGALSWLVELV---
-SRPISDKSYMQKEIRRIIEFLQENGFSNPISVKQLMSPTTKDFVRVFQFLFSFLDEDYV-VGSKIDEEIPAKFKEMKYPFTISKSSMCTLGSSHAWPHILAALHWLFDSIQ--
DPRNVKDKAFINAAKHRLIEFLVENNYDRQISLKQLDAPTTKDYLHILMFLYNKIDPKFQLSQN-IAEDVPAMFRRLRYPFNVSKSHLQAVGSPHAWPSLLASLVWIVELL---
DPRPVTDKGFVNASIHQLLEFLSAKQYDAILSSKLLKGPTKKEACNILQFLFRQIDFNYAF-EGKLEEEIALFFKILRYPFPMTKTSMVTV-APHSWPPLLASITWIIELL---
DPRPLTDKTYMNGCIHALIEFLSDRQYDHVLSPKILKGPSKKDFYNIILFLFRQVDPTFEF-GVKFEEDVATQLKTLRYPFAISKTALVAVGSPHTWPALLGSIAWVIELL---
DPRPINDKSFQHNCSKALLTFLQESGYDYPISTKTLARPSGKDFANIVSFLLGQVDPEFSKGNVKFEDEVAMNFKAMGYPYPISKTALVAAGSPHTWPSLLAALHWLMEHLV--
DPREWSDKQYLKKAIRDLVVFLSETGYPDMISAKMLAAPSNKIFVKMFQHIYGLLDPMFPWDKYKPEDEIPEIMKYLGYPFPIKKSTIFASGSSHNWPKLLGALTWLVDATR--
DPRPLKEKQWQTKAIKQLVSFLAQTGCPVPISAKKLTAPSSKDFADVFKFLYRKLEPNHEF-KSKIEEELLSLLRTLRYPFAINKSQLFSIGSPHAWPTFLGILHWLME-----
DPRPLSDRRYQQECATQVVNYLLESGFSQPLGLNNRFMPSTREFAAIFKHLYNKLDPNFRF-GARYEEDVTTCLKALNYPFLISRSRLVAIGSPHVWPAILGMLHWVVS-----
DPRPVGEKAFTNLCARQLISYLSTHGFSSSLSPKAMTSPTTKEFASLVSFLFKVLDENFKL-GSKSEEDITTVFKQLRYPFQISKSSLYAVGSAHTWPYLLAALIWLVH-----
DMRLVREKGYKQQCVDTILGFLMENGYDGQISQKIMHNPSSKDFQSIFKFLYGFVDD-FV-FSSRFEDEVVNVMKNLRYPYCGTKSQL-SAITPHTWPVILSMCSWLVELIR--
DMRVVREKGYRQQCVDGILRFLVENGYDGQVSQKILHNPSSKDFQSIFKFLYGFVDD-FT-FSSRFEDEVVNVMKNLKYPYCGTKSQL-SAITPHTWPVILSMCSWLVDLIS--
DVRMVRDSNYKKSCIDNVYKFLSENNFEGNLSQKVLQNPSNKDFQMIFKFIFSFIDD-YE-YMNKFEEDVINILKILKYPYSSTKSQL-TAITPHIWPIVLSMLSWMIDLLN--
KQRNPREKTHQEENINIIINFLSSTEYDRPFSHALLSNPSSKDFQSIFRHIQSFID--AE-FTKRIEDEAPLFLRAIKYPYISNRSQL-IAITPHTWPVLLSMLGWLVKTVN--
EKRNVRSSSYQEQCITNVQSFLEEHNYENT-SHKSVRNPTLKEFQNLFKFVISFFYK-NW-ECKRFEDDAMLLIKQIKYPYVNNKSHL-VTTSPHSWPVVLSFISWMVDMVR--
DPRPVRDKAFQNSCVKTIGEYLAAVR---SMTPKTLVSPTAKEFHDIFRFLVNVINVGMVWG-KKFDDDANTVLKDLRYPSMETKTSFTAPGSPQSWPNLLAMLNWLVELCKA-
DPRPLRDKVFQSNCMRNVNEYLISVR---PLTAKTLTSPTAKEFQSIFKFLVNLVDPGAAWG-KKFEDDTLSILKDLKYPGMDSKTALTAPGAPQSWPNMLAMLNWLVDLCKA-
DPRPIRERSYQAKMRQDIFSWLQSTDY--EVSMQTLQSITGKDFRAIFNHLVTMIDPSYPFDPSRFEDEFLPSLKALRYPFSADNKWLAAPGSMHSWPFLMAALHWLVEMGKA-
DTRPLRDRQYQTKMRQDILSYLQSSGF--DITMATLANIQGKDYRAIFDFLILNLDPFHPLNQARFEDEFVPALKAVRYPFAHDNKWLAAPASMHSWPSLLGVLHWLVELNKM-
DPRPLRDKQYQAKMRQDILAYLQANGF--EITMSTLTNIQGKDYRTIFDFLLLTLDPYHILNQPRFEDEFVPALKALRYPFAHDNKWLAAPASMHSWPSLLGVLHWLVEMCKL-
DSRPLRDRQFQAKMRQDIVTWMIDNGL--EIQSGVLQSIRSDEFRRVFQQLVQLLDPQWSFKPQKFDEQFVQPLKAFRYPYLGDLKILATPGAMHAWPLLLGVLHWLAELGKA-
DTRPLRDRPFQAKMRQDIWAWLDRNGI--EVSPQTLREVTVKDFRAVYQHLVRLLDPEWPFGDETPEDQFANPLRALRYPYIGDRKTLATPGAMHAWPFYLGVLHWLAELG---
DTRPLRERSYQVKMRQDIIDWCRANDL--DVSPQILQNITAKDFRVIFEQLVQCLDPDWHFDPKNLGDQLIQALKALYYPYVSDLKWLSAPGAPYSWPSLLGMLHWLAEMGRA-
DPRPLREKSYQALMRRDIIEYLRSCGM--EITNATLANITAKDFRSIFNYLIQMLDPAYPFNPARLEEEFVPALKAQRYPFAHSNNWLAAPASMHSWPSLLGVLHWLVELCKL-
DPRPMRDRNYINELKELVHKHLLECA---QITAKTLTSPTTKDFQSMFRFLYTILDPAFIWA-RKFEEEVMMILRDLRYPVADSKTQLQAASAQHIWPGMLAMLAWLADMNKT-
DTRPLRDKQYQSKMRQEIFLFLQSAGY--EIAPSTLSNIQGKEYRNIFEYLVLILDPMYMFSTGRFEDDFVPALKALRYPYAHDNKWLAAPASMHSWPPLLGVLHWLVDMCKL-
DPRPLSSKDYQNYCIKTLIHYFTTHGFSYPVSPKTLASPTGKDFTAIVTFLIQQLDPATR-VQGKVEDEFPVFLKRLNYPYQVSKSALFAVGAPHSWPAVLGALTWVVEAL---
DTRNLMDREVQASMQRKIFDFLVASNYSM-VSEKLVRSPTRSDFARMFEFIFLQLDPQYTFQ--KIEEEMPRIMRNLGYPVPLKPSTMQTIGAAHTMPHLLGAITWLIDAINF-
DSRNLNDRHLQQQMQQKVLQYLRDGNYPQ-TSEKLVKSPTKAEFARMFEFIFQQLAPDFTL--RKIEDEMPRLFRALGYPVQLKPSTMQTIGAAHTMPHLLGAITWLIDLIQM-
DPRPVSDRSYLNSCVQALVEFLSDRMYDQTLSPALLRGPSKKDFCNMILFLFKQVDPTFEF-GVKFEEDVVLQFRNLRYPIPISKTSLAAVGTPHTWPTLLLSISWLIELL---
DPRPVSDRSYQYRCVQTLVEFLSEHMYDQVIAQALLRGPSKKDFENMILFLFKLIDPTFEF-KGKFEEDVVQQLRDLRYPISISKTSLAAVGTPHTWPMLLMAMSWLIELL---
DPRNLSDRHVQQQMQRKVLQYLRDENYP-QISEKLVKNPTKTEFARMFEFIFQQLAPDFTLR--KIEDEMPRLFRTIGYPLQLKPSTMQTIGAAHTMPHLLGAITWLIDLIQM-
DQRPLMDKAHLNASLHKLAAYLTDHGYDQPINSKSLTRPSGRDFNNITGFLFRQLDPNYR-PSGRFEDDVVPFLKMVRYPFTISKSSLAAVGAPQTWPKVMGAISWIVDAL---
DPRDIKDKSYLNNSIRKMVSYLKSHGYDSPLNVKQLNGPSGRDFNHFMTFLLRRVDPTFNDPPIKFEDEVSMAFRTLGYPFPISKTGIVAVGAPHTWPALIAAIDWLIDVLV--
DPRPLRDRNFQATIQQDIFEYLVNNNFEHPLSLKSLKNPTQKDFVMMFNFLYKKVDPGYKF-SKSIEHEVYYVLKSIKYPYLINKSQISAVGG-QNWPVFLGILHWLV------
DPRPIRDKQYQLMCIHSILAYLSSSGFPQVLTQKSLQQPTQKDFMTIFKWLYHKLDPNYQF-VKKMDDEVIICIKNLKYPFAISRSQLIAVGSPHSWPSMLSMLHWMV------
DPRPLRDKNYQSVLHQDIFDYLQTQKFDHSISLKSLKQPTQKDFIYIFRWLYQRLDPGYAF-KRSLESEVYSILKTIHYPYLINKSQISAVGG-SNWHKFLGMLHWLV------
DPRPLRDKNYQAVLQQEIFDYLQSRKFDHAISLKSLKQPTQKDFICIFRWLYRRLDPGYSF-KRSLETEVYSILKTIQYPFLINKSQISAVGG-SNWHKFLGMLHWLM------
DPRPVRDRHYQQQISQQIYEYLVTNHFEHPLNQRTLSNPTQKDFKTMFEWIFRRIDPGYPF-HKSIENEVHAVLRAAKYPWLITKSQIVAVGG-QSWAYFSGMLHWMV------
DSRPLRDKNYQSLISQEIYDFLTANKFEHPIMLKTLQSPTQKDFVLIFQFLYGRIDPNYKF-TRSIETEVFALLKTLNYPYLINRSSISAVGG-QNWPSFLGMLYWLV------
-SRPISDKEFQKQCTNRIATFLTSNSYPNQITPKTLKSPSGKEFYAMAEFMFLQIDPTFKF-TAKQDDELIAFFKIIRYPFPISKRNLTSVGTPHTWQHLLAALSWLVEVI---
DPRNINDKSFQKEATAELIEYLTTHGFNSPISSK---LPSGKEIKDIIEFLFRQVDPHIVF--GKLEDDVPGFFRSLNYPFQISKSALYAAGSPHSWPGLLAALAWLVNLL---
DARNVTDKAFQKQCIIKLVSFLTSHNYPSAITTKELMAPSAKDFYDITEFLFHHIDSTFKFTPTKKEDELIAFFKIIKYPVTINKRNLNPVGTQHNWPYILAALAWIADLI---
DPRPLRDRNFQLKMQQELYAFLSTIEMKHPLTNKTLKNPTQKDFVLMFQWLYKKIDPGYKFL-RSIEQEVYSLLKFLEYPYLINKSQISAVGGSN-WHIFLGMLYWMR------
DPRPLRDKSYQAMLQQEVHDYLMELEMKYPITIKSLKFPTQKDFVLVFQWLYKRIDPGYKFL-KSIEQEVYFLLKTLGYPYIINKSQISAVGGSN-WPIFLGMLHWLV------
DQRPLRDKNYQTLIQQEIYDFLLALEMNHPLTFKTLKQPTQKDFVVIFQFLYNKIDPYYRFT-KSIETEVFLLLKILNYPYLINRSQISAVGGQN-WPNFLGMLYWLV------
ESRPLRDKNYQALIQQEIYDFLLALEMNHPLTFKTLRQPTQKDFVLIFQFLYTRIDSNYKFT-KSIETEVFTLLKVLGYPYLINRSQISAVGGQN-WPNFLGMLYWLV------
DSRPLRDKQYQTLIQGEVYEFLRLVEMNHPLTTKTLKQPTQKDFIMIFQFLYNKIDPFYKFT-KSIETEVFTILKLLNYPYLINRSQISAVGGSY-WPTFLAMLYWLV------
DPRPVRDKAFQSLCATNIVNYLINSGYPSPITIKNILSPTTKEFQSIFKYLYNKIDPNFSFV-RRIDEDVKLCLKDIQYPYAITRSHLMAIGSPHSRPVILAMLHWMV------
DPRPLRDRNYQALLQHEIHDFLSALESNHPLTTKTLRQPTQKDFVVIFQFLYNKLDPCYRFT-RSIETEVFSLLKILNYPYLINRSQISAVGGQN-WPAFLGVLYWLV------
------------------------MSQETGIVRRPRESPTQKDFVYMFQWLYHRIDPSYRFQ-KSIDQEVPPILAQLRYPFQITKSSLSAVGSANSWYIFLGLLHWMM------
--RNTRDKIYKAACIENIVKFLTENAYDSAFSHKILGNPSNKDFQNIFKFIYSFID-ST--PFLKFEDDVLGILKLLKYPYCITRSQL-TAVTPHTWPVVLSMMSWMVDLIR--
--RNVRSPSYQEQCITNVLSFLDEHNYENT-SQKGVRSPTLKEFQNLFKFTISFFK-NW--ECKRFEDDAMQLIKQLKYPYIINKSHL-VTTSPHSWPVVLSFISWLIDMIR--
-------KSFQAANAHKIFNFLVESDGADAPAESVIRTPGKNDFIVIFESMYQHLSKDYEFPNARIEEEVSSIFKGLGYPFHLKNSYFQPMGASHGWPHLLDALAWLVDFIKIN
-------KSAQASNVTKIYNFLLEHEQSGAPPEKIIRQPGKNDFITMFEQLYQHLSKDYEFPQARVEDEFTGIMKGLGYPFPLKNSFFQPMGSSHGYPHLLDALAWLIDVISLN
-------RSLQAANVQKIYNFFVETDGAEAPSERSIRAPSRREFIVLFESMYQHLSKDYEYPEARLEDEVTQIFKGLGYPYPLKNSYYQPMGASHGWPHLLDALSWLVDVIKMN
-------KSLQAANVQKIYQFMVEADGDDAPSEQTIRAPGKTDFVMIFETIYQHLSKDYEFPQTRLEDEVIQIFKGLGYPFPVKPSYFQPMGVGHGWPHLLDALGWLVDFVKIN
-------KSLVSLNGSKIYNFLVEYESSDAPSEQLIMKPGKNDFIACFELIYQHLSKDYEFPRERIEEEVSQIFKGLGYPYPLKNSYYQPMGSSHGYPHLLDALSWLIDIIRIN
DPRPILDKNYMSTCIKELIFFLNRKGFGESLNIKLLSTPTSRTFFKILIFLLSEIDTNFDF-QKKVEENFSIFLKELKYPFPLPKGSLYVISSPHMWPNLLACLKWISEL----
DPRPLGEKSYTNNNIQYLMSYLSCHGH-TSLSPKTFTSPTAKEFATLSLFLFCKVDKKFKFG-LRAEDDISLIFKQLRYPIQVSKNALYAVGSPHTWPSLLAALTWLVQ-----
DTRPLSDKSYMKKCVFDLIHLLNDLHYPTSLSPKMLNPPTNREYQRIFEFLAITLAGNFRI--IKPDEDMMRFVKACGYPYSLSKSTLNNVGPMSSWPHALGVLMWLANQIKIA
DPRPLKDRSFQMRMGQELMEYMAQNDFSHTLSPNVMKSPTQKDFNYMFQWLYHRIDPGHRFLK-NIDQEVPPILKQLRYPFEITKSQLAAVGGQN-WSTFLGLLYWMMQL----
DPRRLRDSSTRAAMGQELMEFLTQRNFKHSLTHKTMSSPTQKDFNLMFQFLYHNIDPSYRFQK-NIDAEVPPLLKQLRYPFEISKSQLAAVGGNN-WSTFLGLLHWMMQL----
DPRPLRDSSYRARLSQELLDYLTQNNFKHSLTQNSVKSPTQKDFTLMFQWLYKRIDPGYRFQK-GIDTEVPPIMKQLRYPYEITKSHLVAVGGQN-WPRFLGLLHWMMQL----
DPRPLRDRSYLARIGQELIEYMTQHNFSHTLSQNVLKSPTQKDFVYMFQWLYRRIDPNYKFHK-SIDQEVPVLMKQLRYPYEITKSQIAAVGGQN-WGTFLGMLHWMMQL----
DPRRLKDASTRAQMAHELLEYLTQNNFKHVLSNKAMTSPTQKDFNCMFQWLYNRIDPSYRFQK-SIDQEVPVLLKQMRYPFEIMKSQIAAVGGNN-WATFLGLLHWMMQL----
DPRPLKDRSYQNRIGQELLDYLTQHNFNHNLSQNVIKSPTQKDFNYIFQWLYNRIDPSYKFMK-NIDQEVPPLLKQLRYPYEITKSQIAAVGGQN-WSTFLGMLHWMMQL----
DPRPLRDRSYQNRLGQELVEYLAQNNFSHKLSDNFIKSPTQKDFNFMFQWLYRRIDPSYRFQK-NIDQEVPPLLKQMRYPYEITKSQIAAVGGQN-WSTFLGLLHWMMQL----
DPRPLRDRSFQARISQELLEYLTHNNFKHSLGQNSLKSPTQKDFTNIFQWLYRRIDPSYKFQK-SIEAEVPPILKQLRYPYEITKSQIAAVGGQN-WSTFLGMLHWMMQL----
DPRRLRDRTVQNQMGQELLDYMAENNFNHVFQPNVMKSPTSKDFNYMFQWLYHRIDPSYRFQK-SIDQEVPPLLLQLRYPYAISKSSLAAVGSANSWHLYIGMLHWMMQL----
DPRPLRDRGFQARISQEILEYLTRNNFKHSLTQNTLKSPTQKDFNYIFQWLYKRIDPSYRFQK-NIDTEVPPILKQLRYPYEITKSQIAAVGGTN-WYTFLGVLHWMMQL----
DPRPLRDRAYQGKIGQELLEYMANNNYNHNVSANTLKSPTQKDFVFMFQWLYHRIDPSYRFQK-AIDQEVPPIMAQLRYPYQITKSALSAVGGANSWPLFLGLLHWMMQL----
DPRPLRDRSFQARISQEILEYLTHNNFKHSLTQNTLKSPTQKDFNYIFQWLYRRIDPGHKFQK-SIDSEVPPILKQLRYPFEITKSQLAAVGGQN-WPTFLGMLHWMMQL----
DPRPLRDRSFQARIGQELLEYLTHNNFKHTLGQNTLRSPTQKDFNYIFQWLYHRIDPGYRFQK-SMDAEVPPILKQLRYPYEITKSQIAAVGGQN-WPTFLGMLHWLMQL----
DPRPLRDRSFQARIAQELMDYMVQNNFKHSLSQNILKSPTQKDFNFMFQWLYHRIDPSHKFQK-NIDQEVPPILKQLRYPFEITKSQIAAVGGQN-WSTFLGLLHWMMQL----
DPRPLRDRSYQAKIAQELLDYLAQNNFKHTLSQNIIKSPTQKDFNYMFQWLYHRIDPSYRFQK-NIDQEVPPILKQLRYPYEITKSQIAAVGGQN-WSTFLGVLHWMMQL----
DPRPLKDRAFQARIGQEILEYMVNYNFKHVLSQNVLKSPTQKDFNYMFQWLYHRIDPSYRFQK-SIDQEVPPLLKQMRYPFEITKSQISAVGGQN-WSTFLGLLHWMMQL----
DPRPLKDRAYQQRIGQELLEYLAQNNFNYKLSDNFIKSPTQKDFNYLFQWLYRRIDPGYRFHK-NIDQEVPPLLKQMRYPYEITKSQIAAVGGQN-WSTFLGLLHWMMQL----
DPRRLKDPSVRAQMAQELSEYLARNNFKHSLSHKSFTSPTQKDFNCMFQWLYHRIDPSYRFQK-NIDQEVPPLLKQMRYPFEIMKSQIAAVGGNN-WSTFLGLLHWMMQL----
DPRPLKDSAYRQEIASKVFEYVVNNGFKHTLGPNSLKSPTQKDFSMVFQWLYKRLDPNYNFQK-AIENEVLPILKTLRYPYAITK-QLAAVGSMNSWPQFLGILHWMMEL----
DPRPLRDKNFIQQ---EIFDYLIENKFDHPISIKSLRQPTQKFFIILFKWLYNRLDQGYSFVSKSIENEVYHLLKNLQYPYLINRSQISAVGGGTNWYKFLGMIHWLV------
DPRPLRDKNFIQQ---EILDYLIENKFDQPVSIKSLKQPTQKFFIIVFKWLYNRLDQGYQF-TKSMENEVYQTLKSLQYPYLINRSQISAVG-GSNWYKFLGMLHWLV------
DPRPLRDKNFIQQ---EIFDYLLQNKFDHPISLKSLKQPTQKGFIIIFKWLYSRLDPGYQF-TKSIEYEIYQILKNLQYPYLINKSQISAVG-GSSWHKFLGLLHWLI------
DPRPLRDKNFIQQ---EIFDYLIQNTFEYPISLKTLKQPTQKSFVTIFKWLYLRLDPGYTF-TKSIEYEVYQLLKILQYPYLINKSQISAVG-GSGWPKFLGMLHWLV------
DPRPLRDKNFIQQ---EIYDYLIQNKFDYPITIKSLKQPTQKGFIYMFKWLYSRLDPGYQF-TKSIEQEVYQLLRTLQYPYLINKSQISAVG-GNSWHKFLGMLHWLV------
DPRPLRDKNFIQQ---EIYDYLHSQKFDHPISLKSLRQPTQKDFVFIFRWLYQRMDPGYKF-SKSIEHEVYTILRAIQYPYLINKSQISAVG-GSSWPKFLGMLHWLV------
DPRPLRDRNYIQQ---EICDYLTFNRFDHPISLKFLKQPTQKGFMIIFKWLYMRLDPGYKF-TRSVENEVYQILRTLQYPYLINKSQISAVG-GSSWPKFLGMLYWLV------
DPRPLRDRNFVQE---EIFDYLTRNKFDHPISLKFLKLPTQKGFVLIFKWLYLRLDPGYNF-TKSIEHEVYQILKNLQYPYLINKSQISAVG-GSSWPRFLGMLHWLV------
--------------LKTILTFLSKRGYPNQINPKTLNSPTRALYLEVLQFIISQYDPRIKI--SKPDDEIPRFFKDIGYPITINKTTIIAPGAPNTWPQHIAAMTWLCEL----
--------------LKTILTFLSKRGYHGQINPKSLSSPTRTLYLEVLQFIINQYDPKIKL--SRPDEDIPRFFKDVGYPFTINKTTIIAPGAPNTWPQHIAAMSWLCEL----
DPRTRDRK-TIASWQHEVYEFLQERGYPEPLTIKTLQTPTTKDFQNIFKFMIQCSDHGWGIYGKKPEDEVIPLLKSMGYIAALTKSGLQAPGSMHTWPTLLAMLQWIVTTIKL-
DPRPRDKK-TIAAWQQDVYDFLIERGYT-TITIKALQTPTNKDFQNILKYLVQCSDPGWGVNGKKFEDEVIPLLKQMGYVATITKSGLQAAGSMHTWPTMLAMLHWIVMTIKL-
EPRPRDRK-TVAHWQHEVYEFLQERGYPEPLTIKTLQTPTTKDFQHIFKFIIQCSDPGWGVHGKKPEDEVIPLLKMMGYHAVLTKSGLQAPGSMHTWPTLLAMLHWIVTTIKL-
-------------------------------------------------FLFQKVNPNLKLS-VEVEEEVPVVFKRLGYPFQISMSALYAAGSPYTWPGLLAALVWLIHL----
------------------VNYLRSTGYPDSSSVRLLGGPSGRDFQNIMTFLMRRVDPTFARTSIKFEDEITMAFRCLGYPFPISKTGLVAVGSPTHWPTLVAAIDWLVDLLV--
DPRKLSDRSLQHRMTKDIIEFLSMQGFPHQISQKLMSPPTTKLFTAIVQFLYGFLDPSFQI-LKKFEEEIPRIFKDL-------------------------------------
DPRPISEKTYMSSCIRDIIFFLEKNGYVKYLHPKFFYNPTSKDFFQILSFLLRKIDENLFL-GKKYEENFQWIFNTIYYPFPIPKSSRYSIASQDIWPILLSCLKWITEL----
DQRPFQDKAYIAESVEKLYAFLQEQAYPHHTTRQLLARMSTREFENVFTFLFKLLEPNFAVGGARLEDVVLERLRTLQYPYPLHKSMLTGIGSPH---KALAIITWLADVVKY-
DPRPLRDKEFQNRLIDEIYGYLKENRFDMPISLNTVRQPTQKAFVTIFKWLYNKIDPGYRF-SPSIEDDVYPLLKILRYPYISSKSQISAAGG-TGWSKFLGMLHWLIQ-----
DPRPLRDKNFQNAMQQDIINYLTSNKFELPITNKTLKQPTQKGFIITFQWLYQRLDPGYQFKNKSVELEIYQILKFLQYPYLDTKSQISAVGG-NSWHKFLGMLHWMVK-----
DPRPLRDKNFQNLLQQEIFSYLTDQKFDHPISLKSLKQPTQKDFIYMFKWLYLRLDPGYVF-TKSLEHEVYSILRTIHYPYLATKSQISAVGG-SNWPKFVGMLHWLVI-----
-TRPVNDSKFQSQCLLRLQQFLSEKGYPRAITKDNLMS----EINGLMKFLCLHIDPHFKLPTEKPEEEIISFFKMMGY-----RSAGFQVGNAHTRGHLLGAIIWMVELI---
-NRPINDGKFQQNCLSKIQQFLSEKCYPKQINKDNISN----EIFGIMKFLIHFIDPHFKLPPDKQEEEIIAFFKRMGY-----RSSGFTVPSVHNKGHLLGALVWIIELI---
----------------DVYNFLQERNYEHKVSLKMLTGPTSRDFWDIFRFIWSIYDPST-LPSVKWEDEAVYIIKGSGYPFAFSKSHLQAPAAMHVWPMMLGLLDWIIKTIR--
----PRDKGHQEENINAITNFLASTEYDRPFSYSLLSNPSLKDFQSIFRHIQSFIDPTIEF-TKKFEEEVPFFLRAIKYPYTSERSQL-IAITPHTWPVLLSMLGWLVKIV---
DPRPLKDKKYQELIQKEIIRYLIDYKFEIALTENILKSPTQKNFNAIFKFLYNQLDPNYMFIKSSIEQEIVTLLKLLNYPYMITRSHFSAVGG-NNWPTFLGILYWLVE-----
DPRPLRDRKYQELISKEIIRYLVDHKFELALTENMLKSPTQKNFNAIFQFLYNQIDPAYVFIK-PVEQEITGLLKILNYPYMITRSHFSAVGG-SNWPTFLGILFWLVE-----
DPRPLRDRGFQEQMQRHINNFLVSHDFERTFSENLLKFPTQSNFNEIFKFICFTIDSSFRFEK-TPEQEIFLTLRALDYPYLITKTQIGAVGG-HNWPTFLGILDWLVE-----
----AVNRARQEQMAQKIYKFLATNHFEVSLNENTLKIPTQRNFYEIFKFMYHFIDPSYVFRK-SAEQEIIPLLKSLQYPALISRLQFNAVGG-QNWPNFLAILEWILT-----
---TSPTGPPQAAALRVVNAYLAP----A-VSLRP-PLPSAKDIVAAFRHLFECLDFP--LH---FEDDLLFVLRVLRCPFKLTRSALKAPGTPHSWPPLLSVLYWLTL-----
---NFSDRATQAAALRIVNGYLSP----A-LTLRG-PLPAARDIQAALRLLLERIDFP--PN--TFEDDLIQALRLLGCPHKITRSALKAPGTPHSWPPVLAVLHWLTL-----
---NFSDRASQAAAIRVVNAYLAP----A-VSLRA-PLPSAKDIIAAFRHLLERVDFP--LQ---FEDDLLIFLRLLGCPYKLARSALKAPGTPHSWPPLLSVLYWLTL-----
---PISDRSYQLSALRTINAYLSSHSLP--FTLKH-PLPSAKDITETLKFILSRFGFSL-AESHKLEDDLQTLLKSLNCPVKLNKSALRAPGTPHSWPSLLAAIHWLVQ-----
---LYKDRSYQQSAVATINSFLSSHNFH--ISFKSSPSPSAKQIHETLIFLTKLLEF----PVTKLEDDLPLLLKLFNYPFKLNKSILKNPATPHQWPSMLCLIHWLVQ-----
---LYTDRGHQNSAVRAINTYLSSHSSS--LSLRTHPISSAKDITEVLQFLLHQLDY----PTTKLEDDLFLILKFLNCPFKVSKSALRAPNTPHNWPSFLALIHWLVQ-----
---SLSDRASQAAALRAVNAYLAP----AAIRLRP-PLPSAKDIVAAFHHLAHRLRYP--LM--GWEDDLIALLRALACPYKVTRSALKAPGTPHSWPPLLSILYWLTL-----
---PYTEKAHQASAIRAINAYLSSHSSK--L-LPPNSTPSGKDITETLKYLLHQLDY----ESTKLEDDLASILKSLNCPFKFNKSTLRAPNTPHNWPSYVAIIHWLVQ-----
---LYNEPSYKQTVVSTINSFLSSHNFP--ITFKTT-LPSAKDIHETLKFLLSLLDF----PFSKLEEDLPPLLKRLNYPFKLNKSILRSPAAPHQWPTFLALIHWLVQ-----
---GLSDRASQAALLRAVNAYLAP----AALHLRP-PLPPAKDIVAAFHHLAARLRCP--LK--AWEDDLLALLRALACPYKVTRSALKAPGTPHSWPPLLSILYWLTL-----
-----DDRSHQSSMIRVINTFLSSHDFP--ISIRGNPVPSVKDISETLKFLLSALDYH--CDSIKWDDDLVFFLKSQKCPFKITKSSLKAPNTPHNWPTVLAVVHWLVE-----
-----DDRSHQSSMIRFIKSFLSSHKCP--IQIRFNPAPSVKDISETLKFLLEALDYP--CDSNKWDEDIVFFLKSQNCPLKITKSSLKAPNTPHNWPTILAVIHWLVE-----
-----SD--DRSSMIRFINAFLSTHNFP--ISIRGNPVPSVKDISETLKFLLSALDYP--CDSIKWDEDLVFFLKSQKCPFKITKSSLKAPNTPHNWPTVLAVVHWLAE-----
DSRPLNDKNYQNEMLGKIDNYFHSIGQSAILNGGSLKPLTLKIFVEATNLLVKLLDMKQSLTAANYIEEIPKIAKKIHYPGLMNKSWLKTANTMHSWPHAIGWICWLVEL----
DTRPLTDKTYQMTLLNKIDSFFYTNQCSSILNSGSLKPITLKMFVDVSNILLKYLDIKQDFTMTNYIDELPKCTKKLHYPGTVTKAWLKTANAMHSWPNVLGWIGWLVEA----
DTRPLLDKTYQAALLNKIDNFFYVNQYSAMLNSGSLRPITLKMFVEVSGFLLKYLGVRHALTMANYVEELPKCAEKIHYPGKI-KSWLITANAPHSWSNVLGWIGWLVEA----
DTRPLTDKSYQATLLANIDNFFHTNECSDILDRGSLKPITLKIFVEASNHLLKFFKIKQELTILNYVEELPRIAKQLHYPGVMTKSWLKTANAMHSWPSVLGWIGWLVEA----
DTRPLSDKSYQMELLNKIDNFFHVNQRSNMLNSGSLKPITLNMFVETSNFLLKFFDIKQELTTKNYIEEIPTSAKKLHYPGIISKSWLKTANVPHSWPNVLGWIGWLVEA----
----------------------------GEFAMDDVKQPTRRKFLEIFTYIYGSIEDNYTMLEGKKATEIIKVFKNLGYPVTIKNSTLTSLNAPTSWPHLLGALTWMVELVEF-
--------------VKTIIRFLCYSGFPQQLSPKIFVAPPRNLLVEIWNHLLRRACDSVQVTNENANEEVPRLFKELGYPLTIAKSSMQAPNSAHQWPLHLHALSWLCEL----
------------ECVQCILAFLGTKGFQ-PCSAKLLRSPPLQTLLDIWNFLFRLLDPSANVTKDNMVVEVPKFFKEFAYPHIMKTSNLRTPTADHQWESNLIALSWLC------
------------ECVNTILQFLAWKGYQ-LCSERLLRAPSLGVLLQIWNILFKFVDANVEITRENMTVEVPRFFKDFGYPLIMETSHLRTPTADHQWERNLLALSWLC------
------------ECVTVILNFLAWKDYH-PCSEKLLKSPPLNVLLNIWNFLFRLIDSNVNITKENMAVEVPKFYNEFGYPHTMKTSHLRTPTADHQWESNLVALTWLC------
-TRNLSDKNCVLEMAKNVAQFLNGSGFPESVTHKEIQKIDKQSFMKYFNYIYQFIDHNYQLAPRFNEDELIKNMKDLGYCGTLAKSALVSIGALHSTSQIAGLLSWLCDLIR--
--KHLRDKSTINCSIQHLETFLKAHSFTQTINAKPLMCPSAKEVSSIFLFLFRFFDANFA--FNKYEDDVINFMKILRYPFISNKSQ-FIAVTPHTWPILLAALVWIIGLI---
DPRPLRDKNFQSAIQEEIYDYLKKNKFDHPISIKFLKQPTQKGFIIIFKWLYLRLDPGYGFTK-SIENEIYQILKNLRYPFLENKSQISAVGG-SNWHKFLGMLHWMVR-----
DQRPLNDKQFVANMIKEVSAFFLEAIMG---------------------------CP---IQINKFEEDILRYLKELEYPYLVKKSTLQTLSAPSSIPHIIGIFYWFVEEIKV-
--RNVRDPSFKSQCIEFITNFLTKNNYEGPI---SLNNPTTRDFQTILKFLIAFID--IEM-TNKFEEEIVFFLKMVKYPYSITKSQLSS-ITPHALPNLLAMTVWIIQLL---
DPRPLRDPNFRNKMAQEIQEYLLRYNFKHNLGPKALSSPMQKDFTTIFQWLYHRMDPNFKFSLKSVETEVMPLLKGIKYPYLITKAQLGAVGGANNWPTYLGLLHWMVQLV---
DPRPLRDRKYQELIQAEIHSFLTENRFELEPSIKTLKTPTQKEFTGIFSFLVKMQNPSFKLGKAP-DTEIYTALKVMNYPYLISRSQIGAVGG-QNWPTFLGVLYWLVT-----
DSRPVRDRTFQSECQRNIEDFLAQRGVQLALSSKWLVSPTQRDFQTLFRYLVEELMGTGYPWSKKVEDDFIAILRDLRYPAMIGKTALTAPG-GSNWPHVLAMLNWLVELCKV-
-------------MLRGINEFLRKQGKD--LTLRTQPTPSAKEFQDAFLFIYNLVFEDDRAPTGKMDEEVIELLRQARYPWMLSKLS-FSIIQPHIWPHLIGALSWMV------
DDRPIKNKAYMKRCVMDLIHLLTDLGYPNTVYPKMMNPPTNREYQRILQFLVTTICNHH---MDKPDEDMLHLVKALGSPYMISKS----------------------------
--REVKSKANITKMIGSLIQFLETTDYSNSLSPKILHSPNSKEIFSIFEHLVRQVEPSFRIENDKAEEVCLNYLKILGYPNILSKHHLLAPGAPQAWNSVLAAFDWFRE-----
DPRNLQDKEFQRE-ASQLVQYFKEKSPEVLINS-SEIKPTIKMFIAMINHLFGILDKGIEFAASTFEKELPYWMKKLEYPGLITKAWLQTVNVPQYWPHVVGLLSWLV------
DTRPLTDKTYQAYMLNKIDNFLSINHCSSMVNTNNSFKPTLKMFVEVSAYLLKILDIKQILTISNYVEELPKIAKKLHYPGIINKSWLKTANAMHSWPNVLGWICWLVEI----
-VRPLTEKDFQQTAIHKIQNFLTAPNASHILNNGSMKPMTTTMFTDMCEYLLHKLDRNQVLNKSKYMEELPKIMKKYYYKGKVDKSWLITVNASHSFPQVVGLLLWLVE-----
---ALTDRSYQLSAIRTINSFLASHSSP--LSLKP-PLPSAKDISETLKFLISQL----DFPTTKLEDDLPILLKHLNCPIKLNKSALKAPGTPHAWPPLLGVMHWLVQ-----
---------------------------------------------------------------------SPRILRTLNYPHALSRQYLATPGAAHAWPSILGALNWLRELV---
------------DNVKTLIRYLGWKNYSGTISPNLFKNPSMTDLINIWNFIFKHVDPLIEVNKDNYGEVVLSFYRDIGYPYTISKST---LVAPTTGLQHLSALAWLC------
DSRPLRDKNFQLAIEEEILQYLQAHGFDGALTLKILQTPTQKTYMLIFRWLYARVDPNYTFKRS-VEAEFYDCLKRLGCPFLINKSQISAVGG-SQWHKFLGLLHWFV------
DERPLQSKGYRKACAEYVYAYLSKHGYR-SDRMKWFDSPSTREFFSLCEFLLERIDPNLIVPQKKPEDLISIVRTKLLYPYDVPKSALASVGAITTWPALLGLLSWLVE-----
-----------KECVNFILHFLAWKGYNACSVSDLLRSPPLKVLLDIWNFLFRLVDKNVTITKENMAVEVPKFYNDFGYPHIMTTSHLKTPTAERQWESNLVALSWLC------
----------------------------------TIKPPTINSFVAVFNLLFKEIDPRFEVNTTNYKDIVLNTLKIYQFPGNISLSLLKTVNTMHAWPQVMGILGWLVDLVT--
---------------RPIFDYLAQANVPDFIERGNIKTMSMKQFLIIVAHLLRQIGGHYKIGTN-FIEDIIKAITELQCPFTVSKSMLKTPSAPHSIQQVVTMLTWLVQL----
---------------RRICDYLV--TIPEFVERRNLKAMSTKQFLLIMSHLFKQIGGSYKMGNN-FIDVIMKTMAELEYPFTVNKSMLKTPNVPHSINHIIVMIGWLIQL----
---------------RRICDYLE--TIPDFVERRNLRAMSTKQFLVIMNHLFRQIGGNYKLGTN-FIDDIMKTMIELEYPYTINKSMLKTPNVPHSVNHIIVMIGWLVQL----
---------------HKVFEYLAQSEIPEFIERGSLRSMSMKQFLIIVAHLLRQIGGSYKIGSN-FIEDIMRAITELQCPFTVNKSMLKTPSAPHSLGQITLMLTWLIQL----
----------------------------------------GKDYRAIFEYLIQCLDPSYPIPSGKFENLFIPALKALRYPYVNLLDNKWAPASMHSWPALLGVLHWLVELCKM-
DPRPIYDEDYLDISKGKFSSFLKKRGCT-INSLDSFGSYSFKSAIAVFHFLLNQLDQNLKF-GQKADEDLPLILKSLNYPVIIPKDTFF-------------------------
----------------NLMAFLERTGFTMGWSAKFVHEPTQSAFVNMFKHIYNTIDPSYQMGAKKFEEEVILLMKEIRYPFILTKTKLTAAGSQQNWPACLAMLDWIV------
---------------------------------------TLKTFVKVSNFLLKYLDVKQDLTMEDYIDELPKCARKLHYPGTITKSWLKIANTMHSWPYVLGWIGWLIEAC---
---VFHDKKWVQEQAQMIAEYLDEMVQTYALSNDTLRQMTMKQFVGIVNFLLHFIWPNRTTVGSNHVEDITNALHKLNYPYQVNKSWLMTPTTQHSFGHVIVMLDFLKDFA---
---ALHDKKWVQEQSQLIAEYLDEMNHLAILPGEALRQMTMKQFVSIVNFFFQFIWGNRTSVGNNHVDDITSVLHKINYPYQVNKSWLMTPTTQHSFGHVIVMLDFLKDFA---
---APHDKKWVAERAQQILEYLHGIQHSEAPTGDGLRHMTIKQFVSILNFLFHHIWRNRVTVGQNHVEDITSAMQKLQYPYQVNKSWLVSPTTQHSFGHVIVLLDFLMDFV---
---PHHNKKWVSEKCQQIQDYLHAILPTHSIQGEGLRQMGTKQFVGIINFFFHQIIRNRVTVGSNHVEDIMNTMQKLNYPYQMNKSSLMTPTAQHSFGHVIVMLDFLMDF----
---PHHDKKWVAERTQAIVEYLDAVHSDQPIPGDCLRQMTMKQFIGIVNFFFHNIWRNRVTVSSVNAENIMSALTKLKYPHQINKSSLMTPTTQHSFGQIIVMLDFLMDLA---
---ALHDKSWVSERAQQILEYLSNMQGALQCLGEGLRKMTFKQFLGILNFFFQNVWRNKVTVGSNPVEEITAALQKLHYPHQVSKSWLSTPATQHSFGHVIVLLDFLMDFA---
---VLHDKKWVQEQSQLIAEYLDEMAQIHALQGEALRQMTMKQFVGIVNFFLQFIWRNRISVGTNHVEDITRALHKLNYPYQVNKSWLMTPTTQHSFGHVVVMLDFLKDFA---
DPRLKRDRSYQAKMQKDLFDYLTANKFDQQLT-RTLRSPTQKEFIL-FQFLYK-IDPGYRFV-RSSEQDIYSILKFLEYPYLITKSQLGAVGG-TSWPTFLAMLHWIMQLVQ--
---NLSDRSSQAAALRVVNAYLAPNIH-----LRA-PLPAARDILAAFRHFLERLQ--YPV-QTSLEEDLLILLRLLACPYKLTRSALKAPGTPHSWPPLLSVLYWLTLLCRVA
---PRRDPAFRAECRANIDQFLYSHGLP-PLSSKT-KDPIQKEIEATFRALSEILGPTT--RGKKFEDDCTAMLRDLKCPYLLSKSAIAVAGSEKYWPLMLATLSWMVDLCKA-
---NFSDRATQVAALRVVNTFLAP-----AVTLRG-PLPAARDIQAALRLLVDRLHLPR--NDATFDDDLIQDLRLLGCPYKVTRSALKAPGTPHSWPVLLSVLHWLTLLCH--
ETRPIKDKHYKTRMGLTVKEHLERTGFTMWDANKGVHEPTQSAFVGMFKHIYACIDTNFVMGGKKFEDEVLTLMKEIKYPAALSKTKLTAAGSQSHWPYCLAMLEWMVNLGN--
DPRDLIKKPKQKEFCEKLQNFLLEINFR-EVTVKELSTMTTGMFHEIFSALVRNLGPNYLSGKMKVPEMIVETMQALNYPIKLVKSSFVNLT-KQTFPTALGVLDWLSDLIVL-
--------------LQDLLSFINLNIYQSPISEQQLKNPTQKIFFEIFWGFIRIIDPSINS-KVISVDEIPQLMKFWNCPFQLNKNILTTIGAPHTLKQTQNLLFWLYQLSK--
--------------IKLILKFLEWRKYP---TPKSLENPTVNDIIDIWNCVIQTIDPNMIVTRNNMAMIVPSFLHIFSCPFTVNKASMAAPHLGAGLSANLHFIGWICKLL---
DQRPISNVAWQRAECGRVQAALAAGGGG----MALIRPLTIARFVDITGALLASVIGDAKLNNDNYITKLPHLSKRLLYPGTVSKSWLRTVNTLHAFPHALALIAYLLDLVS--
DNRPWHNVEYRTQKWNELVSFLEQLNYEDKVTLRDLELPTSREYIIMVELLARRIIPNFVVHEGREEQTLSLLFSMLGHNFNA--NQLSHVSAPHCWSFMLGSLTWLKDAG---
DQRPISNVAWQRSECARVGEALAKRES----SMALIRPLTITRFVDTVGALLTAITKDAKLNNDNYVTKLPHLSKRVLYPGPVSKSWLRTVNTLHAFPHALALVAYLLDLVT--
---------------------------------------------------------------------------ALRLLVEVTNSALKSPGTPHSWPVVLAVLHWLTLLC---
DPRPFHEREYARACQENLNVFLTERSCPFHIPEQEFNTAVPGDIDSVVLFVLKLVEN--DV-GADRVEHFKFALKCLKYPFPVNTTALGKKYS--SEQHVIPFLTWLIYLFV--
DPRPFHDEEYARSCWRRSTDFVAAFSSTRNTGDYTTKPSDTGDIDSLIRSLSRLVGY--EVNGAEDMEHFHFVLKCVKYPFWLNNLTLGHKSG--QERQLVANFSWLVMLLR--
------DKESVGPKLKRLTAYLQHGGWG-SSTEQRLRQMTIADFVSIVRHLLPLSGATVGMSKACYAEQLIEALQQLKYPKKVNRTWLLMPGTQHVIGHVLDLLDFLLDFA---
------------KHLKEILEFLRNVTI---LSDKNIKTINGQQAQYIIFHLIKVFAPNYTT-SPKFEEEIQSLFKLLNYPGSIRSDAIKAVGAQTSIAFLMRALYWLYLVTKI-
-----------------------------------------SVYLNILQLIMNVIDPYLEI-KSFSDEKFQLLLKRLGYPCKMSKTRFMSLGSSHTYLQSLHILSWLCRQ----
DSKKTNEENFISKSLKKIDNFLKRFCYNIKIDKKMFRYIGLKI---IIIKMIMILDYNCNL-STNLAFDYYKILKTLGYPFEMSIFSFSSKLNSRTVLSVITSLIWLTELIRY-
--RVISDEIYITFCVYDVLFFFFQDYFSVNLYRDLFFNCKKKYIIVVLSFLVEKIIFYFKH-INKVQKNAFFLIKFLGYPFLIHKIYFLLISRCCSWNVLTNFLEWLVNLV---
DPRT-RDRKTIASWQHQVYELLLERGYSELLTIKTLQTTTTKDF----------------------------------------------------------------------
---------AMMKMEARFLAYIEQIAAFNSISKRGILKMNVNELVRILSHLLAFTGVRQPLGKENYIEQTIKAMQLLKYPHKINKSCLLVPSAAH---HLMLILDFLLDLA---
