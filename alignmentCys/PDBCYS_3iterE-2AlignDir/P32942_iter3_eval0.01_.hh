QEFLLRVEPQNPVLSAGGSLFVNCSTDCPSSEKIALETSLSKELVASGMGWAAFNLSNVTGNSRILCSVYCNGSQITGSSNITVYG
--LQVWVHPPNPVILSGDSVVLNCSTNCPTEAKIGLETRLPKIVEDQGKNWRAFRLRNVTRDDSPWCFVNCTKNQSSTRTHILVYE
--FSLQVEPQDMVVSAGGSLLVNCSTDCPHPKSITLETYLFKETVGSGLGWEAFQLNNVTGDSKVLCSVFCNGSQITGSSNITVYR
--FQLRAEVQSPVVPAGGSFLVNCSSDCPNPELITLETSLSKEPVGNGLGWAAFQLSNATTDSQVLCSGFCNRVQMIGSSNIIVYR
--F-IRIWPQNPMVEFNGSVVLNCSSSCK---AISLETSLKWVQAGTGTNWKAFNLTKVDQTARPLCFARCDGYRKLLRANFTVYR
--FEVSIWPNQALVEYGQSLVVNCSTTCPEPGPSGIETFLKKTQVSKGPQWKEFLLENITKNSFLQCFFSCAGIQKDISLGITVYE
--FPLRVEPQDPVLPVGKSLVVNCSTQCPHPELITLETSLLKELVGNSLGWAAFRLSNVTSNSRVLCSGFCNGSQVTGTSEITVYR
--FSLRIEPQEAVVRTWSSLLFNCSSDCPNLEKLGLETSLHKAVNASGTHWRTYVVKAGADQGRVLCFANCNGTQMQTEVNITTYE
--FSLQMEPQDPVLPAGRSLLVNCSTSCPHPELITLETSLPKEVIGEDQGWAAFRLSNVTGDSKIICSAFCDGFQMTSSSNITVYR
--FEVSVWPDQALVKYGQSLMVNCSTTCPDPGPGGIETLLKKTQVGKGPQWKEFLLEDVTQNSILQCFFSCAGIQKDINLGITVYQ
--FLLRVEPQNPVFPAGGSLLVNCSTDCPSSKKIILETSLSKELVDNGTGWAAFQLSNVTGNSRILCSGYCNGSQITGFSDITVYS
---QVSIHPTEAFLPRGGSVQVNCSSSCEENLGLGLETNWMKDELSSGHNWKLFKLSDIGEDSRPLCFENCGTTQSSASATITVYS
--FRLRVEPQNPVVPAGGSLLVNCSTDCPQPELISLETSLFKEEAGEGLGWKAYRLSSVTSDSEIFCSGICNGSQMSDSSDITVYQ
--YQVRVEPKDPVVPFGEPLVVNCTLDCPGPGLISLETALSKEPHSRGLGWAAFRLTNVTGDMEILCSGICNKSQVVGFSNITVFG
--FLLQVEPQNPVLPAGGSLLVNCSTDCPSAEQVTLETSLSKELVDNGTGWAAHRVSNVTGNSRILCSGYCNGSQIIGSTGITVY-
--FPLRVEPRDPVLRAGASLLVNCSTDCPDSDLITLETLLSKELVGQGQGWVAFLLSGASDDSQVLCSGYCNGSQMTSTSHITVYE
---QVSIHPREAFLPQGGSVQVNCSSSCKEDISLGLETQWLKDELESGPNWKLFELSGIGEDGSPLCFENCGTVQSSASATITVYS
---EISMWPLNTIIPKGGSMKVNCSVACDGNTSFGLETHWHKTEVDHRDKWKIFELSNVENDGTLLCHAVCQGNQTQVQGNLTVYW
---QVSVSPSKATVPRGASVLVNCSSGCDREQNLGLETELTKVVVDTGPTWQLFNLTEVLEDSRPLCFLTCPERQATATVSLTVYA
---GISIHPSKAIIPRGDSLTVNCSNSCDQKSTFGLETVLIKEEVGRGDNWKVFQLRDVQEDIELFCYSNCHKEQTIASMNLTVYW
---QISVHPPEATIPRGGTVQVNCSTSCDQTPKLGLETQLTKKEVAHGSHWKVFELSDVQEDSHPICFVNCRS-QLMAPMSLTVYW
---QTFVSPSKAMLPRGGSLQINCSSSCDQKATVGLETPLTKR-LTRGDNWIVYELSNVGRDSKPICFSNCNNNQSSVGASLTVYW
---QTSVFPPEVILPRGGSVKVNCSASCDQPISLGMETPLPKKEIPGGNNWKMYELSNVQEDSQPMCYSNCPDGQSSAKTLLTVYW
---ETSVSPTKVILPRGGSVVVKCSTSCDQHTLLGIETPLPKKELPGGNNWKMYELSNVQEDSQPMCYSNCGDRQSTAKTFLTVYW
---QTSVDPAEAIIPRGGSVQVNCSTSCNQTSIFGLETLLTKKELNSGDNWVLFELTDVQEDSKLICFSNCHE-QTMAPMHLTVYW
---EISIHPPKAIIPRGGSLRVNCSISCDRKTTFGLETVLNKEEVSRGPNWKVFELSDVQEEINPLCYSNCHGEQIVASMNLTIYW
---QVSIHPKEAFLPRGASMQVNCSSSCSENLSLGLETQWPKVELDHGHNWKLFELSDIGDDSKPLCFENCGPIQSSASATIVLYS
---QTSVHPKEVIIPRGGSVLVNCSTSCDQYTLLGLETHLDKKEVASGSNWQMYELSSVQGDTTPFCYSVCHNRQSEATMSLTVYW
---QISVTPPEAIIPRGGSVTVNCSASC---AELGLEIELSKKEVA-RGNWKTFELSDVREDISPICYANCP-NQTESSVSITVYG
---EVSASPREATLAQGGSIQVNCSLVCDRKDSLGLETDFSKVLVDYGTNWRLFNLSGVDKDSSPICYLSCPGHQGTARVSLTVYV
---HTSVSPANVFLPRGGSVLVNCSTSCDQPTLLGIETPLPKKELLGGNNWKMYELSNVQEDSQPMCYSNCPDGQSAAKTFLTVYW
--FEVSLWPNQTLVEFGQPLMVNCSTTCPEPGPGGIETLLKKTQVDKGPQWKEFLLEDVTENSVLQCFFSCAGIQKDTSLGITVYK
--FSLRLEPQDLVVPEGGSISVNCSTDCPDAQHVTLETSLPKEPLSSGLGWAAFRLSNVTDDREVLCSSFCNGLQIAGSFGITVYR
---QTSAHPPEAIIPRGGSVQVNCSTSCDQPPTLGLETQLSKKEVAHGDHWKIFELSDVQEDSHPICFANCHA-QTTAAMTLTVYW
----------------GASMQVNCSSVCSENLNLGLETQWPKVELDRGETWKLFELSDIEEDSSPLCFENCGPIQSSVSATIVVYS
QQFPLKVEVQSPVVPAGGSFLVNCSTDCPNPNLITLETSLSKEPVGNGPGWAAFQLFNLTGDTQVLCSGFCNGVQMIGFLIITEY-
EEFEVHMYPEQLVVEPGGSQLINCSTSCANPNIGGLETTLTKTLNQSGPQWKQFLVSNIYQDTVVHCYFSCFGSQKLKSLNVSVF-
QMFPLWLEPPAAVLAAGGSVLVNCSTECPSPSNLILETSLPKQLNGSSPGWATFWLSNLTEDTRIMCSVLCGGSQVTASSNITIYR
QEFQMRVELQPPAVLPGESVLVNCSTDCLHAKLISVETYLLWEPVGSGRGWAAFQLNNVTGDTQFFCFGLCDDFQIVRSSNITIYR
-LFQVSVHPNEALVEFGHSLTVNCSTTCPDPGPSGIETFLKKTQLSKGSQWKEFLLEDITEDLVLQCFFSCAGEQKDTVLAITMYQ
-LPSVQMDPKVLVVRLGTSLNISCSSSCPFPKEDGLETSLTKQTLKEGNYLKVFKVTSAVSGSVL-CWANCNVTQSSKKMEVTVYD
-----HLESPAAVPAAGGPVLVNCSAGCPSPSLLGLETSLPKQLNGSG-------LSNLTEDSTIICSALCGSSQVTASSNITVYR
-TFEVYVWPERLAVEAIGSRKVNCSTSCVKPESGGLETFLSKHLVDQQPQWQQYLISNISQDAELICYFSCAGKTLTKSLNISVYQ
-TFEVHIWPEKLVVEPTGSKRVNCSTSCAQPDRGGLETYLTKKLVDHQPQWQQYLVSNISKDTKVICFFSCAGKTLSKSLNISVYQ
-AFEVYMWPETLVVESTESQLVNCSTSCNQPEKGGLETILHKTLLEQGPQWKQYLVSNISQDTKLLCYFACSGEQKSRVANVLVYK
-TFEVRMWPERLVVEPRGSWEVNCSTSCTQPEMGGLETTLTKTLLDSQPQWKRYLISNVSEDTDLQCYFLCAGKHQSKHLSIIVYQ
-AFEVHTWPERLVVENGGSRTVNCSTSCANPVTGGLETTLDKTLLEEQPQWKSFRISNVSQDTVLFCYFTCAGEQQSKSLNVSTYQ
-AFEVYVSPEQLVVKHGEYSEINCSTSCLQPTVSGLETTLSKILLEKQPQWELYRVFNISKDTVVYCHFTCSGKQLQRNASITMFY
-VFEVHTYPEQLAVKPGESQFINCSTSCSRPEAGGLETILAKTLLKSGAQWKEYLVSNISQDTIIYCYFTCFGEQRLTSLNVSVFC
-GFEVRMYPEQLLVEPGGSKFINCSTSCAQPEAGGLETALTKSLLQSGPQWNQYVVSNISQDTVIYCYFTCSGRQKLKSLNVSVFY
-AFEVYIRSEKQIVKATEAWRVNCSTSCAAPEIGGLETPTNKLMLEEDPHWIEFLVSNATVDTVLLCHFTCSGKQQAESFNIRVYE
-AFVVYMSPEQVVAEPNGSLEVNCSTNCSQPQLGGLETSLTKTVLDDQPQWKRFLVSNISQDTSLECYFVCSGQQLSKNATVSVYH
-AFEKPTWTKQLVVESGRSQVINCSASCTQPENSGLETTVSKKLLKEGAQWKLYEV-NVSQDTILLCYFTCFGRQEIKRFNISVFS
-LFDVTVWPEWPTVEAGQALWINCSTNCTHPEKGGIET-INKAVWKEETHWKAFYLADIAQDSHIICYFTCSGTQKKKSANITVYR
-AFEVHMRLEKLIVKPKESFEVNCSTTCNQPEVGGLETSLNKILLLEQTQWKHYLISNISHDTVLWCHFTCSGKQKSMSSNVSVYQ
-LFEVSVWPEWPVVEAGKALWINCSTNCTYPTKGGIET-INTTMQQEEAHWKVFHLSNISQNTNIFCYFICSNTQKKKSVNIIVYR
-AFEVYVQSEVLTVESGMTWKVNCSTNCTEPEMGGLETSLTKNLLDEQAQWKQFLVSNVSQDIDLHCYFICSGEQQLKTLNIGTPL
-AFE---GPEHLMVGSGEFQMINCTASCTDPTNLILETALNKTLLESQAQWKLFQVYNISKDEELLCSFTCAGKQEMKVFNITVFY
--TQVWVDPPNPVILLGTKVIVNCSTDCADPLDIGLETRLARNVESSGTSWKAFSLSNVTQDNSPLCFVNCPKRQGSARANIMVYE
-VFEVHVRPKKLAVEPKGSLEVNCSTTCNQPEVGGLETSLDKILLDEQAQWKHYLVSNISHDTVLQCHFTCSGKQESMNSNVSVYQ
-A---PLQPEQATVKRGEPYKVNCTSSCPHPQIIGLETTLNKVELQKHAQWTEFLVLNVSQDTVMYCHFTCSGKQMSNTISVRVFH
-IFEVSVWPDQALVKLGQSLMVNCSTTCPDPGPSGIETFLKKTQVDRGTQWKEFLLEDVTENSILQCFFSCAGIQKDTSLDITVYQ
-SFWVRISPKFMAEPPGSSVWLNCSSSCPRPEGFSLRTGLQRGQNISGPSWVSFQLLDVRASSNVHCFVTCAGVTRGATARINTYK
-PFWVQVLPREISVIPGSSIWINCSTSCAQPEAWGLITSLTQGKKETGPGWAAFQLLDVRRASAVRCFFTCAGETQEATATIRAYS
-PFWVRISPELVVVPQGGSVWLNCSNSCPLPESSSVRTQLLLGKTLSGPGWVSYQLLNVRASSEVHCFVTCAGKTREATARITTYK
-PFWVRISPNFIAVRPGSSVWLNCSSSCPLLEGSTLHTGLRRGQKLSGPSWISFQLLDVRASSEVHCFLTCAGVTRGATARIHAYK
-PFWVRISPQFKAVQPGGSVWLNCSTSCPLPENSSLSTLLQRGQTLSGPGWVSYQLLDVRASSEVRCFVTCAGETRGAKAKITAYK
-LFWVLISPEFKAVPPGGSVWLNCSSSCPQPEGSSLHTELRRGETLSGPRWVSYQLLDVRASSDVHCFVTCAGETRGATARITAYK
-PFWVRISPKFKAVPPGGSVWLNCSSSCPLPEDSGLSTRLRRGGTLRGPGWVSYQLLDVRASSEVHCFVTCAGQTRGATATITAYK
-PFWVRVSPEFVAVPPGKSVWLNCSNSCPQPQNSSLRTRLRQGKTSRGPGWVSYQLLDVRASSHAHCLVTCAGKTRWATARITAYK
-PFWVRISPAFVAVPPGSSVWLNCSSSCPLPDGSSLRTGLRRGQTLGGPGWVSYQLLDVRASSDVLCLVTCAGETRGATARINAYK
-PFWVRISPESVAVQPGGSVWFNCSSSCPLPQNSSLYTQLRRGKTFSGPAWVSYQLLDVRTSSDVRCFVTCSGETRGATARITTYR
-PFWVRLNPELEAVPPGGSAWLNCSHNCPLPVHSSLRTQLRQGKIVNGSGWVSYQLLDVRANSKVRCVVTCAGETREATARITAYK
-PFLVRLNPELVAVAPGHSVWLNCSTSCPLPANSSLFTQLRHRHTVTGPGWVSYELVNVTASSIVRCFVTCAGETRQATARISAYK
----SDLQPRSALVERGGSLWLNCSTNCPRPERGGLETSLRRNGTQRGLRWLARQLVDIREEAHPICFFRCARRTLQARGTILTYQ
-----------VYAPFGGWAQLNCTHNCTKA----WESRLTKRNRIKGPGWVSV--KEWRAEI--VCAVLSG--AVDNYVTVIPYE
----VWLQPPVLLVPFGGSLNITCSTSCSDPKATG-SVETSSFHDRQSHNATVLEVRNVTENSSLTCFFHCYGAREWERTQLIAYG
----TWIEPPDPVTTLGGTLVVKCITDCDQPLLIGLETQLDKLPENNGTRWRAFRLKNITQDSLLLCFANCNDKQMLHSTNVTVIQ
----VHVSPEAPVVEHGGSLWINCSHTCNSSDPGGLETPLTKSIEERGPGWKAFHLTNITQNPTLQCHFTCPGNVTIASTNLSVYR
-AFEVYIWSEKQIVEATESWKINCSTNCAAPDMGGLETPTNKIMLEEQGKWKQFLVSNVSKDTVFFCHFTCSGKQHSESLNIRVYQ
-SFEVSVWPNQALVEFGQSLVVNCSTTCPDPGPSGIETFLKKTQVGKGPQWKEFLLEDVTENSILQCFFSCSGIQKDTSLGITVYQ
---EVSVWPSQAFVGFGQSLVVNCSTTCPDPGPSGIQTNLKRTEVDKGHQWKEFLLEDVTVNSVLRCFFSCAGIQKDTALNITVYE
-PFWADLQPRVALVERGGSLWLNCSTNCPRPERGGLETSLRRNGTQRGLRWLARQLIDIREPETQPCFFRCARRTLQARGLIRTFQ
-AFKVHVRPKKLMVKPKGSLEVNCSTTCNQPEMGGLETPLNKTVLAQQTQWKHYLISDISQDTVFQCHFTCSGEQVSASFNVSVYR
-SFEVHVSPEAPVVEHGGSVWINCSHTCEDPAPGGLETSLAKASSKHGPSWVAFHLVNIQEASAPVCHFVCRGDSRRATAALSAYR
--AQTSVSPSKVILPRGGSVLVTCSTSCDQPMLLGIETPLPKKELLPGNNRKVYELSNVQEDSQPMCYSNCPDGQSTAKTFLTVYW
-PFWVRVSPEFVAVPPGGSVWLNCSNSCPQPQNSSLRTRLQQGKTLRGPGWVLYQLLDVRANSQAHCFVTCAGKTRRATARITAYS
-SFWVRLSPEFVAVPPGNSVLLNCSSSCPQPESYTLRTQLQHRETLRGPGWVSYQLRDVRASSDVHCVVTCAGERLQATARVTTYS
-TLTVALFT--LICCPGSDE--KVFEVHVRPKK--LATSLDKILLDEQAQWKHYLVSNISHDTVLQCHFTCSGKQESMNSNVSVYQ
--FWADLQPRVALVERGGSLWLNCSTNCPRPERGGLETSLRRNGTQRGLRWLARQLVDIREETQPVCFFRCARRRLQARGLIRHFP
---QLTVLPQEPLVEVGGSIQLNCSLDCPNGKWKGLDTSLGNILS--TP---SYSLLSVTHGGTQTCTGHCQGSSFQKKVDLQVY-
------LQPRVALVERGGSLWLNCSTNCPRPERGGLETSLRRNGTPEPPRLAAPRLLEVGSESPVSCSL-----------------
-------------------QVINFTASCTDPKKLVLETALHKTFLEDQAQWKLFKVISISKNMELMCSFICGGKEGMKVFNITVF-
----LEVDPPESQVAMGESRNFTCRMTCADGAWRGLDTSLGAVQS--SAGLSILYVLNASAAGTRVCVGSCGDVNLQHRVRLLLF-
----LHVEPPEPEVAMGASLQFTCSLACAGGAWRGLDIGLGAVLS--EAGSSVLSVHNASATGTRVCEGSCGDRRFQRTVKLLVY-
----VQVEPKWPVVPAGGSIQLRCSVACPGNEWKGLDTSLGHVFS--EPGLSVLTIPEATMAGTKVCISTCQGQTYQDRVELLVY-
----LHVDPPEPEVAVGTSLQLTCSMSCDKDVWHGLDTNLANVQT--LPGSSILTIQGM-DTGTRVCVGSCGGQSFQHYVKILVY-
----FQVNPPESEVAVGTSLQITCSMSCDEGVWRGLDTSLGSVQT--LPGSSILSVRGM-DTGTPVCVGSCGSRSFQHSVKILVY-
----LVVVPKEPVVQY---VQLNCSLPCPGG-WKGLDTNLGSIDT--FLTHSILKISNVKTEGVKICQGTCGDSSYQNTAILKVY-
----LEVEPPEPVVAVGESRQLTCRLPCAGPAWRGLDTSLGAVQS--GAGSSVLSVRNASSAGTRVCVGSCGSLTYQHTVRLLVF-
----FQVNPPEPEVAVGTSLQINCSMSCDKDIWHGLDTNLGNVQT--LPGSRVLSVRGM-DTGTRVCVGSCGSRSFQHSVKILVY-
----LEVEPPEPEVAVGESRQFACRLACADGVWRGLDTSLGSVKS--GTGSSVLWVHNASAAGTRMCVGYCGKRSFHHTVNLLVF-
----LEVEPPDSVVAVGGSRQLTCRLSCADHAWRGLDTSLGAVRS--DAGSSVLSVHNASAAGTHVCVGSCGNLTLQRTVQLLVF-
----LILKPPRVVVGFGEPVSVSCEAARPVRV-LGWESAIGGVSTDLSVQWKVDSLIDW--IEEPICFFT-APRQCEEKLNLVLYK
----LQINPPKLVVRFNSSASADCSTSVTHKG-MGWESTVGGVPLASLITWRVLQLTYWE-IQPPQCYIN-YKQQCTVDLPVTIYK
----LQITPQSVVVKYGDPVSVNCSTSVTHMG-MGWESTVGGVPLASLITWRVSELTDWE-IQPPFCYIN-YGKQCEVALPVTVYK
----LKIMPTKLVVRFNSSASADCSTSVTHDG-MGWEATVGGVPLANLITWRVSQLTYWE-IQPPYCYIN-YGKQCEVALPVTIYK
----TVFTPSSLVVKFGDPTSANCSVCEPACVNYGVESP-AGDKTGTTISWTVDSLTEW--SVSSLCFYSDAGHQCCTFLPVTVYQ
----LEISPPRAVVKFGDPVTVSCVASRPVRV-LGWESIIAASHTDLSVQWRVDSLTDW--IEEPICFFT-APRQCEEKLNLVLYK
----VQISPPTVVVRFGDPLRIICSS-SDQIESIGLESRYGGEKTVSSVVLDITSVEHW--E-LVICFLNRK--QCSENFPVTVYK
----LPISPQSVVVKFGDSVSVNCSSSVTHMG-MGWESTVGGVHLASLITWRVSELTDWD-IYTPFCYIN-YDKQCVVALPVTVYK
-SFPFTLTPSEMVVRFGDPVSINCSTSATDVEGMGWEAPFGGTGFKPPPTWRVEKLEEW--APSPFCYVTLDGEQYTAKPDITVYK
----LTVSPSTLVVRFGDPVTANCSVGFS---LLGWKVSLPTPEPDRFLVWRVDRITDWG--IQPVCYALATGGKCDITLPLTVYK
----LVLKPSRVVVGFGESVSVSCEA-----RVLGW-SVIGASYTDRSVQWKVDSL-DWIE--EPICYVFFAPRQCEEKLNLVLYK
----PVFTPSALVVKYGDPASVTCDVDCSG-DVFGLEHAVGKVTQGTTMLWTVDRLTEWG--ISVMCYYNAAGDQCCSALDITIYQ
----LVIKPSRVVVGFGQPVSVSCEA-----RVLGW-SSISAVHTELSVQWTVGSL-DWIE--EPICYVFFAPRQCEEKLNLVLYK
-----VFTPSSLVVKYGDPAFATCVPGCDE-SLANLEAS-GYKEKGSTLKWKVSSLTEWK--SDPICYYIDNGTQCCSTLKVTLYQ
------RSPQAVAVPLGESMQLTFRQDCASRGWRGLDTRLGAVQSDT--GRSVLTVSN--AAGTRVCGGSCGGLTFQHTVRLLVF-
----LRITPPEMVVRYGDSVVLNCSVDDPELILVSWETDYGSHEEIN-PSTSTLTIKKVESILKPICFAVSNGFQCTWEPVITVYK
---PVELNPPRVVVRYGDSVSVNCTSS-TDPEGMGWEATYGDTGLHGNVNWTVRSLMDWT--IEPKCYVKSNGSQCTEILSVILYK
---PVELKPPRVVVRYGDSVSVNCTSS-TDPEGMGWEATSGGTELHENVNWTVESLIDWT--IEPKCYINPNGSQCTETLPVILYK
----LKMSPPRVVVRFGGSFSANCTSLSDQTDGMGWESPYGPVDLTQGVTSLLFTIDSVPEELGPMCYVNSHGDQCTEILPVTVYK
----IELSPPSVVVRYGDPVSINCSTSDGHSAEMGWEATVGSTIIQNVPHWTVERLIQTDYDVTSICFYNPPSDQCSKTSTVVVY-
---PLQISPQSVVVRYGDSVSVNCSTSVTHD-GMGWESTKGGVPLASLITWRVSQLTDWE-IEPPFCYINYGQQQCEVALPVTIYK
------FTPSALVVKYGDPANVTCDVDCSGD-VFGLEHAVGKSTPGTTMLWTVDRLTEWD--IFVMCYYNAAGDQCSSTLDLTLYQ
------LTASEIVVKFGDPISINCSTDVEG---MGWEAPFGGT---PVVTWRVDQVEEWT--PSPSCYATLDGSQCTVSPLITVYK
------FTPSSLVVKFGDPTSANCSVACVNN-QYGVESPAGVTRKGTTISWTVDSLTEWS--VSSLCFYSDAGHQCCTFLPVTVYQ
---PLQEEPPGAGWPCGESMQLTFRQDCASRGWRGLDTRLGAVQS--DTGRSVLTVSNASAAGTRVCGGSCGGLTFQHTVRLLVF-
---PLQVEPPEPVVAVGASRQLTCRLACAERGWRGLDTSLGAVQS--DTGRSVLTVRNASAAGTRVCVGSCGGRTFQHTVQLLVY-
---PLQVEPPEPVVPVGASRQFTCRLTCAGRGWRGLDTSLGAVQS--GAGHSVLTVHNASAAGTRVCVGSCGDLTFQRTVKLMVY-
---PLQINPPKLVVRFNSSASANCNTSVTHD-GMGWEATVGGVPLANLITWRVSQLTDWK-IEPPFCYIN-YGKQCEVPLPVTIYK
------FTPSALVVKYGDPANFTCVVDCSED-VFGLEHAVGEVTKGTTMFWTVDRLTEWD--ISVECYYITIGDQCSSTLNIIIYQ
---SLEVEPPDPVVAVGGSQQLTCRLACAGHQWRGLDTSLGAVRS--DAGSSVLSVRNASAAGTRVCVGSCGTHTFQRTVQLLVF-
---PIELKPRRAVVKYGSSVSADCKSFVPHD-GIGWESTVGGVPLASLITWRVSQLTDWE-IQPPFCYIN-YKQQCEVALPVTIYK
------FTPSALVVKYGGPANATCVVDCRGT-VFGLEHAVGKVTGGTTMLWTVDKLTVW--DTSLICYYNTAGDQCSSILDITTYQ
---PLKISPSTVVVRFGDPVKANCSIQRMGYFGIGWEVPLNPPNFTSFVVWSVEKMTDWT--IKPVCFALSQGGPCYSNLTLIVYK
QAFKLDISPQDKTMAIGDILSLTCSTGCETPS-FSWRTQLHGRVRTEGS--KSVLIMDVSNEHSYLCTATCGSKKLERGIQVDVY-
QAFKIETTPESRYLAIGDSVSLTCSTGCESPF-FSWRTQLNGKVTNEGT--TSTLTMNVSNEHSYLCTATCESRKLEKGIQVEIY-
QAFKIETTPESRYLAQGDSVSLTCSTGCESPF-FSWRTQLNGKVTNEGT-TSTLTMNPVSFEHSYLCTATCESRKLEKGIQVEIY-
KAFEMEVIPADRVVARGETLILTCNTGCALPK-FSWRTQLGGKVYNNKT-YSTLTMNPVSAYNDYLCTVICNREKKEKSVKVELY-
QNVKVEIFPEDKMIAQGDSASLTCSADCESSLSFSWRTQLNGKVKTNGT-RSTLVMNPVSFEHSYLCTVSCGKLKGERGIQVEIY-
----MEIIPAERIVAQGDALILTCNTGCASPS-FSWRTQLGGKVSNHRT-YSTLTINPVSISHSYLCTVICDREKKEKSVKVELY-
KPFTVEISPGQIIAQIGDSVVLTCGTDCESPS-FSWRTQIDGTVKVEGAK-STLTLSPVNEEHSYLCTVTCGHKKLEKGIKVDLYS
KPFTVDIFPGQVVAQIGDSVVLTCRTGCESPS-LSWRTQIDGKVTSEGTE-STLTLSPVGEEHSYLCTVTCGRRKLEKRIRVELYS
TPLTMEISPGKIDAQIGDSLVLTCTMGCDAPL-FSWKTLIDGQVYSEGNK-STLIMNSIGEEHFYVCTASCGDKKVEKVVQVKLYS
KPLRVEVSPGKTAAQIGDSVVLTCTMGCEAPF-FSWRTQIDGKVSNEGTK-STLIMNPIGEEHSYLCTASCGDRKVEKGIQVELYS
KPFTVDISPGQVAAQVGDSIVLTCSTDCESPS-FSWRTQRNGMVRIVRTE-STVNLSSVSEEDQYLCTVTCGHRKVEKEIQVELYS
KMFTVEISPGQIAAQVGDSVVLTCDRDCESPS-FSWRTQIDGKVRREGSK-STLTLSPVSEEHFYLCTVICGQKKLEKGIQVELYS
KPFIVDISPGQVAAQVGDSVVLTCAIGCDSPS-FSWRTQTDGVVRNEGAK-STLVLSSVGEEHSYLCAVTCLQRTLEKRTQVEVYS
KPFTVEISPGKIAAQVGDSVVLTCGTGCESPS-FSWRTQIDGKVSSEGTT-STLTLNPVSEEHSYLCTVTCGHKKLEKGIKVDLYS
KPFTVEISPGRIAAQIGDPVVLTCSRGCETPS-FSWRTQIDGKVTSEGTK-SLLTLSPVSEEHSYLCTVTCGHKKLEKGIQVELYS
KPFIVEISPGQVIAQIGDSVVLTCGTGCESPS-FSWRTQIDGQVKTEGTK-STLTLSPVSEEHSYLCTVTCGRKKLEKRISVDLYS
KPFTVEISPGRIAARIGDSVVLTCGMGCESPS-FSWRTQIDGKVRSEGTN-STLTLSPVSEEHSYLCTVTCGRKKVEKGIQVELYS
KLFTVEISPGQIATQIGDSVTLTCRMGCESPS-ISWRTQIDGKVRTEGTK-STLTLSPVSEEHSYLCIVTCGHKKVEKEIKVDLYS
KPFTVDISPGQVAAQVGDSVVLTCAVGCDSPS-FSWRTQTDGEVRDEGAT-STLTLSPVGEEHSYLCTVTCQRRKLEKTIQVEVYS
KPFTVDISPGQVATQIGDSVVLRCSTGCETPS-FSWRTQRDGIVSSDRTE-STVTLTSVSEEDHYLCTVTCGRRKMEKEVQMKVYS
KPFTVQISPGRIVAQIGDSVVLTCGTGCESPS-FHWRTQIDGKLKSEGTK-STLTLSPVSEEHSYLCTVTCGRRRLEKEIQVELYS
QLFTVELSPDPVAAQIGDSVVLTCSTGCESPS-FSWRTQIDGKVRSEGTK-STLTLSPVSEEHFYLCAVTCGHEMLEKEIQVKLYS
KPFTVEIGPGQVAAQLGGAVVLTCGTGCEAPS-FSWRTQIDGQVRSEGPR-STLTLSPVGEEHSYLCTVTCGRKKLEKGIKVDLYS
SAVNVELLPSQVLAELGKPLVLTCKTECESPT-FSWKAQLDGKENVQGFE-SNLTFSSVNQENSYTCMVTCDGRQKHKTVSVMVYS
QG--------DVAGLMKHNISVSCG---EAPA-SSWNSLQDASIQIRKPK-----------THYYFAN------------------
APKNTVVSVTSTVVQEGNYVMMTCSEGLPAPE-ISWSKKQKGNLEILSHN-ATLILNAAMASGLYVCE---GTNRVGK--------
----LLMSPSRLVVRFGDPVAVNCSVLQAGFQVLGWEVSLVRDSV-----------------------------------------
QASKMEIFLERVAAQIGDVISLTCSTGCETPS-FSWRTPLNGKVKNEGNN-STLTMDPVSNEHAYLCTATCGSKKLEKGIQVEIY-
QAFKIETSPERILAQIGDSVSLTCSTGCESPV-FSWRTPLNGKVKTDGTN-STLIMDPVSNEHSYLCTATCGSEKLEKGIQVEIY-
EELTIEVGPSNIAAEIGGSTTLTCNVGCEAPL-FSWRAPLGGKVQNQGTK-SVLTIDPVGNENYYLCIASCGNNKVERGIQVKVY-
EDLNIEVGPSRIAAQIGGYTELTCTMGCESPF-FSWRTPLGGKVQNEGTK-STLIMNPVSNEHAYLCTASCGSKKVERGIQVELY-
QAFKIETSPERILAQIGDSISLTCSTGCESPS-FSWRTPLNAKVRNEGTK-STLTMDPVSNEHSYLCTATCGSRKEEKEIQVDVY-
QAIKVDMFPEKIFAQIGDSVSLTCSAGCESPS-LSWRTPLNGKVRNEGTT-STLIMDPVSDEHQYLCTVICGAAKLEKAIQVEIY-
QAFKIEIFPEKIVAQIGASVSLTCSTGCESPS-FSWRTPLNAKVRNEGTT-STLTMDPVSNEHSYLCTATCGSGKEEKGIEVRIY-
QAFKIETFPERSLAQIGDSVSLTCTTGCASPT-FSWRTPLNGKVRSEGTT-STLTMDPVSNEHSYLCTATCESKKLEKGIQVEIY-
QAFKMEIFPERVVAQIGDVISLTCSTGCESPS-FSWRTPLNGKVRNEGTK-STLTMDPVSNEHAYVCTATCGSMKLEKGIQVEIY-
LAFKIKTTPERSLAQIGDSVSLTCSTGCDSPS-FSWRTPLNGKVRNEGTT-STLTMNPVSNEHSYLCTAICGSSKLEKGIQVEIY-
QAFKIEIFPEKILAQIGDSISLTYSTGCDSPS-FSWRTPLNGKVINEGTK-STLTLEPVGHEHSYLCTATCGHDKQERGVQVDLY-
QAFKIEISPEKTLAQIGDSMLLTCSTGCESPS-FSWRTPLNGKVKTEGAK-SVLTMDPVSNEHSYLCTATCNSGKLERGIQVDIY-
QAFKIETTPERTLAQIGDSVSLTCSTGCEAPS-FSWRTPLNGKVRSEGTK-STLTMDPVSNEHSYLCTATCESGKLEREIQVEIY-
QAFKVEISPEKIFAQIGDSVSLTCSTGCDFPI-FSWRSPLNGKVKSEGNK-STLTMDPVSNECSYLCTAICGDEKLEKENQVEIY-
QAFKIEISPDPVMAPIGASVSLTCRAGCEAPA-FSWRTPLNGKVRSAGST-STLTMDPVSNEHSYLCTATCGARKAERGIEVQVY-
FAIKIELVPQKRYVPVGEPLMLMCKVGCESPS-FTWRTPLGGKVNTRGPE-SNLTFSSVGNEESYICTGYCGNSKLQKHVPVRVY-
NAFEIHLTPEKVAVQLGKKLELICHSDCESPE-FSWRTPLGGTTNNTRSS-SVLTMDPVDNEHEYVCSATCDNVKKEKNIKVDVY-
ETFKVEITPGKIAAQIGNVTVLTCIAGCAAPT-FSWRTPLGGDLEQGVNK-SILTMNPVDNEHSYLCTASCDTKKMERAIQVELY-
---KLKLTPLNPAVHVGEDLRLTCSNSCSVNVTFTWKTLLDRFHVEDEPTVSRLLIRSISHNIKLVCKANCGSERSEKTSAINVYS
QAFKIEISPEYKTAQIGDSMALTCSTGCESPL-FSWRTQINAKVRTEGSK-SVLTMEPVSNEHSYLCTATCGSGKLERSIHVDIYS
TFEILDIASGHNIAQIGARLVLNCCTGCESPE-FSWRTQLGGIVSRQGSN-SSLTLASVGHEQNYVCNAICGSERKERRVQIDIYS
QAFKVDIFPNEIFTQIGDSVSLTCSTGCESPS------------------------------------------------------
