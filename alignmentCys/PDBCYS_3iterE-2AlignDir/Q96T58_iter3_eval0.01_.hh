PVDMVQLLKKYPIVWQGLLALKNDTAAVQLHFVSGNNVLAHRSLPLS-PPLRIAQRMRLEATQLEGVARRMTVETDYCLLLALPCGRDQEDVVSQTESLKAAFITYLQAKQAAGIINVPNPGSNQPAYVLQIFPPCEFSESHLSRLAPDLLASISNISPHLMIVIASV
ECKMPLDIKQGFFFFHGV--------DFQGHFRGGELFVDIGAKPLGRPSVCLA--------SVPGGVKNI------CLLLALPCGRDQEDVVSQTE-LKAAFITYLQAKQAAGIINVPNPGSNQPAYVLQIFPPCEFSESHLSRLAPDLLASISNISPHLMIVIASV
SVDMVQLLTKYPIIWQGHLALKNDTAAVQLHFVSGNNVLAHRSLPPPGAFLRIAQRMRLEASQLEGVARRMTAENEYCLLLALPCGLDQEDVHNQTHALKTGFITYLQAKQAAGIINVPNPGSNQPAYVVQIFPPCEFSESHLSHLAPDPLNSISSISPHLMIVIASV
PVNMVQLLTKYPIVWQGLLALKNDTAAVQLHFLCGNKALGLRSLPLPGGILRIVQRMRLEAQQLEGVARRMTGESDFCLLLAMPCGLDQEDVLNQTQALKSAFINYLQAKLAAGIINVPNPGSNQPAFVLQIFPPCEFSESHLSRLAPDLLSQISSISPHLMIVITSV
SIDTVQLLTKYPIVWQGLLALKNDQAAVQLHFVSGNTVLAQRSLPPPGPLLRIVQRMRLEASQLDSVARRMTVESDYCLLLALPCGRDQEDVLGQTQALKSAFITYLQAKQAAGIINVPNPGSNQPAYVVQIFPPCEFSESHLSRLAPDLLSSISSISPHLMIVIAAV
------ILDTLVLLWQGLLALKNDTAAVQLHFVCGNKALAHRSLPLQGTLLRIVQRMRLEASQLESVARRMTGDSDFCLLLALPCGRDQDDVLNQTQALKAAFINYLQTKLAAGIINIPNPGSNQPAYVLHIFPPCEFSESHLSQLAPDLLSRISSISPHLMIVITSV
----LELLCRYPVVWQGLVALKNDTAAVQMHYLSGNSRLAEASLPQAPPPLRIAQRMKLEQSQLEGVVRRMQCEADHCLLLALPCGRDPLDVHAQTRALKNGFINYLQQKQAAGIINVARPGSVQPSYVLHIFPPCDFAQEHLARVSPDLLDSAAD-SGHLMVVVASV
-------LQRYPLVWQGVLMLKNDTVVVQMYLLCGSKAIATESL---ISPLKIAQRMKLEASQLENVSKRMQNDKEFCLLLALPCGRDFTEMQKQTHALQASFVSYLQQKEAAGIINITLPGSEKIGYVLHIFPPCDFSNCHLMKNAPDL------------------
---VDDIFTRYPVIWQGKMALKTESAAVQIHYICGNMFHARAGLPSDLPVLRIVQRMRLNPLPLEGVSKRMEMKDDYCLLFAVPCGRTTNELEHQTNQLKQSFIDYLKQKQVAGIVNTAS--NNQQVYVLHVFPPCNFTTQNLQQRAPDLVANLGDIQ-HLMIVITTA
---LVSLLQRYPIMWQGLMALKNDHAVVQMHFISGNPMVARGSLPCSLP-LRIAQRMRLEQQQLEGVARKVQMEQEHCILLALPCGRDAMDVLQQSNNLRQGFITYLQLKQAAGIVNIAAPGSQQPSYVVHIFPACEFANDSLARITPDLLHRVTDIA-HLLVIIATV
----MNLVQRYPVFWEGMIALKNDASSVQMHFVAGNAQLARHSLPAVQRPLRIAQRMRMEPTQLDGVDRRMMSDEDYCLLLALPCGRDESDVINQTSTLRSGFITYLQQKQAAGIINVPAPGSSQPAYVLHIFPPSEFSKKHLGRLAPDLLESVNDIA-HLMIVIAT-
------LLQRHPVMWQGLLALKNDQAAVQMHFVFGNPRVARDSLPCNTPPLRIAQRMRLEQTQVEGVARKMQTEDEHCMLLALPCGRDEVDVLQQSKNLQAGFIMYLQQKQAAGIVNIAAPGSQQAAYVVHIFPSCDFAHETLARIAPDLHHRVAKIA-HLLIVIATV
----LMLLQRYPIMWQGLLALKNDQSAVQMHFVFGNPHVARDSLPCNTPPLRIAQRMRLEQTQLEGVARKMQMDNEHCMLLALPCGRDHMDVLQQSTNLQTGFITYLQQKQAAGIVNIAHPGSQQAAYVVHIFPSCEFANENLARNAPDLLHRMQEI-PHLVIVIATV
-----QPLLRYPVMWQGLLALKNDQAAVQMHFVSGNSRIAVASLPPMTPPVRIAQRMRLEQTQLEGVARKMQMVEEHCILLALPCGRDHMDVLQQSNNLRNGFINYLQLKQAAGIVNAAAPGSQQPAYVIHIFPSCEFSNENLARIAPDLLHSVSDIA-HLLVIIATV
---LVTLLQRYPVMWQGLLALKTDQAAVQMHFVHGNPNVARASLPSMTPQLRIAQRMRLEQTQLEGVAKKMQVDKEHCMLLALPCGRDHADVLQHSRNLQTGFITYLQQKMAAGIVNIPMPGSDQAAYVVHIFPACDFANENLERAAPDLKNRVAELA-HLLIVIATV
-----MLLQRYPVMWQGLLALKNDQAAVQMHFVYGNPNVARESLPFNTLPLRIAQRMRLEPTQVEGVARKMQTDNEHCMLLALPCGHDHMDVVQQSKNLQAGFITYLQQKQAAGIVNIAAPGSQQPAFVVHIFPSCDFANENLARIAPDLLHRAADIA-HLVIVIATV
----MGLLMRYPVMWQGVLALKNEAAAIQMHYVSGSQSVAQDSLIMATPPLRVAQRMRLEPSQLDGVGSRMQVEEESVVLLALPCGHDQTDVIAQTNTLRVSFIEYLLSKQAAGIINVAPHGSQKAAFVLHIFPPCEFTEAHLSRLSPDLLESVQEIA-HLMIVIAT-
---LDQLLQRYPVMWQGLLALKNDQAAVQMHFVHGNPGVAGSSLPSNTPPLRIAQRMRLEPAQIDGVARKMQMDQEHCMLLALPCGRDRMDVLQQQNNLQTGFITYLQQKQAAGIVNIAAPGSSQAAYVVHIFPSCEFANENLARIAPDLMHRVANIQ-YLLIVIATV
----FMLLKQYPCMWQGLLALKNDQAAVQMYFVSGNDSVAKCSLPKNTPPLRIFQRMRLEPPQVEGVARKMQMENEHCMLLALPCGHDHMDVLKQSTNLTNGFITYLQQKQAAGIVNVAAPGTTQPAYVVHIFPSCDFVNENLRRIAPSLLERVADIA-HLLIVITTV
------LLRRYPVMWQGLLALKNDSAAVQMHFVGGNPGVAGDTLSRHTPLLRIAQRMRLEPAQLDQVHRKMKMENEHCMLLALPCGRDHMDVLQQSNNLTAGFITYLQRKQAAGIVNIAPPGHHQAMFTVHIFPSCDFANENLNRIAPDLMHRVADIA-HLLIVIATV
---LLSLLQRYPVMWQGVLALKNDQCVVQLHMINGFEQLVKLSLPQQGPPLRIAQRMRLEPAQLDGVIKRMKFDNDYCMLLALPCGRDHDHIIEQTRAMTTGFIQYLQQKQAAGIVNVAEPETQQPAYVVHIFPPCDFSRSTLERLGFGLMQPLRDLA-HLLVIITTV
-----SILKEYPIYWQGTLALKNDKALVEMHYLFGNAAVAEHSLQRNDGELKLSQRMRLEQTQLDGVTRKMQIEDECCVLIAVPIGMSEEEWNQQSCTLHNGFITYLQQKQAAGIINITAPGSTQNAYIVHMFPPCDYNNSVLASVHPEMVKEIASMA-HLIIVIATV
--------SSYPLVWQGRLSLKNMETRVALHFVQGNHNLLNFSLITSGGPLRIVQRMRLEPAQLEGVQRKLNQEGASCACLALPAGSSPVELAQQTQILNENFIRYMQEKMAAGIINVGHPDYQQGLYVVHIFPPCDFSHAQLGLAAPDLHRRVVQNQSHLLVVITTV
-----SLSHAYPLVWQGRLSLKNAETRVALHYIYGNPNLLHDSLVASGGPLRIVQRMRLEPAQLEGVQRKIHQEGASCVCLAIPAGNGAVELMQQTQILNDSFIRYMQEKMAAGIINVGFPDFQQGLYVVHIFPPCEFSHTQLDLAAPDLNRRVQANQSHLLVVITTV
------EIQEYPIIWRGRLSLKNEEVFVNMHYVSGNQDLLTSSIAPTHQPLKIVQRMRLEASQLEGVQRKLRQLNDFCMCLTLAAAPPQTSDASLNRILCDGFIKYMLEKCAAGIINVCHPCTQQNMYVIHIFPPCEFSRCQLQGYAPALYHSLENPIPHVLTVITTV
--FLEQLLNKYPVVWQGNDNLPS---MGLLHDMD-CKI--IGT----IPTLHISQRMKLEPLHLKGIEKCMEKQTDYCLLLGLPCGLDSYDIAKQTSSLQKSIISYLWLKQAAGIINLSTEED--SKIALHIFPPCQFSRSHLSKVAPDMLAVA-LDDAHLFIVLAV-
----VASSSRYPVLWQGIVALKTNEARIQMHRVGGNIEMLKRSLVQLMPVIRINQRMRMEASQLESVQSKMTDEQSYIALICLSCGFNKDDIRNQSEMLKERFVDYLESKQAAGICNVGNEQHPSPNSIVHVFPPCEFATTFLQRNSPDLFETIRQQANYLFVVITS-
----ASVIARYPVMWQGIIALKTNETRVQMHKVGGNAEMCKRSLDQFMPLIRINQRMRMEAGQLESVQCKMMDEHSYIALICLSCGPSKEDIKSQSELLKERFVDYLESKQAAGICNVGNEQNPTPNTIVHIFPPCDFASAFLQKNSPDLLEIFRQQASYLFVVITS-
----ERLLDRHKLLWQGYLSIKNDTATVHLNLIHGNADVPMEFLPRAMATLKIVQRMRLEQNQLDKVAEQIEKPSTCCTMVALPCGTDERDVHTQTDALQKKFIEYLKEKRSAGIVNVN---SSRNPHILHIFPPCEFTTNTLRRLAPEFLKMVHHL-PMLLVVIA--
------LAKRFAVAWKGSLILKNSAFPVRMHLVGGNPEIADSLIRNDKSSLRITQRLRLDQPKLEEVSRRINGASGHCLLLALPG--TQIQLPESTEELQRNLMTYLKQKQAAGVVNLPVSPSNSTVGVLHAFPPCQFSHEHLLRIAPHLSAEPS-KEDHLVIVV---
-------TQDYPVGWRGRLSLKNEEVFVHMHYLSGNQGLLQNSSPSELPLLRIVQRMRLDPSQLDGVQRKLRQVNDFCMCLTLAAPMTPEETIRMNQVLCDNFIKYMVDKCAAGIVNVCNPFTNQNLYVIHIFPPCDFSRCQLEGTSPALHRQLTQTTPYLLVVITTV
-------AQTLSKTWHGFFALKNSSFPTDLYLLEGGAAFFSAVMKDSPSQLKIAQRLRMDQTRLDEVSRRIKLPDNFAILLALQ-GPVDRQAPGLQVRLLRHLVTYLRNKEAAGVVSLPAA--KAPGAMLYAFPPGEFSQQYLQAA-KRTVGNLD--EEHMVIVIV--
-------AQTLQLGWNGLLVLKSSCFPTSMYILEGDQGVINGLLRDQLTQLKITQRLRLDQPKFDEVTRRLRQSNGYAVLLATQAGAGAEGMPGLQRRLLRNLASYLKGKQAAGVISLPVGGAKDYQGMLYAFPPCHFSQQYLQSVPPRTLGEPE--EEHMLIVIV--
---------KLCLG-RGMLLLKNSNFPSNMHLLQGDLGVASSLLVEGVAQLKITQRLRLDQPKLDEVNRRIKVPNGYAILLAVP---------TSTQRPLRNLVSYLKQKQAAGVISLPVGGNKENSGVLHAFPPCDFSQQFLDST-AKALAKSE--DDYLVMIIV--
-------AQTLQLGWNGLLVLKNSCFPTCMHILEGDLGVISGLLKDHLTQLKIAQRLRLDQPKLDEVTRRIKQPNGYVVLLATQAGPGAEGTHGLQRRLLRNLVSYLKQKQAAGVISLPVGGTKDNTGMLYAFPPCEFSQQYLQSA-LRTLGKLE--EEHMVIVIV--
-------VQTLQLGWNGFLVLKNSCFPTSMHILEGDQGVISGLLRDQLAQLKIAQRLPLDHPKFKDVTQRIKQARGFAVLLATQSGPGTEGMPDLKKRLLRNLISYLKGKQAAGVIRLPVGGSKGLKGMLYAFPPCPFSQQYLQSA-LRTQDQLE--EEHMLIVIV--
---------KLCLA-QGMLLLKNSNFPSNIPTCSC-CRVTSKWLAVFVAQLRITQCLCLDQPKLDEVTRCIKVPNGYAILLAVP---------ISTKRLLRSLVSYLKQKQIPRVISLG-EHNKKNTGILLAFPPCELSQQFLDS---KALAKSE--D-YLVMIIV--
-------GRTLPPVWCGHLVLKNSCFPTYLHFLEGDRDVPGALLKDRLAQLKIAQRLRLDQPKLEEVTRKVRQAGGYAVLLATQA-PQNEGAIGLQRRLLRNLVSYLKQKQAAGVISLPVGGSKDPSGMLYAFPPCDFHQLYQQSA-QRLVGKLE--E-NMIIVLV--
-------SPKLCLAWQGMLLLKNSNFPSNMHLLQGDLQVASSLLVEGVAQLKITQRLRLDQPKLDEVTRRIKVPNGYAILLAVPGSSAASDTATSTQRPLRNLVSYLKQKQAAGVISLPVGGNKDNTGVLHAFPPCEFSQQFLDSPAKALAKS---------------
-------GTKLIVAWRGMLLLKNSNFPASMHLLEGDYGVASDLLIDGVSELKITQRLRLDQPKLDEVSRRIKVPGGYAILLAVPGSTSSSDPMASTQRPLRNLVSYLNQKQAAGVISLPVGGSRDNTGVLHAFPPCDFSQQFLDSSAKALAKS---------------
-------GQKLSQVWLGDLLLKNSSFSTSLHLLDGDVAVATALLLKGVSQLKISQRLRLDQPKLEEVSRRMTAPGGSSILLAVPGRPDAQDANNS-ERPLKNLVSYLKQKEAAGIISLPVGGARDQGGVLHVFPPCEFSREFLDASAKAFAKS---------------
-------GGKLSMAWNGMLLLKNSNFPASMHLLEGDLSVATSLLIDGVSQLRITQRLRLDQPKIDEVSRRIKVPGGYAVLLAVPGSSSSSDPAASTQRPLRNLVSYLKQKQAAGVISLPVGGSRDNTGVLHAFPPCDFSQQFLDSSAKALAKT---------------
-------TQKLSLAWQGMLLLKNSNFPSNMHLLEGDLSVARNLLIDGVAQLKITQRLRLDQPKLDEVTRRIKVPNGYAVLLAVPGNDSVMEQATSTQRPLKNLVSYLKQKQAAGVISLPVGGSKDNTGVLHAFPPCDFSQKFLDSAAKALAKS---------------
-------AQTLPLAWKGVLVLKNSCFPTTMHILEGDLGILNILLKDYISQLKIAQRLRLDQPKLDEVTRRIKQPSGYAVLLATQAPQ------LQ-QRLLRNLVSYLKQKQAAGVISLPIGGPKENNGMLYAFPPCDFSQQYLQSALRTLGKL---------------
-------AHKLCLAWQGMLLLKNSNFPSNMHLLQGDLGVASSLLVEGVAQLKITQRLRLDQPKLDEVTRRIRVPNGYAILLAIPGTSSSVDQASSTQRPLRNLVSYLKQKQAAGVISLPVGSNKDHAGVLHAFPPCNFSQQFLDAEAKALVKS---------------
-------GQKLSQAWEGVLLLKNSTFPTSLHLLGGDMNLASSLLVEGVSQLKISQRLRMDQPKLDEVSRRIKASSGYSVLLAVPGRSEGQDSSNSTERPLKNLVSYLKHKEAAGIISLPVGGGRDHAGVLHAFPPCEFSQQFLDASAKAFAKS---------------
-------EQKLIQVWQGVLLLKNSSFPTSLHMLEGDLAVATSLLLKGASQLKISQRLRLDQPKVDEVSRRIKASGGYAVLLAVPGKCGIQEANSSAERPLKNLVSYLKQKEAAGIIGLPVGGVRDHGGVLHAFPPCDFSQQFVDASAKAFAKS---------------
------VACRCMAVWQGALILKNSLFPAKFHLTDGDGDIIESLMKDGKHLLRITQRLRLDQPKLDDVSKRISSSSSHAIFLGLSGSSTIDDASVQTRPLR-NLVSYLKQKEAAGVISLLNKES-DSSGVLYAFPPCSFSTDLLKRSAPGLSDEG-LKEDHLVIVVV--
------VARKCSTVWTGALILKSSLFPAKFHLTDGDTDIVESLMRDGKHNLRITQRLRLDPPKLDDVQKRIASSSSHAIFMGLAGSTNCDDASVQTRPLR-NLVSYLKQKEAAGVISLLNKET-EATGVLYAFPPCDFSTELLKRTCHSLTEEG-LKEDHLVIVVV--
------VARRSPSNWTGALVLKNSFFPTKLHATSGEAAIADLLLHDNQPHLRITQRLRLDPPKLEDVSRRVSASASHAIFLGLPGSTSDEAAGVQNRPLR-NLVTYLKQKEAAGVISLANKNA-DVTGVLYAFPPCPFAADLLRRAAPSLSDET-LKDDHLVIVVV--
------LACKCSYTWQGSLVLKSSQFPTKCHLTSGDTNVIKSMMKDGKSILRITQRLRLDETKLEDVSKRIMTADKKAIFLAMPFSTSNNDALIQIRPLH-NLVSYLKQKEAAGVISLMSEDP-ETAGVLYVFPPCEYSAKLLKQNAKNLSSEA-LNDDHLVMVVV--
------VARKCTTVWQGALILKNSLFPAKFHLIDGDTDIVEGLMKDGKHQLRITQRLRLDQPKLEDVQRRISGASSHAIFLGLAGSTTNDDSTVQTRPLR-NLVSYLKQKEAAGVISLLNKET-EATGVLYSFPPCAFSTELLKRTCPNLSEEA-IKEDHLVIVVV--
-----ELGKSYPIAWRGNLVLKNTGFPARLHLVGGDPSVAEYLLRGKPCVLRITQRLRLEQPRLDEVNKRLTGPNGHCILFALPGPPDEGDDSMQLRPLRS-LVSYLKQKEAAGIVALSSGGSNKVIGVLHAFPPCEFSQAQLMKVIPSL------------------
---------TLQLGWNGLLVLKNSCFPTSMHILEGDQGVISSLLKDHLTQLKIAQRLRLDQPKLDEVTRRIKQPNGYAVLLATQAGLGTEGMPTVQRRLLRNLVSYLKQKQAAGVISLPVGGSKGGTGMLYAFPPCEFSQQYLQSALRTLGKL---EEEHMVIVI---
----------LQLAWHGLLVLKNSCFPTSMHILEGDLGVINGLSGGKLTQLKIAQRLRLDQPKLDEVTRRIKQPNGYAVLLATQSTASAEGFPAVQRRLLRNLVSYLKQKQAAGVISLPVGGARDSTGMLYAFPPCEFSQQYLQSA----LRTLGKLEEHMVIVIV--
----------LSQAWQGVLLLKNSSFPTSLHLLEGDMKVAASLTGGQASQLKISQRLRLDQPKLDEVSRRIKASSGYAILLALPWKSGAQD----TERPLKNLVSYLKQKEAAGIISLPAGSGKDHGGVLHAFPP---------------------------------
----------LSQVWLGDLLLKNSSFSTSVHLLEGDMAMISSLTGQQVSQLKISQRLRLDQPKLDEVSRRIRAPNGCSVLLAVPGKGDAQD----SERPLRNLVSYLKQKEAAGIVSLPRGGRQRARRSAARFPPLRVLAGVPGCL----CQSFCQIR----------
----------LVKSWHGFFALKNSSFPTTLYLLEGGPAFFGAVKPPGQNRLKIAQRLRMDQTRLDEVSRRVKLPDGYAVLLALQGP-----VDRLQARLLRHLVTYLRNKEAAGVVSLP--GAGGPGAMLYAFPPGAFSQQYLQAA----KRIVGNLDEHMVIVIV--
----------LSRMWHGFFALKNSSFPTELFLLEGGASFFGAAKAPSQ--LKIAQ-LRMDQSRLDEVSRRIKLPDNFAILLAVQGA-----VERQEVRLLRHLVTYLRNKEAAGVVILP--AAGGPGAMLYAFPPGEFSHQYLQAA----HGALGRLEEHMVVVIV--
-----DVARKSIAVWQGALILKNSLFPAKFHLTDGDTEIIDSLMKDGKHMLRITQRLRLDQPKLDDVSKRIQTSSSHAIFLGLAGSSTNDDANVQTRPLRN-LVSYLKQKEAAGVISLLNKD-TEGTGVLYAFPPCAFSTDLLKRTCPNLSE-EGLKEDHLVIVVVK-
-----DLAHKCTDIWQGSLVLKSSQFPAKCHLISGDTNVIESMMRDGKSALRITQRLRLDEPKLEDVSKRIITADMHAIFLAMPCSTSNDDASVQIRPLRN-LVSYLKQKEAAGVISLIIKD-PEPAGVLYAFPPCEYSAKLLKQSAKNLSS-ETLKDDHLVVVVVS-
-----EVARKSSTVWQGALILKSSLFPAKFHLTDGDNDIVESLMKDGKHHLRITQRLRLDQPKLEDVQKRISTSSSHAIFLGLPGSTASDDASVQTRPLRN-LVSYLKQKEAAGVISLLNKE-TEATGVLYSFPPCDFSTELLKRTCHNLTE-EGLKEDHLVIVVVK-
---LSELVKSYPFAWRGNLVLKKTGFPARLHLVGGESSVAEYLLKSRPCVLKITQRLQLVPSGFDEVNERLTGSNGHCILLALPGPTDEGDNSMQLRPLRS-LVSYLSQKQAAGIVTLSSGGSNKVIGVLHAFPPCEFSQARLMKAIPNL------------------
---------KLCLAWQGMLLLKNSNFPSNMHLLQGDLSVASSLLVEGGAQLKITQRLRLDQPKLDEVTRRIKVANGYAIL---SSGAT-EAATTSTQRALRNLVSYLKQKQAAGVISLPVGGNKDNSGVLHAFPPCDFSQQFLDSTAKALAK----------------
---------TLQLGWSGLLVLKNSCFPTTMHILEGELDVVSGLLRDPGAQLKIAQRLRLDQPKLDEVTRRIRRGNGCVVLLAG-----------LQRRLLRNLVSYLKQKQAAGVISLPVGGAKGGTGMLYAFPPCEFSHQYLQSALRTLGK----------------
--------ILYPSIWLGFVGMKSKAAMIQLHHIDGNQHIASRLLTKSHPIIKIKSRMKLDGKAIDTIKRKWTDIPYKCVMLALPYGVDPDDTEKQKQCLENHFSKYFAGKNCCGNLQINH-GD--QDWHIYVFPPCEFMNSSLVEKASDLANAVEKL-PKLMITII--
-----AFTKNFPMVWTGRLALKSTETMVNLHLINGSEVLG-RHLNPRRDSVKILQRLRLDNGQVEHIYGILTNP-EYACCLALSSVTEYENLARNESNLKASFIDYLTKKQIAGISSLEEVDAKFKSARVHVFAPGEIVNKYLSELAASLHDYLQNTDTRLLIVF---
-----SFSQHFPMVWTGRLALKSTEAMINLHLINGSEVLG-RQVNPRRDSVKILQRLRLDNGQVEHIYRILTNP-EYACCLALSSVNNIENLKENDTNLKSHFIDYLINKKIAGISSLGEVETKFKSARVHVFAPGEIVNRYLSELATSLHDYLQNTDTRLLIVF---
-----DFTKNFPMVWTGRLSLKSTDCMINLHLINGSEVLG-RQINPRRDSMKIVQRLRLDNGQVEHIHSILTNP-DYACCIALSSVTDLTDIQTKDAHLKASVIDYLTKKQIAGVSSLEEVEMKFKSARVHVFAKGDIVDKYLSELAAPLHNYLSNTDTRLLVVF---
-----------ASAWQGGLVLKNSAFPARMVLCSGDSVE-----PLLQASLRITQRLRLDPAKLADVERRLAQG---CAIVAL--AQEQA--GGQPQRPLRNLVSYLKQKEAAGVIPLAR-------AVLYAFPPCAFALQVLRRACPAIQS---ADEDHLVVVVA--
-----------GTVWQGLLVLKSNAFPARMVLCTSEPAI-----TMLDQTLRITQRLRLDAGKLAEVDRRLSQGL--CSIVAL--GREQE--ASHPV--LQNLVSYLKQKNAAGVITVPK-------GIVYAFPPCDFALKVLHRACPNIKG---LDEDHILVVAA--
---IEQLASCLPTAWDGAFYLKSSSFQCQMHVLRGDRSLVDRSLDDQAVCLRITQRMRLDPVKLDDVSERIQGPGGFCILLAVPCNEVGDADNKQPQRPLRNLVGYLRAKDSAGVVLLNPSGLNQNSGVLYAFPPCDFALSLLRESAPQLEKD---------------
-----SFLQHFPNVWSGALGIKTNSVPIKLHFVNGNSELGRSGNAPQRDALKITQRLRLDNGQVEHLHGVLTDPQYAC-CVAFVESTSPEDLTNHENIMQTNFIEYLKSKNIAGISSLS-PADTFTGARVHVFPPGELVEKYFAELAPALYSYL--------------
----PEVVRKATTIWNGALILKNSLFPTKFHLTDGDSDII-DSLMKGKNQLRITQRLRLDQPKLDDVQKRIATSSSHAIFLGVAGSITNEDASIQTRPMR-NLVSYLKQKEAAGVISLLNKET-EATGVLYSFPPCDFSTELLKRTCHNLTE-ESLKEDHLVIVV---
---VEQLASCLPSAWDGSFYLKSSSFQCQMHILRGDRSLVHTSLDSQVVCLRITQRMRLDPVKLDDVNQRIQGSSGFCILLAVPCSE-VGDGDKQPQRPLRNLIGYLRAKDSAGVVLLNPGGQNANSGVLYAFPPCDFALNLLRESAPQLEK----------------
---------EKFKHWDTSFLLKSSKFMTRLFFVSGSMDFADSIWSSPQSPFRITQRLRLDNEKLGEVNKRIEGPSNFCYVLAMDRNISPEDDANLTGREFRNLVMYLRQKDAAGVVMLEMAD--NVECIVHVFPPCQFSNNHLMEAYPRI------------------
-----------QTAWDGVFFLKSSSFPCRMHILRGDKSLVDRSAHEPGVCLRITQRMKLDPVKLEDVNQRINGNSGFCVLLAVPSPSTPTDTDKQPQRPLRNLISYLRTKDSAGVVLLVGPGSNDPQSVLHAFPPCDFAFELLKERA---------------------
--DEVAFTQHFPMVWTGRLTMKSTETMINLHLINGSETFLNGVLNPRRDSVKINQRLRLDNGQVDQVHGILTNP-EYACCLALSAVTDYDELAKNESNLKASFIDYLIKKKIAGVSCLEEVDQKFKSARVHVFAPGEIVSRYLSELASSLYDYLQSDTRYLLIVF---
--------RRIAPTWQGFVILKKTEYAIRLHRISGTEHLLQKLLRDA-SALKITQRLPLA--SQEALEDRLINSSQLSLMIAIACDKSIRP-----------LVNYLCEKEAAGVVSVPG-------GVLYVFAPSNMADRLVHACAPRV-NLLTPECSHLLF-----
--------RRIAPTWQGFIILKKTEYAIRLHRISGTEHLLQKLLRDT-SALRITQRLPLV--SQEALEERLIHSSQLSLMIAVACDKPIRP-----------LVNYLSEKDAAGVVTVPS-------GVLYVFAASNMADRLIQACARNI-TLLTPECGHLLF-----
--------ATYAATWSGKMALKKTDYPVKFYRVYGAERLVVKLLRDEDAPLRITQRLSLS--SQNLLFDKLSLCNDLSLGVIT----DLQP-----------LVNYFTNKEAAGVVTVPG-------GILYIFPFCEFALRLLKEFTPQI-NVFNENCPFLLG-----
--------ATYAPTWSGKMALKKTDYPVKFYRVYGAERLVVKLLRDEDVPLRITQRLSLS--SQNSLYEKLAGCSELSLGVIT----DLQP-----------LVNYFTNKDAAGVVTVPG-------GILYIFPFCEFALKLLADFTPQV-HVFNENCPFLLG-----
--------ATYAPTWSGKMALKKTDYPVKFYRVHGAERLVVKLLRDEDAPLRITQRLSLC--SQDLLFGKLTSCNEISLGVIT----DLQP-----------LVNYFTNKEAAGVVTVPG-------GILYIFPFCEFALKLLAEFCPQV-HVFNENCPFLLG-----
-------------MWHGFFALKNSSFPTELFLLEGASFLRRRHAR---QPVRRRASLRMDQSRLDEVSRRIKLPDNFAILLAV-----QGAVERQASAPEAILVTYLRNKEAAGVVILPAAKEGGPGAMLYAFPPGEFFH-HYLQAAHGALGRLED--EHMVFVIVS-
-----------PPCWQGGFVLKNIAFPVLFFFLDGDMGTADPNSPTGQTMFHLNQRLKLNEEKTTEISRRMKSTSKTCLIVAVPGGTVVGPVTSQHKPL-RGLVKYFKEKESAGIVSIPAITTETPKGVLHIFPPGPFAHEQLLKIGPNLQ-----------------
-----------QLCRNGLLVLKNSCFPTSMHILEGDQRVISSLLKDHGTRLKITQHLRLDQPKLDE----IKPGHGYAVLLATPSGLGTEEMPRQRW-LLRNLVSYLKQKQAAG------------------------------------------------------
-------KEKFPLKWNGRIGIKKSEVTTQWLKLHGDDDLLNSVFPPFDSVTKITGKFRMDNDSVQGLLNSMDRTEDNACLVVTPAGKTLDEVRDNRELFTKNLVNFFTEKVVAGITEFTH--RNGQTYIGHYFAPGPFVADYLSPKYPDFLDLL--------------
----------------------------------------------------------MDQSRLDEVSRRIKLPDNFAILLAVQGAVERQAEGGCRCACCAILVTYLRNKEAAGVVILPAAKEGGPGAMLYAFPPGEFFHHYLQAAHGAL-GRLE--DEHMVFVIVS-
----------------------------------------------------------MDQTRLDEVSRRIKLPDGFAILLALQGAIDRQAEPGLQIRLLRHLVTYLRNKEAAGVVGLPATKEGGTGGMLYAFPPGEFSQQFLQAARRTV-GNLD--EEHMVIVIVS-
--TMAELVRKATTIWHGALILKNSLFPTKLHLTDGDSEIIDSLMKDERKQLRITQRLRLDQPKLDDVQKRIATSSSHAIFVGVSGSTTNEDVSIQTRPMR-NLVSYLKQKK---------------------------------------------------------
-----------PDIWRGEILLKTNGFFFRCLHVIGDAEVGSQLGK-EHPTLRVQKRGSLDPTWMAEATHRIHDHRGLCLLLMLPDHETVSDLA---HFPLATLIAYMKLKQAVGVV---------PPLSVHLFAPSSFALSLLKQAAPRLSSELATADSYMVL-----
-----------PDLWKGKFLLKNNHFYFRCLMLIGNKDISQDLLDYSSPTLRISRRGTLDASWMAEATHRIHNRHNLCLMLILPDTETNKYLHNQSCFSLHALIAYLKLKQSAGIICPKEEKIQRSSLIVYLFAPSSFALSLLKQAAPCLSSDLATTSDYMIL-----
-----------PELWKGKFLLKNDHFYFRCLMLIGNKEIGEDLLNYSCPTLRISRRGTLDASWMAEATHRIHNHHNLCLMLILPDVETNKSINNPSCYSLHALVAYLKLKQSAGIISNREKEPQHSSLIVYLFAPSSFALSLLKQTAPCLSSDLATTNDYMVL-----
------------------------------------------------------------------LYEKLAGCNSKELSLGVITGKDLED-------LQ-PLVNYFTNKDAAGVATVPG-------GVLYIFPFCEFALKLLAEFTPQV-HVFNESCPFLLGAL---
----------LAVAWAGALQLKKWHFGVELRAVSGGRDVLSRCLPEEHTVLRVTKRAALTEATQALVSKSRAGDNNSSVSEAGPSARARADAAVLTCPLRAAFVLYLSKKNAVACVTTPA-------STLYILPPCSFAQQCVDKAAPLL-DSAG-------------
---------------EGRVVMKTQSASVRMQDLQGQGNPIVKFLP---DRLVVTQRMRLETEQVQLFSDRINEKPSFAIFLIRPPDEEEADSANIRAQFSKNFVRYFMERNAAGVVP---------------------------------------------------
-----------SVVWQGQLQIKSHVARVRAVHIWGPQDLA-GMLPGAEPTLLIKLRKRLE--SVEQLVSRCSSPTPPPLLTALE---LVQESPEQVDNLQRYFTQYLQDKNVAGLIPMPK-------ATVFVLPAAEPAARSVASAHPALADGLK---NKLLMVV---
---IVQLYQAYPLIWYARLHTSTQYFELAFRCVKGSTQVVYQVMSERKVPRKASGRMAID--GLKAVVDRLFQENDHAIVLRVD----YF-LLEQQAKPRDDFVRSLVNDMYIGFCSTEEMSAQQKKLSVHIFPPNKFVRKYLLDNHPTEYNALNLKEDYLVLVIG--
---LLELHSALGDCWEGELEIKKQPISARFSLIAGDQKRFSLSFA-SLSKMSIKQRLTFNHKLANSMSEKAKQGKVGAFLLITKG-RG-------SQRSIKSLNQYFQLTNSVGLVYVNSEER----IAMFIYPASSWTEGIISSYAPLLAGRLK-------------
-----------GVTWQGALA-KSGMHMCTLLCTTGGASAASGATPGEPATLDV--KLRVD---LSYVVHSLYSPHARALRLVTSGG------PEQRNKL-NDFLSYLADKNRAGVIKLEAA-AGLPPRTLYLVPPSEQVC-------AALGAEWSTGEPFLLA-----
