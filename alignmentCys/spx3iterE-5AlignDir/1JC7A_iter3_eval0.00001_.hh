CPERELQRREEEANVVLTGTVEEIMNVDPVHHTYSCKVRVWRYLKGKDIVTHEILLDGGNKVVIGGFGDPLICDNQVSTGDTRIFFVNPAPQYMWPAHRNELMLNSSLMRITLRNLEEVEHCVEEHRKL
CPERELELREEEANVVLTGTVEEIMNVDPVHHTYSCKVRVWRYLKGREIVTNDILLDGGNKVMVGGFGDPTICDNQVSTGDTRIFFVNPAPRSMWPTHRNELMLNSSLMRITLRNLEEVEMCVEG----
CPEKELERREEEANVVLTGTVEEIMNMDPVHNTYSCKVRVWRYLKGKEVVTNDILVDGGNKVMICGFGEPKICDNQVSTGDTRIFFVNPAPEYMWPAHKNELMLNSSLMRITLRNLEEVESCVEGV---
CPEKNLEDREEEANVVLTGTVDEIINMDPVHNTYSCKVRVWRYLKGKSSVDREILLDGGNKVMIGGFGNPQICDNQVATGDTRIFFLNPSSLSMGPEHKNELKLNSSLMRITLRNLEDVEHCVEGKR--
CPERDLENREEEANIVLTGTVDEIINLDPVHNTYSCKVRVWRYLKGKSNVNQEILLDGGNKLMIGGFGNPGICDNQVATGDTRIFFLNPALEAMGPEHKNELMLNSSLMRITLRNLEDVEHCVEGKQ--
-----MEIREEMADVVVTGTVKSVSALQRGSKLYSAVVEIKRVMKGEDLLGESSVSNAGQEVLVQGFGDPRFCENTVRVFDTRILMLS---------DGGILRLNSSVVRISLRNLDRVEHAVNGKK--
CPERALERREEEANVVLTGTVEEILNVDPVQHTYSCKVRVWRYLKGKDVVAQESLLDGGNKVVIGGFGDPLICDNQVSTGDTRIFFVNPAPPYLWPAHKNELMLNSSLMRITLRNLEEVEFCVEDKPGT
CSEKELEKREEEANVVLTGTVEEILNMDPVHNTYSCKVRVWRYLKGKTMVNGEVLLDGGNKVMIGGFGDPHICDNQVATGDTRIFFVNLAPEYMWPNHKNELMLNSSLMRITLRNLEEVEHCVE-DKPV
CPERALERREEEANVVLTGTVEEILNVDPVQHTYSCKVRVWRYLKGKELVARESLLDGGNKVVISGFGDPLICDNQVQVGDVSVYAGWPGIPIVWPCVWTRGLMPASFPNMSLRGLPNLGVCLPDKPGI
-------------------------------------------------------------------MPPLPLERRPGLQPGVSVLVR---YFMVPCNACLILLATSTLGFAVLLF--LSNY---KLGT
-------------------------------------------------------------------MPPLPLEHRPRQEPGASMLVR---YFMIPCNICLILLATSTLGFAVLLF--LSNY---KPGI
--------------------------------------------------------------------MPPLPLERDPRQQRRVSLV--VRYFMVPCNVCLILLATATLGFAVLLF--LSNYKSSPAGQ
-------------------------------------------------------------------MPPLPLERDPRQQHGVSLLVR---YFMIPCNVCLILLATSTLGFAVLLF------LNNKPGT
----------------------------------GIGVRVWRYLKGKDVVTQESLMDGGNKVVIGGFGDPLICDNQVSTGDTRIFFVNPAPRYLWPAHKNELMLNSSLMRITLRNLEEVEHCVEGRFG-
CHEDNLMKRVRRATAIVTGTVEQKMNGYPDG-KYKAKVRVWRVMKGTNEIPTAPLSRGVAEVMVDGFGDPHICDNEVDQGDTRIFLLDKKP--AGP--PDEFELTSSLIRVTLDNLI------------
CQEYPVEKREEIANVVISGYIHRIMR-KPRTIMYDCDIEVVRVFKGSEALYSNVVPMDGNTIQIGGFGDPAICDNAVTPGETRIFMMNK-------NGNGHLMLNSSVIRITLNNLVQAE---------
----------STSTVILTGTVQYCDVIR--AGSYKCNIKIWRVMKGNSLAMNLITIQKKKYVDVYGLGNSAFCKSNVERADTRIFFLNK--------LNNRLVVSASLDRITLKNLENTQ---------
----------------------------------------------------------------------SALDRDQLYQHKVSLVVR---YFMIPCNICLILLATSTLGFAVLLF--LNNC-------
---------------------------------------------------------------------PPGPERERRYQHKVSLVVR---YFMIPCNICLILLATSTLGFAVLLF--LNNC-------
-------------------------------------------------------------------MPPLPLARDTRQPPGASLLVR---GFMVPCNACLILLATATLGFAVLLF--LNNY---KPGT
---------IKRADVILSATVLSVKKLKSYSDFFEARISIRRIWKGEHLIPSRRF----GQLTVQGIANPSICHSRAVEKDTKIFLLKL-------LKDGRIQLDSSLLSINLANLDAVNAYVQ-----
-------------------------------------------------------------------MPPLALERDPQRQHRVSLLVR---YLMIPCNACLILLATATLGLAVLLL-------------
--------------------------------------------------------------------------ERDRMYQHKVSLV--VRYFMIPCNICLILLATSTLGFAVLLF--LNNYK------
----------QNAQVVLTATVQQVTI-DPRN-NRLATVRVKRVIKGREFIG------NAHQIAIYGLGDTKYCRSMLYERDTKVILLR--------EQNGLLYLNSSLISINLDVLDLI----------
----------ENSDVILAGIISGISY-RVDS-LYIITVTPRRIYKGREYLD------EGKIIVIGNVLETEPCAHRLLASDVRIFALT--------NQQGTFLLDAPMIRVSLPVFDTF----------
---------FENSDVIVAGIINGISY-RVDS-LYIITVIPGRIYKGREYIE------ENKNIVISNILETEPCAHRLLASDVRIFALT--------NQQGSFLLDAPMIRVSLPIFDTLY---------
-----------------------------------ATVRVKRIFKGFNIIN------SRKKIVIHGLGGAQICRSVLHERDTKIILLN--------ELNGLFYLNSSL---------------------
----SLEERQNRSQVVFTAVIESCQTSESGSPNLAINVRIKKVVKGLDRMW------EGRRVRVEGLRDPRICPSRVRLRDTRIFLASYVNQPMAPVAPIRLRLNSSLMSVSLRHLQQLRAF-------
----PVAERARRADVVLSGTVVRTDALQPGL-TYSARVRVRTVLKGKSRLREIPASDQPRVYNVSNFGRRAQCYSEVTQGDDVIFFLG-----------------------------------------
CSPSPLQQRISNADYVITAKVLNINR-NQQQPTYSSNVEVIEVFKPPRLSL--P-----LSITIDNFGNTRICRSNVNTNDTRILLFNG--------SSQKYVLSTSFEYVTEERLKLIRTAVQDP---
