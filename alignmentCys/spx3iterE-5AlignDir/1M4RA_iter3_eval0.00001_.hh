SHCRLDKSNFQQPYITNRTFMLAKEASLADNNTDVRLIGEKLFHGVSMSERCYLMKQVLNFTLEEVLFPQSDRFQPYMQEVVPFLARLSNRLSTCHIEGDDLHIQRNVQKLKDTVKKLGESGEIKAIGELDLLFMSLRNACI
DSCRLRKINFQQAYIRNRTYTLAKLARLSDQDTDNRLIGQQLYINVQETNRCYVMKKVVKIIVDEVLLPAPKNYYPHIHEVAQFLAALTTELSGCKFSGHREHIERNLEQMRDKIKQLGQSGKNKAIGELDLLFDYMENACT
HACRLRKINFQQPYIRNRTYTLAKTASASDTDTDNRLIGQQLFVNIRENNRCYMMKRVVELVVKDVLLAEVKNQYPYVEQVAQFLASLTSELSGCQFLGKRDHIEKNLEQMKNKMEQLGENGKLKAIGELDLLFDYMENACT
QACRLRKINFQRPYIRNRTYTLAEMARVSDQDTDNRFIGQQIYVNIRENNRCYMMKRITEIILKDVLLTEAKERYPYAEDVAQFLASLTSELGRCKYSGNREHIEKNLEEMKSKMKELGENGKNKAIGELDLLFDYIENACT
KQCTIRKTFFLQTFMKKNIFALAEQARIMDKDTANKIFGSYLFFGVKEKDHCYLMKNVVNSFVENVLHESSKKY-PHIDNAIAFFVNIEKDLAGCKSEEGDQ-IQRNVEQMINKIKMMGPDGKNKAIGELDLLFAHLRKCTL
SHCRLNKSDFQEPYIFNHTFTLAQEASLADNITDVRLIGNKLFQGINVTKRCYVLKQVLNFILEEVLFPRSDEFHPHMKKVVPFFSKLSKKLSQCHIENDNQHIQRNVQNLKNTVKKLGESGEIKVIGELNLLFITLKRECA
HHCKLDQSNFQQPYITNRTFTLAQEASLADNNTDVRLIGNNLFQGVNMRERCYLVKQVLNFTLEEVLFPNSDRFHPYMQEVASFLDSLSKKLSQCRIKGDDQHIQRNVNNFKDTVKKLGESGEIKVIGELYLLFMALKNECT
SHCRLDESDFQQPYIINRTFKLADEASLADNNTDVRLIGEKLFHGVNVRQHCYLMKQVLNFTLEEVLLPYSDRFQPYMQEVVSFLAGLSNKLSQCHIEGDDQHVQKKVQNLKDTVKKLGESGDIKAIGELNLLFMRLKNACI
DRCRLKNSDFEVSFINQRILNLSKKASDQDTITDIRLINSTFPQDVQVGDRCYLMKQILNFTLEVVIPPQYTQLMPFIQDVESFFSDLHRKLRYCVSVFTPTLWESLLRVLPTLLSQLGKKGMNKVIGELNLLFMALKHACV
HSCVLRTV--LPSYIKNCTLTIAEQARLSDKDTDNRFIGQHLYANIKEKDHCYVMKRVTDIIMKNVLLKSAPGQYSNVKEVANFLAERNGELHGCKPSGKNEHIERNLKQMKDKLDKLGENGKNKAVGELDLLFEYLVKACT
DRCRLKNSDFEVSFINQRILNLSKKASDQDTITDIRLINSTFPQDVQVGDRCYLMKQILNFTLEVVIPPQYTQLMPFIQDVESFFSDLHRKLRYCNTVSGGQHVESHVETLKKTVTKLGKKGMNKVIGELNLLFMALKHACV
DHCGLHKASFEGPFIINRTFLLAEEASFADNNTDVRLIDDRLYQGVSMKDRCYVMKQVLNFTLEEVLLPNSDRPLPYLQDVVSFLEGLNNDLRLCHIKGDNRHIQKNVQNLKDTVEKLGESGVIKVIGELNLLFFYLKNACV
SQCKLEAANFQQPYIVNRTFMLAKEASLADNNTDVRLIGEELFRGVKAKDQCYLMKQVLNFTLEDVLLPQSDRFQPYMQEVVPFLTKLSSHLSPCHISGDDQNIQKNVRQLKETVQKLGESGEIKAIGELDLLFMSLRNACV
SYCKLNESDFLQPYFTNRTFRMAEEVSLVDDNTDVSLIGKKLYHGVNENERCNVMKQVLKFTLEEVLLPELDRFKPYMKDAVSFLEMLSNKLSQCHIENDDQHIHRNVQKLKDTVKELGESGKIKAIGEVNLLFMFLKEACI
SHCRLDKSNFQQPYITNRTFMLAKEASLADNNTDVRLIGEKLFRGVNMGERCYLLKQVLNFTVDEVLLPQPHRFQPYMQEVVPFLARLSTRLSSCHIEGDDQHIQRNVQKLKDTVRKLGESGEIKAIGELDLLFMALRNACV
SHCRLDKANFQQPYITNRTFMLAKEASLADNNTDVRLIGEKLFHGVHMSEHCYLMKQVLNFTLVEVLLPQSDRFQPYMQEVVPFLDRLSNKLSQCHIQGNDQHIQTNVQKLKDTVKKLGEIGEIKAIGELGLLFMALRNACV
SYCKLNESDFLPSYFTNRTFRMAEEVSLLDNNTDVSLIGKKLYHGVNENEHCYVMKQVLNFTLEEVLIPESDRFQPYMKEVVSFLKKLSNKLSQCHIENDDEHIHRNVQKLKDTVKELGESGKIKAIGEINLLFMFLQEACI
SPCRLNSSDFQQPYITNRTFMLAEEASLADNNTDVRLIGDQLFHGVNMGDRCYLMKEVLNFTLEEVLLPQSHRFQPYMQEVVPFLAKLSNRLSQCHIESDDQHIHRNVQNLKDTVTKLGENGEIKAIGELNLLFIALKNACI
SHCVLKKSDFS-TYITNLTFMLAEEASFADNNTDVRLIGGKLVRGVNVKERCYLMKQVLNFTLEEVLFPHSDWFQPHMKEVVSFLDGLSNKLSPCHIRGDDQHIQRKVEQLKDTVKKLGESGDIKAISEVNLLLSYLKDYCV
SHCRLDESNFT-SYITNLTFMLAKEANSADNNTDVRLIGNELFHGVNEKDHCYLLKHVLNFTLEEVLFPESDRFQPYMQQVLSFLAKLSNNLSQCHIEGDDQHIQRNVQQLKDTVKQLGESGEIKAIGELDLLFMALRTACV
AHCRLHNFQ-E-PYITNYTFMLAKEASFEDNNTDVRLIGDKLFRGVSMRDRCYLMKQVLNFTLEEVLLPQSDRFQPYMEQVVPFLADLSRKLSLCHISGDDQHIQRNVQQLKDTVKKLGESGEIKVIGEVDLLFMALKNSCV
--------DFQEPYIINYTFTLAQEASLADNITDVRLIGKKLFQGINVTKRCYLLKQVLNFTLEEVLLPQSDKFQPYMKEVVPFFSKLSKKLSQCH-EYDNQHIQRNVQNLKNTVKKLGESGEIKVIGELNLLFMALRRECA
SKCKIARSIFQARHINNQIHDLSKMASDADDDTTTRFIGHLIVKDVQETNRCYVMKEVLNFQLREVLLSYEQKYEKYMRDVLPFLNNVKSKLSSCTMNGDTSKVRRTIDDMRSKVKKLGKHAVVKAMSEMDMMLLYLRGYCS
-SCKIAKS---PPHIGKITYTLAKWAQRSDADTVTRLIDPKLFSNKLEEDHCYQMKEVLNYLLQQVLMNSSEAQYPHLRDVLEFFAKLSEDLTDCKLKGDKKGIESNMEVLKDKIKKLGESARNKAIGELDQLLDTTLKVC-
SPCKLPKSEFHLNEISNQIVNMSQMASLEDMDTSTRFIDSSLFQDVATVDRCYVMKLVLDFALSEGVKPYGDKFQPYTQHVFNFLTSLKSKLSKCVMKFDNIHVLEKIAAMKNKLKQV------------------------
-PCRVEGVALPR--LLEAFRTVKETMQIQDNITSVRLLQREVLQDVSDAESCYLVHDLLKFYLSTVFH--AEMLPSFSTLANNFL-FIVSKLQ--QKNETSDSAHRRFLLFKSTFKQLEETALSKAFGEVDILLTWMGKF--
-PCRVEGVVLQE--LWEAFWAMKDIVQAKDNITSVRLLRKEVVQNVSGAESCYLIHALLKFYLNTVFD--ADIRKSFSTLANNFF-VIVSKLQPSKENEMSESARRRFLLFQRAFKQLDQAAQTKAFGEVDILLTWMEKF--
-PCRVEGIVLQE--LWEAFWDMKDVVQAQDNITNVRLLRKEVLQNVSEAESCYLIHSLLKFYLNTIFE--VKILRSFSTLANNFV-VIMSKLQPSQENEMSENARRRFLLFQREFKQLDEVALTKAFGEMDILLTWMETF--
-SCRVKGVALQE--LWKAFWAVKDTVQAQDNVTSVRLLRREVLQDISDAESCHLIHALLKFYLNTVFK--VETVKSFSTLANSFL-VIKSQLQPSEENEMRESAHRRFLLVQRAFKQLDEAALTKAFGEVDILLSWMEKF--
-PCQVKGVVPQK--LWEAFWAVKDTMQAQDNVTSVRLLHQEVLQNVSAAESCYLVHSLLEFYLKTVFD--AETLKSFSTLANNFI-LIVSQLQPSQENEMRDSAHRRFLLFRRAFKQLDEAALTKALGEVDVLLTWMQKF--
-PCRVEGVALPS--LWEAFRSLKDTVQAQDNITSIRLLRREVLQNVSDTESCHLIHDLLKFYLNTVFH--AEMLPSFSTLANNFV-FIVSKLQPTQKNEMSDRARRRFLMFKRAFKQLEEAALTKAFGEVDILLTWMGRF--
-PCRAEGVNLQQ--LREAFWTIQETVQAQDNTTSVRLLRQEVLQNVSDAESCYLIHALLKFYLNTVFK--AETLKSFSTLANNFI-VIMSKLQPSQENEMSESAHKRFLLFHRAFKRLDEAALTKALGEVDILLTWMEKF--
-SCRVEGVVLQE--LWAAFRAVKDVVQAQDNITGVRLLRKEVLQNVSDAESCYLSQALLKFYLDTVFG--AEILKSFSTLANNFI-AITSRLRPSQENEMSESARRRFLLFQREFKQLDEAALTKAFGEVDILLTWMEKF--
-PCRVEGVVLQK--LWQAFWAMKDIVQAQDNIMSVRLLRKEVLQNVSETESCHLIHALLRFYVNTVFD--VEILKPFSTLANNFF-VIVSKLQASQE-KMRKSARRRFLLFYQAFKQLDEAAVTKAFGEMDILLSWMEKF--
-SCRVKGVDFQE--LWEAFQAMKDIVQAQDNITSIRLLRREVLQNVSDTESCYLIRALLKFYLNTVFE--AEILKSFSTLANNFI-FIASKLQPSVSRTKGESARRRFLLFQRAFKQLDEAAQTKAFGEVDILLTWMQKF--
-PCQVTGVVLPE--LWEAFWTVKNTVKTQDELTSARLLKPQVLQNVSDAESCYLAHSLLKFYLNTVFS--VKVLKSFSTLANNFL-VIMSKLQPSKDNAMSDSARRRFLLFHRTFKQLDEVALAKAFGEVDILLAWMQNF--
-PCRVQGVELQG--LRRAFWTMKDTVQAQDHNTSVRLLRKEILQNISDAESCYFIHSLLGFYMKTVFN--NDILQSFSTLANNFI-VISSKLQPSEENEMIKSAHRQFLQFQVAFKKLDEAALTKALGELDILLTWMEKY--
-SCWVQRAILRD--LWRAFQAMKNTVQALDCDTDTRLLRQEFLRNASWAEICHLTNSLLDFYLKNIFT--AKILPPFMSLANNFF-AILQKFKRCKERGLNESAQRKFELFQQEFTQLNEVRLTKALGEVDILLTWMEKA--
-PCRVQGVALRE--LREAFWTVKDTVQAKDNITSVRLLRKEVLQDVSDAESCYLIRALLKFYLNTVFD--ADIRRSFSTLANNFF-VIASKLQPSQEDEMSESARRRFLLFQRAFKQLDQAAQTKAFGEVDILLTWMEKF--
-SCQVTGVVLPE--LWEAFWTVKNTVQTQDDITSIRLLKPQVLRNVSGAESCYLAHSLLKFYLNTVFS--AKVLRSFSTLANNFI-VIMSQLQPSKDNSMSESAHQRFLLFRRAFKQLDEVALVKAFGEVDILLTWMQKF--
-QCELNSVSFRE--LRDNFDAIKENVQTQDIRTDVILLKESVLREVPMSESCCLLRHLLRFYVESIFPTSNLLRRKTSTLANAFL-SIKAKLRECHNQNKGEETNRRFKLVLDEYQKLDTTAAIKSLGEMDVLFAWMEGF--
-HCRVQRAILQE--LWRAFQAMKSTVQALDHNTETRLLRQEFFQNATWAEICCLNYSLLDFYLSNIFT--AEILRPFITLANNFF-VVLKKLQHCKERGMSENSERKFQLFQKEFTKLHEARLTKALGEVDILLSWMEKA--
-VCTVV-FNIYE--IRNEFSEIRGFVQAEDEHWDNRILKNSALQDIKPTERCCFFRHLLRFYLDKVFPSSPHIRRKVSSIANSFL-GIKKELRLCHDQMTGEEAKEKYMQILSHFEE---KAVIKALGELDILLRWIERT--
-SCVIT-ANLQA--IQKEFSEIRHSVHAEYQYIDVRILTTELITRNGLSDRCCFLRHLVRFYLDRVFTPDHHTLRKISSLANSFL-IIKKDLSVCHSHMAGEEAMEKYNQILSHFTEIDYIQCLYGLWELGILLRWMKQM--
-ACRLF-VHFHD--LKENFADIKHTIVSIPVLLSIRRVLRVGPRNSKFGDSCCFLRHLLKFYVEKVFTVDGNIRTKTSSLANSFL-SIKTELRKCHERNMGEESRQKIEAILHAYKNMDNAAATKAIGELDILLEWMEKY--
-ACRVS-ISMTE--IRAGFTAIKANIQSRDPIRTLSILSHPSLHKVKSSDRCCITYQLFTFYVDKVFTEDSFVNRKISSIANSFL-STRRKLGQCREQNNGEESTEKFKQILANYEGLNTSAAMKSLGELDILLDWMEKS--
-SCVIT-ANLQA--IQKEFSEIRDSVQAEDTNIDIRILRTTSLKDIKSLDRCCFLRHLVRFYLDRVFTPDHHTLRKISSLANSFL-IIKKDLSVCHSHMAGEEAMEKYNQILSHFIELEQAAVVKALGELGILLRWMEEM--
-QCVIS-VNIHE--IRHSFRAIKDMTQSKDEHTDIRLLHQSSLQDTEPMDRCCFLRHLLRFYLTTVFVSSSPIIRKVSRIANTFL-SIKKDLRLCHQVSMREDVKHKYGLIMSQYEKMDHSAGLKALGELDILLDWMDKA--
-SCVIA-TNLQE--MQTEFSQIRDRMQAEDEIMDLRILKTES-QDTKPADQCCLLRHILRLYLGMVFTSNKLILQKLSSLANSFL-TIKKDLRLCHARMTGEEAMEKFNQILSRFEELKQEAVVKALGELDILLQWMGEM--
-HCSVS-VDIQK--LRDEFSGIKALIQSEDESMNIRILKRSSLQVTEPADRCCFLRHLFRLYLDRVFTHDSDILRKISTIANTFL-GVKRDLRLCHDHSTGEEARGKFSRVLSHFEKLDEAAAVKALGELDILLSWMEDI--
-SCSFN-VYTHE--LRKYYADMRHHAISGDTETGVKILDKSLMKDVQEDQTCCFLRLLLRFYVERVFPSEPHQLRCSSAVANAFV-SIRRDMQKC----HAEETHRQIDSMHTAFDKLQQLAAQKAVGELDTVLDWLEVF--
-SCKVN-IHTHE--LRHHFQYVRQGMISGDDHKGIRLLRKDVMSSLQATESCCFLSQLLHFYMDTVFSSHSLHRRTTSVLANSFL-SISKDLRVCHANAHGENTRLQLKSIQTAYEKLDAAGTVKAIGELDSLLEWIESF--
-ACELSGVSFQE--LRDYFGAIRGATQTQDRMTDVVLLKRVALQGVPISESCCLLRHLLRFYVESVFATSSLLRRRTSRLSNSFL-SIKGKLRYCHDQKKGEESTSRFQLIREEYEKV------------------------
-------------------------------DLDAALLDQTVEDTLKTPFACHAINSILEFYLSTVLPTDTKDLKPHMESIQQIFDQLKSDVTRCRHYFKCKHHF--INTLNSTYTQMESKGLYKAMGELGLLFNYIET---
-------------------------------NIDIALLDEDLLYGFKDHFGCQLLKEMLKFYLEEVLPKKDLDIQPTVSHIGEMLFNLKQKITLCQNFFACSNKSRAVKRIKETYNKLQEKGVYKAMGELDIFIDYIED---
-------------------------------DLDSALLDQTVEDSIKSEFACQTIDSILDFYLKTILPKDTADVKPHVLSIQEIFDQLRTDVTQCRNYFSCKKHF--IRNLTAAYDQMQNKGLFKAMGELDLFFNYIES---
-------------------------------DLDLVLIDQSIVESFKTPFACHVMDGILKLYLDSVLPRETRDLQPHVESIQQILDQLKTEVNNCKHFFACKNQF--MNTLTSAYTQMQEKGLFKAMGELDLLFNYIEM---
-------------------------------DLDIVLLDQSIVDTFKTPFACHLMDGILRFYLDSVLPRETRNLKPHVESIQQIFDQLKIEVTNCKHYFACKNRF--INVLNSTYTKMEDKGLYKAMGELDLLFNYIEN---
-------------------------------DLDSALLDQSVEDSFKSPFACYAMNSLLEFYLDTVLPTEIKDLKPHVESIQEIFNTLKRDVTQCRKYFSCKKQF--INNLNSTYTQMESRGLYKAMGELDILFNYFET---
-------------------------------DIETALLDDDMLSDFQSPFGCHAINDVLRFYLDTVLPTDRKDYTYHIDNIGGIFNELKKEMLHCRNYFSCKKPF--LDSIMTTYKKMQGQGLYKAMGELGLLFNYIEE---
-------------------------------ELDTILLKANLLEDFKSYLGCQAMLDMIRFYLEEVMPKNK-EIKHHVGSLGNKLQSLQHQLKRCHRFLPCEKRSKAIEEIKETYGQLKEKGIYKAMGEFDIFINYLES---
-------------------------------DLE-PLLNEDIKHNINSPYGCHVMNEILHFYLETILPTNPLHSTTPIDSIGNIFQELKRDMVKCKRYFSCQNPF--VNSLKNSYEKMKEKGVYKAMGELDLLFRYIEQ---
-------------------------------ELDIMLLKQDLLEDFKGYLGCQSVGEMIQFYLEEVLPNSSNTINQDVGFLGDMLLELRQLIKRCHRYFICEKKSKTIKDIKDTYKKLQDKGIYKAMGEFDIFINYIEE---
-------------------------------DME-PLLNENVQQNINSPYGCHVMNEILRFYLDTILPTSHLHSKTPIDSIGNIFQDLKRDMLKCKNYFSCQNPF--LASIKNSYEKMKEKGVSKAMGELDMLFKYIEQ---
-------------------------------ELNIQLLNSELLDEFKGNFGCQSVSEMLRFYTDEVLPRTSTSHQESMGDLGNMLLGLKMTMKRCHHFFTCERRSKVIKQIKETFEKMDENGIYKAMGEFDIFINYIEE---
-------------------------------DLEIGLLDQSIAHSLRSPFGCHAMKNIIDFYLKTVLPTENKNYKPHIESIQLIFDQLKNDMIKCKNYFQCKKPF--ITQLNSTYTQMEGKGPYKAIGELDLLFNFIEM---
-------------------------------KLDNILLTGSLLEDFKSYLGCQALSEMIQFYLEEVMPRHDPDIKNHVNSLGEKLKTLRLRLRRCHRFLPCENKSKAVEQVKSAFSKLQEKGVYKAMSEFDIFINYIET---
-------------------------------QLDDMLLSESLLEDFKGYLGCQALSEMIQFYLVEVMPQHSPDVKEHVNSLGEKLKTLRLRLRRCHRFLPCENKSKAVQQVKDAFSKLQEKGIYKAMSEFDIFINYIEA---
-------------------------------QLDNVLLNKSLLEDFKGYLGCQALSEMIQFYLVEVMPKHDPDIKEHVSSLGEKLKTLRLRLRRCHRFLPCENKSKAVEQVKNTFNKLQEKGVYKAMSEFDIFINYIEA---
-------------------------------QLDNMLLNESLLEDFKGYLGCQALSEMIQFYLEEVMPQHEPGIKEHVNSLGEKLKNLRLRLRRCHRFLPCENKSKAVEQVKSTFSKLQEKGVYKAMSEFDIFINYIET---
-------------------------------QLDNMLLNGSLLEDFKGYLGCQALSEMIQFYLEEVMPQQGPDIKEPVSSLGEKLKTLRLQLRRCHRFLPCENKSKAVEQVKSAFSKLQERGVYKAMSEFDIFINYIEA---
-------------------------------QLNSMLLTESLLEDLKGYLGCQALSEMIQFYLKDVMPQHSPAIREHVNSLGENLKTLRLRLRQCHRFLPCENKSKAVEQVKSAFSKLQEEGVYKAMSEFDIFINYIET---
-------------------------------ELSIQLLSSDLLEEFKGNFGCQSVLEMMRFYMEEVLPSSSEHHQQSVGDLGNLLLSLKAMMRRCHRFLTCEKRSKTIKQIKETFEKMNENGIYKAMGEFDIFINYIEE---
-------------------------------QLDSMLLNRSLLEDFKGYLGCQALSEMIQFYLEEVMPQHSPDIKQYVSSLGEKLKTLRLRLRR-HRFLPCENKSRAVEQVKNAFTKVSSAGGRVGLGEHDFCPLPRGR---
-------------------------------ALEIVLLQDDLLQEFKGNLGCQSVSETIRFYLEEVLPQYKMN----VSFLKDKLLDLKHTLRRCHNFLPCERKSKAIKQIKQTYNKMHEQGIYKAMGEFDILIDYIED---
-------------------------------ELSVRLLDEPLLEDFKSYLGCQALSDMITFYLEDVIPRET-EIKNSVVSLKDKLMGLRRTLKQCHRFLPCEVKSTAVQKIKSTYEQLNGNGVLKAMGEFNIFINYMET---
-------------------------------QMGDLLLTGSLLEDFKGYLGCQALSEMIQFYLEDVMPKDGEDIKEHVNSLGEKLKTLRLRLRRCHQFLPCENKSKAVEEVKSAFSKLQERGVYKATGEFDIFINYLEA---
-------------------------------DLSIQLLSSDLLEEIKGRLGCQSVSEMMGFYMEEVLPSTSTEHQHSMGDLGNLLLGLRAMMRRCHRFFTCEERSRSMEHIKETFSRMDKNGIYKAMGEFDNFINYIEK---
-------------------------------KLETRLIDRSLLEDLKSYLGCQALSQMIKFYLEKVMPQEE-GVKEKVGSLGDKLQILRLRLKRCHRFLPCEDESKVVNRVKDTYEKLQEQGVYKAMGDFDIFINYMEE---
-------------------------------KLETKLIDKSLLEELKSYLGCQALSEMIKFYLEEVMPRNELDVKEDVGSLGEKLKALRLRLKRCHRFLPCEDNSRVVKQVRNTYKELQEQGVYKAMGDFDIFIGYMEE---
-------------------------------ELDILLLKDDLLEDFKGYLGCQSVSDMIRFYLEEVLPKSSKDVKRSVGTIGNMLLDLRKTLKRCHRFFICEKRYQTLKQIKETYDKLQEKGIYKAMGEFDIFINYIEE---
-TCSVT-VHTHE--LRKHYTEIRSSVIAADSQMGVRLLR-DVMRNIQE--YCCFLRLLLRFYVERVFVSQPLHRRSTSALANSFL-TINKNLRQ------GEDTRRKMDSLQAQFDKLEYQAAVKAIGELDSLLDWLEEL--
-SCSVN-VNLEE--LRKHYYSIRLNAITGDDEIGVKFLD-SLIEDVQD--RCCFLRLVLRFYVERVFRSQPQDERILSSLANTFI-IIRKDMHK------EEPTQKRVDALHQAFNQLEGKAARKAVGELDIILGWLQDS--
-ECMIA-MNMHE--IRDNFQAIKSAIQAKDVFRNSTILSKSSLHNIKKTTRCFLLHSSRRYFM--IFMTVENFLQKINQLRFSLL-LMKMDVLILYEQKSKKETTHKYQLVLTNYEQLEKAAAIKSLGELDILLSWMDKR--
-RCPIS-VDMRH--LEESFQEIKRAIQTKDNFQNVTILSTLALQNTKPLDVCCVTKNLLAFYVDRVFKSDPQILRKISSIANSFL-YMQKTLQQCQVQRQSQEATNATKIIHDNYDQLEPSAAVKSLGELNVFLAWIGKN--
-RCLIA-MDTRH--ISQSFQQIKETIQAKDTFPNVTILSTSTLHSIKPLDVCCVTKNLLAFYMDRVFKLSPQILRKISSIANSFL-YMQKVLQQCQEQRLGQEAANATRIIHDNYNQLERSAALKSLGELDVFLAWIDKN--
-RCPIS-MDMRH--LQESFQDIQRAIQANDTFQNITILSALSLQGIKPLDVCCVTRSLLAFYMDRVFKPDPHILRRISSIANSFL-YMQKTVQQCQVQGRREEATNVTQIIHNNYNQLERVAAVKSLGELNVFLAWISEN--
-RCLIS-MDIHP--VEETFQEIKKTIQAKDTFQNVTILSTSTLHSIKPSDVCCMTKNLLAFYVDRVFKLNPQILRKISSIANSFL-YMHKALQRCQAQRQREAATNATRIIHDNYDQLERSAAIKSLGELDVLLAWIDKN--
-PCRIS-MSMSE--IRAGFTAIKTNIQARDPIRTLSILSHPSLHRVQPSDKCCIVHKVFNFYVDKVFKENSYINRKISSIANSFL-SIKRKLEQCHDENKGQEPTERFKQILVNYEGLNTSAAMKSLGELDILLDWMEKS--
-RCLIS-VDMNQ--LEESFQEIKRAIQTKDTFQNITILSTVTLQSIKPVDVCCVTKNLLEFYMRRVFKLKPQILRRISSIANSFL-YMQKTLKQCQEQRGSEEATNATRIVHDNYDQLGPSAAIKSLGELNIFIAWIEKN--
-SCVIV-TNLQE--IQSEFSEIRDSVQASDRNTDFRILRSTSLQDTEPSDRCCLLRHLLRLYLDRVFKSDHHTLRKISSLANSFL-TIKKDLQLCHTYMAGQEAAETHRQILSHFEQLEQAAAVKVLGELDILLRWMEET--
-SCVVT-TNLQE--MHNGFSEIRDTVQAKDKIIDIRILRKTSLQDTKPADQCCLLRHILRLYLNTVFKPDHHILRKISSLANSFL-TIKKDLRLCHAHMTGEEAKEKYSQILSHFEELEQEAVVKALGELDILLRWMEET--
-SCVIT-TNLQE--IRNGFSEIRDSVQAKDGNMDMRILRRTSLQDTKPAGRCCLLRHLLRLYLDRVFKPDHHTLRKISGLANSFL-TIKMDLRLCHAHMTGEDAMKKYSHILSHFEELEQAAAVKALGELDILLRWMEET--
-SCVIA-TNLQE--IRNGFSEIRGSVQAKDGNIDIRILRRTSLQDTKPADQCCLLRHLLRLYLDRVFKPDHYTLRKISSLANSFL-TIKKDLQLCHAHMTGEEAMKKYGQILSHFEELEQAAVVKALGELDILLQWMEET--
-SCVIS-INLQK--IRNEFSEIRGSMQAEDGNIDIRILRRSSLQDTVPSDQCCLLHRLLRLYLDRVFKPDHYMLRKISSLANSFL-TIKKDLRLCHDHMTGEEAMEKYSQILSHFKRLEQAVVLKALGELDILLQWMEET--
-SCVVT-TDLQE--IQSVFSELRHSVQAKDDITDIRILRRTSLQDTKPKGRCCLLRRLLRLYMERVFNPDHQTLRKISRLANSLL-TIKKDLRLCHANKKGKEAMEKYNQILRQFEQLEQAAVVKALGELDILLQWMEET--
-SCMIT-TNLQE--MRNGFSEIRDSVQAKDEIIDIRILRKTSLQDTKPADQCCLLRHVLRLYLDRVFKPDHHILRKTSSLANSFL-TIKKDLRLCHAHMTGEEAMEKYSQILSHFEELTQAAVVKALGELDILLQWMEEM--
-SCVIT-TNLQE--MRNGFSEIRDRVLAEDEIIDVRILRKMSLQDTEPAGQCCLLRHVLRLYLDKVFKPDHHILRKLSSLANSFL-TIKKDLRLCHDRMTWEEAMEKYSRILSHFEELKREAVVKALGELDILLRWMEEA--
-NCVLS-MNYQE--MRNEFLEIKGYVQAEDGNLDKRILKSSALQDTKPTERCCFFRHLLRFYLDRVFKTNHHIRRKVSSLANSFL-GIKKELRLCHDHMIGEEAKKKYTQILSHFEELDQTAVVKALGELDILLRWIEKT--
-SCVIT-TNLQG--IRSGFSEIRDSVQAKDEIIDVRILRKTSLQGTKPADQCCLLHHILRLYLDRVFKPDHHIFRKVSRLANSLL-TIKKDLQLCHAHMSGEEAKEKYSQILSHFEELPQAVVVKALGELDILLQWMEEA--
-SCSIS-ADLQE--MHQHHSNIRLNAITEDEEIGVKLLS-RLMEDVQD--RCCFLRLVLQFYIDKVFPSHPQNQSSSSSLANTFI-IIVRKIQK------EQETQKKVDSLLDAFNKLESKAVLKAVGELDTVLQWLQLF--
-SCSVS-VHTHE--LRKYYAHIRSNAISEDNEIGMKLLD-SLMKNVQD--TCCFLRLVLRFYVERVFSSEPEQQRCSSALANAFV-SIRRDIHK------GEETQRTIDSVHAEFIKLQNRAAQKAVGELGTVLEWLEAL--
-SCSVN-VHIHE--LRQYYNDIRLDVIESDTDIGVKLLD-SLMTNIQD--TCCFVRLVLRFYIERVFSSQPQHQRCSSALANAFF-SIRRDIRE------EEDTQRKIDSVIAEFNKLDNQAAKKAVGELDTVLEWLQGL--
-RCPLF-VDMHH--LEESFQEIKRAVQTKDTLQIITTLSTVILRSIKPTDVCCVTKNLLEFYMDRVFKLKPQILRKISSIANSFL-YMQKTLRQCVHRR-SEEATHATRTVHDNYDLLEPSAAIKSLGELDVFLAWIDRN--
--------------LRAAFGKVKTFFQMKDQ-LHSLLLTQSLLDDFKGYLGCQALSEMIQFYLEEVMPQHGPDIKEHVNSLGEKLKTLRLRLRRCHRFLPCENKSKAVEKVKRVFSELQERGVYKAMSEFDIFINYIETY--
--------------LRAAFSRVKTFFQMKDQ-LDNMLLDGSLLEDFKGYLGCQALSEMIQFYLEEVMPQHSTQ-KDKVNSLGEKLKTLRVRLRRCHRFLPCENKSKAVEQVKSAFSKLQEKGVYKAMSEFDIFINYIEAY--
--------------LRDAFSRVKTFFQMKDQ-LDNILLKESLLEDFKGYLGCQALSEMIQFYLEEVMPQMSLKSQEHVNFLGENLNTLRLRLRRCHRFLPCENKSKAVEQVKNAFSKLQEKGVYKAMSEFDIFINYIEAYM-
--------------LRSEFSKIKSFVQDNDQ-ENMMLLSQSMLDKLTSRIGCKSLSDMIKFYLNDVLPNIE-HMKNKITSIGEKLKSLKEKLISCD-FLHCENHD--IKTVKTIFNKLKDKGIYKAMGEFDIFINYLEKYI-
--------------LRDAFSRVKTFFQTKDE-VDNLLLKESLLEDFKGYLGCQALSEMIQFYLEEVMPQQDPEAKDHVNSLGENLKTLRLRLRRCHRFLPCENKSKAVEQIKNAFNKLQEKGIYKAMSEFDIFINYIEAYM-
--------------LRAAFDQVKTFFQKKD-QLDSILLTDSLMKDFKGYLGCQALSEMIQFYLVEVMPQHGPEIKEHLNFLGEKLKTLRRRLQRCHRFLPCENKSKAVEQVKSDFNKLQENGVYKAMSEFDIF---------
--------------LRTAFSQVKTFFQKKD-QLDNILLTDSLLQDFKGYLGCQTLSEMIQFYLVEVMPQHGPEIKEHLNSLGEKLKTLRRQLQRCHRFLPCENKSKAVEQVKDNFNKLQEKGVFKAMNEFDI----------
-PCRVEGVVFQE--LWEASRAVKDIVQAQDNIMSVRLLRREVLQNVSGAESCYLLRALLNFYLNTVFRNELRILKSFSTLANNFI-VILSKLQPSQENEMRESARRRFLLFQAAFKQLDEAAQTKAFGEVDILLTWMQKF--
-DCLVQGISLPE--LWKPVSAVSRSLQAQDQVMDVKLLKNELLLDVKLDDSCCLILSLVRFYLDRVFRAELGVQRKVTPVANSFF-VVQMKLSQC----------------------LDKAGVIKALGELNTLFDWMNQV--
--------------MRSIFQDIKPYFQGKD-SLNNLLLSGQLLEDLQSPIGCDALSEMIQFYLEEVMPQHHPKHKNSVMQLGETLHTLISQLQECTALFPCKHKSLGAQKIKEEVSKLGQYGIIKAVAEFDIFINYMESY--
-SCVIT-AN-QG--IRNG-SEIRDSVQARDEIIDIRILKKTSLQDTKPADQCCLLRHILRLYLDRVFTPDHHVLRKISRLANSFL-TIKKDLRLCHARMTGEEAMEKYGQIMSHFEELQQAAVVKALGELDILLQWMEET--
---------LKE--LRMKFEEIKDYFQSQDDELSIQLLSSDLLEEFKGNFGCQSVLEMMRFYMEEVLPSSSEHHQQSVGDLGNLLLSLKAMMRRCHRFLTCEKRSKTIKQIKETFEKLDQTGATSSILS-------------
---------LRE--LRMAFDRVKTFFQ-KKDQLDNMLLNKSLLEDFKGYLGCQALSEMIKFYLEMVMPQHGPDIKEHVNSLGEKLTTLRTRLRRCHRFLPCENKSKAVEQVKEAFSKLQEKGVYKAMNE-------------
--------------IEESFREIKGAIQAKDIFQNVTILSTLILRSVKPLDVCCMTTNLLAFYVDRVFKDNPQILRKISSIANSFL-YMQKALQQCQEQRLGQEATNATRIIHNNYNQLEQSAALKSLGELDVFLAWVHK---
--------------IQSGFSKIRDTVQAKDENIDIRILRRTSLQDTKPSARCCLLRHLLRLYLDRVFKNDHNTLRNISSLANSFL-TIKKGLRLCHARMTGEEATEKYSQILSHFE---KEAVVKALGELDILLRWME----
-PCRVEGVVLPE--LWEAFWAVKNTVD---------------------AESCYLVHSLLKFYLNTVFKNKVKILKSFSSLANNFI-DIVSKLQPSKDKDMSESAHRRFMLFRRAFKQMDEVALVKAFGEVDILLTWMQKF--
-SCVIT-ANLQA--IQKEFSEIRHSVL---------------------SDRCCFLRHLVRFYLDRVFTPDHHTLRKISSLANSFL-IIKKDLSVCHSHMAGEEAMEKYNQILSHFTEFTALGAIPGPRTMRILLRWMKQM--
-SCVIT-ANLQA--IQKEFSEILDTVH---------------------SDRCCFLRHLVRFYLDRVFTPDHHILRKTSSLANSFL-IIKKDLSICHSRMVGEEAMKKYNHVLSHFTEVTFVGIVVGV--------FLELR--
--------------LRSAYREIQKFYESNDDL--APLLDENVQQHINSPYGCHVMNEILRFYLDTILPTDHLHSKTPINSIGNIFKDLKRDILKCKNYFSCQNPFE-FASIKNTYEEMNGKGVIKAMGELDM----------
--------------LRSAYREIQRFYESNDDL--EPLLNENVQQNINSPYGCNVMNEILHFYLDTILPTNHLHSTTPIDSIGNIFQDLKRDMRKCRNYFSCQNPLE-IASIKNSYEKMKEKGVSKAMGELDIL---------
--------------LRSAYREIQNFYESNDDM--EPLLDENVQQNINSPYGCHVMNEILRFYLDTILPTDHLHSKTPINSIGNIFQDLKRDILKCRNYFFCQNPFE-FASIKNSYEKMKEKGVYKAMGELDIL---------
-RCLIS-MDLHH--IEEAFQEIKKTIQAKDPFQNVTILSTSTLHRIKPLDVCCMTKNLLAFYVDKVFKLNPQILRKISSIANSFL-YMQKALRQCQEQRQRDEATNATRIIHSNYDQLEPSAAIKSLGELDVFLAWIDK---
-RCLIS-TDMHH--IEESFQEIKRAIQAKDTFPNVTILSTLTLQIIKPLDVCCVTKNLLAFYVDRVFKPNPKILRKISSIANSFL-YMQKTLRQCQEQRQRQEATNATRVIHDNYDQLEHAAAIKSLGELDVFLAWINK---
-RCVIS-TDIHH--IEKSFQGIKNEIQAKDVFQNVTILSASTLYNITTSDVCCMTRNLLRFYVDRSFRSDPKILRGLSTMANSFL-KIMNALEQCQNKTSREEVTKKSQLIVRNYEQMEKAAAIKSLGELDIFLSWINR---
-RCLIS-IDMHR--IEESFRGIKKAIQAKDTMQNVTILSPSSLHSIKPLDACCMTKNLLAFYVDRVFKLDPQLLRKISSIANSFL-YMQKTLQQCHEQRLRQEATNATRIIHDNYDQLERSAAIKSLGELDIFLAWIDK---
-RCLIS-MDIHH--MEKSFQGIKTAVQAKDSFQNITILSASTLYNITSSDVCCMTKDVLQFYVDKVFRSDPRIQRGLSTIANSFL-HIRNALEQCRSQKNRKETTSKFQLIVRNYEQMEKAAAIKSLGELDILLSWISR---
-RCLIS-VDRSL--IEKSFPEIKRALQTNDTFPNVTILSTLNLKSIQPVDVCCVTSRLLTFYRDRVFKTSPEVMRQISSIANSFL-YMQKTLEQCQVHNQSQEATNATRTIHDNYDQLESSAALKSLGELDIFLAWIDE---
-RCLVS-VDMRL--LEKSFQEIRRTLQTKDTFKNVTILSTLNLRSIKPVDVCCVTNNLLTFYRDRVFKTSLEVVRRISRIANSFL-HMQKTLEQCQVHRQSQEATNATRTIHDNYNQLESSAALKSLGELNIFLAWIDR---
-KCLIS-MDTHH--LEESFQEIKRAIQAKDTFQNVTILSASTLQSIKPSDVCCVTKNLLAFYVDRVFKLNPHILRKISSIANSFL-YMQKTLQRCQEQRRRQEATNATRSVHENYDQLEHSAAVKSLGELNVFLAWIAK---
-RCLIS-VDMRL--IEKSFHEIKRAMQTKDTFKNVTILS-LNLRSIKPGDVCCMTNNLLTFYRDRVFQRSLEVLRRISSIANSFL-CVQKSLERCQVHRQSQEATNATRIIHDNYNQLESSAALKSLGELNILLAWIDR---
-RCPPS-MDMNP--VKESFQEIKGAIQANDTFKNVTILS--NLHSIKPVDVCCMTRELLEFYMKRVFKLSLQISREVSSISNSFL-PLQRTVQPCKKQSLSEEAISATRTIINNYDQLERSAAIKSLGELDVFIAWIDK---
-RCLIS-MNLHR--VEESFRGIKTAIQAKDTFQNVTILSPSTLHGIKPLDVCCVTKNLLAFYVDRVFKLNPQIMRKISSLANSFL-YMQKTLQQC--QNLRQEATNATRIIHDNYDQLERSAAVKSLGELDVFLAWIDK---
--------------LRAAFGKVKTFFQTKDELHS-ILLTRSLLEDFKGYLGCHALSEMIQFYLEEVMPQEDPD-KQHVNSLGEKLKTLRLRLRRCHRFLPCENKSK------------------------------------
-PCQVKGVVPQK--LWEAFWAVKDTLQAQDNITSVQLLQQEVLQNVSQE--------------------------------NEMF-TI------------RDSAHRRFLLFQRAFKQLDEAALTKALGEVDILLTWMQKF--
--------------LRTSFSTIRDYYEANDE-LETSLLDEGILHHLKSPVGCHAMDSILKFYLDTVLPTQNNHFKSPIDSIGNIFHELKKEIVLCRNYFSCKKPF-DINEFISSYKKMQGKG--------------------
--------------LRAAFSMVKTFFQMND-QLDSMLLNGSLLEDFKGYLGCQALSEMIQFYLEEVMPQQGPGIKEHVNSLGEKLKTLRLKLRRCVSSS-------------------------------------------
----------------QDVRQVSEHYQIQADDTSMRTIPK-VDTNDHHLEICCLHANILDYYLTNILHHTN-NDHAHVHRLKTNLHRISTDLQACNVTQYHD--HKNAVDFRTKLEKMEKKGITKAIGELDILFSYLQDYCV
----------------QAVREVSQHAQLTEDDSSTRLFPR-VSGDQDHMKICCLHANILDFYLNNILIHSA-DTHPKMLRLKTDLSRVSADLQTCSVTHYHD--HLHAVEFRRRLNRMEKRGLNKAVGEIDILFTYLQDYCV
----------------VAVDEVAKQAQREQDDPSVRLLPNIPNQETRDLEICCIHANILDFYLNNVLPHHS-SNNAHAHRLQTDLSRISQDLETCSINRYRD--HQHAEEFSRRFFALDRHRLNKALGEIDILFSYLQDYCI
----------------KAVEEVANQAQLEQDDPSVRLMPH-PPAGPDNLEICCLHANILDFYLLNVLSHHS-TDNQHTRRLRSDLSRISHDLEACNITRYHD--HQHAVKFRQRYFE-HQHRRTKALGEVDILFYYLQDYCV
----------------TMVQQVAQHAQ-SDTDTDTKLMPD-IDTKKNHRDICCLHANILDFYLSNILT-QD-KHHPKLPALKEDLARVSRDLKECAIKHYND--HHHSIAFHKKLSEMEGKGIKKAIGEIDILFTFLKDFCV
----------------QAAHDISKKAQPETDETSLRLISR-VSPNDQNAEICCLHANILDFYLLNVLQSSD-SFHPTMPRLKTDLRRISQDLSHCNVTHYQD--HQNAVEFREKLITMQQRGITKAIGEIDILFSYLQDFCV
---------------------------------------------FKTPFACHAINSILDFYLATVLPGDTKSMKPHMESIQQIFDQLKNDVTACRHYFHCKNQF-DITNLNSTYTQMQSKGLFKAVGELE-----------
--------------------------IPEDRIKNIQLLKKKTKKL--FMKNCRFQEQLLSFFMEDVFGRLQLQACKETDFVEDFH-SLRQKLSRCVSMHSKYGEFNTLSRKQHEFGKFLELYLYKAKSSQDIFFLWSKKL--
--------------------------IPEDRIKNIRLLKKETKKL--FMKNCRFQEQLLSFFMEDVFGGLQLQICKEIGFVEDFH-SLRQKLSRCISCALPAREVTSITRMKRTFYGIGNKGIYKAISELDILLSWIKEL--
--------------------------IPEDRIKNIRLLKKKTKKQ--FMKNCQFQEQLLSFFMEDVFGQLQLQGCKKIRFVEDFH-SLRQKLSHCISCASSAREMKSITRMKRIFYRIGNKGIYKAISELDILLSWIKKL--
--------------------------IPEDRIKNIRLLKKKAKRL--FMKNCRFQEQLLSFFMEDVFGQLQLQVCKEIHFVEDFY-SLQQKLSCWISCASSAREMKSITRMKRIFSKIGNKGIYKAISELDILLSWIKKF--
--------------------------VPEDGIKNKKLLKKKTKRL--ITKNCRFRDQLLTFYLEDVFGSHHLPVGGELRIVEDFQ-RLKEVLNRCVPCAPSSREMKPITKMKELFYKLGNKGVYKAIGELDILLPWIKSY--
--------------------------IPEDRIKNIQLLKKKTKKL--FMKNCRFQEQLLSFFMEDVFGRLQLQACKETDFVEDFH-SLRQKLSRCISCASSPREMKSITRMKRIFYGIGNKGIYKAISELDILLSWIKQF--
--------------------------IPEDHIKNILLLKKKTKKL--FMKNCRFREQLLSFFMEDVFGQLQVQVCREIHFVEELH-SLRQQMNRCISCASSAREMKAITRMKRTFYGIGNKGIYKAIRELDILLCWIKQF--
--------------------------VPEDSIKNVRLLKKNTKEL--FLTNCRFQEQLLSFFMDDVFGQLQFQAFNETDFVEDFH-CFRQKLSRCITCASSAREMKSITRMKRTFYGIGNKGIYKAIGELDILLSWIKKF--
--------------------------IPEDSIKNIRLLKKKSKKL--FMKNCRFQEQLLSFFMEDVFGQLQVR--KEIQFVEEFH-SLRQKLSRCISCASSAREMKSITRMKRTFYGIGNRGIYKAINELDILLSWIKQF--
--------------------------IPEDRIKNIRLLRKKTKNL--FMKNCRFQEQLLSFFMEDVFGQLQLKICREIRFVEELH-SLRQQLSRCISCASSAREMKTITRMKSTFYGLGNKGIYKAISELNILLSWIKQF--
--------------------------IPKDLIKNRRLLKKATKKL--FMKNCSVRDQLLSFYVKNVFGGLR-SGSDRVYMVSAFQ-TLQENLSNCLPCAPSSRVTMAVKKIKQMFDKLGEKGIYKAISELDILLPWIQTY--
--------------------------------LGSILLTGSLLEDFKGYLGCQALSEMIQFYLEEVMPQHDPEVKEHVNSLGEKLKTLRLRLR-------------------------------------------------
-----------PPVQSNRF----EVLQAEDEIMDLRILKTESLRNTKPADQCCLLRLYLRLYLGTVFKLDQHILRNVSSLANSFL-TIKKDLRLCHARMTGEEAMEKFNQILSRFKEQE-----------------------
--------IIKE--VRTGYGSIKHALQSKDTVYYVSLFHENLLNEMLSPVGCRVTNELMQHYLDGVLPRAFQTL-DGLHSLVSSLDALYKHMLKCPA-LACTGQTPAWTQFLETEHKLDWKGTIKATAEMDLLVNYLETF--
--------IFKE--LRATYASIREGLQKKDTVYYTSLFNERVLHEMLSPMGCRVTNELMEHYLDGVLPRASHTL-NGLHVFASSMQALYQHMLKCPA-LACTGKTPAWMYFLEVEHKLNWRGTAKAAAEADLLLNYLETF--
--------IFKE--LRTTYRSVREALQTKDTVYYVSLFHEQLLQEMLSPVGCRVTNELMQHYLDGVLPRAFHTL-NALHALSSSLSTLYQHMLKCPA-LACTGQTPAWTQFLDTEHKLDWKGTVKATAEMDLLLNYLETF--
--------MLKS--MRQDYSRIRDTLHDRDKLHS-SLLTGALLDEMMGYSGCRTTLLLMEHYLDTWYPAAY-TL-VVVDRMGSTLVALLKAMVQCPM-LACGAPSPAMDKMLQQEAKMKYTGVYKGISETDLLLGYLELY--
--------MLQS--LRHDYSKVKTVLHDHDTVHT-SLLTGDLLDEMLGPRGCQTTVQLMEKYLSEWLPKAD-TI-RTLDRLGGTLNGLMKDMVQCPM-LICGAPSSAMMGLQSRENKLPNAGVYKGLAEIDLLGNYLELY--
--CGIEHNELNN--IKNIFFKVRNVVQADDVDHNLRILTPALLNNITVSETCFFIYDMFELYLNDVFVKKLNILKSLSSVANNFL-AIFNKVKKRRV------KKNNVNVL--EIKKLLDNNCKKLFSEIDIFLTWV-----
--CLIA-MDMRH--LGESFQHIKGTIQAKDTFPNVTILSTSTLHSIKPLDVCCVTKNLLAFYVDRVFKLSPQILRKISSIANSFL-YMQKVLQQCVSP--------------------------------------------
----------------------------KDLIKTSRLLKKTTK--MLFMTNCSVRDQLLSFYVKNVFSRGSDKLY--FIS---AFQVLQANMDACLPCSPSTRLTSAVKKLKRMFLKLGDQGFYKAIHELDILLPWIQAY--
----------------------------KDLIKNTRLLKKTTK--MLFMTNCNVRDQLLSFYMKNVFSHESEKLY--VIS---AFRVLQENMNACLPCAPSTRLTSAVKNIKKTFLKLGEKGVYKAINELDILLPWIQAY--
--------------LRSAYREIQSFYESNDD-LE-PLLTESMQQNINSPYGCHVMDEILRFYLETILPTNHFQSKTPIDSIGSIFQNLKR----------------------------------------------------
--------------LRTAFEKVREFYEDSDEEET-ALAST---EHLHGPESCSVIDELITHYTKCVIPAEGADLLS-LDTLQVALENVKGLLANCQEEFGCKPPF-SMRDYKKQYRQLNNAGMIKAMGELGMLFNGIEE---
-------------ATWNDLAAMTDTARNEDDH-ETRLLPYFSHDMLQEEGSCCINARILKYYVNHVLESHTDMKYPMIRNVREGLHRVEQELQNCKHDY-SS--HPLVKQFKRNYHAMDAAARNKAIGETNTLYHYLFESCT
--------RLQD--LRVTFHRVKPTLQREDDYS--VWLDGTV---VKGCWGCSVMDWLLRRYLEIVFPAGYPGLKTELHSMRSTLESIYKDMRQCPLLGCGDKSV--ISRLSQEAERKSDNGTRKGLSELDTLFSRLEEY--
--------------LYNKTARFKE-LFPKDNITDIQFLTEELKQDFMAQKNCNLRNNFLSFYIKTFLPPVQEKA-KKLKIIQDLM-VIQDMLLHCKK-SHCDHKETGITELKIKIRQINNKVLWKAISEMDTLLEWIYEY--
---------LRR--LREDYVQIRDFYEANDD-LDTVLLDQTIEDSFKTPFACNAMNTILEFYLGTVLPA-------------------------------------------------------------------------
