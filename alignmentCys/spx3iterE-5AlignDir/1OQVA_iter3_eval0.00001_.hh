DSQNMTKAAQSLNSIQVALTQTYRGLGNYPATADATAASKLTSGLVSLGKISSDEAKNPFIGTNMNIFSFPRNAAANKAFAISVDGLTQAQCKTLITSVGDMFPYIAIKAGGAVALADLGDFENSAAAAETGVGVIKSIAPASKNLDLTNITHVEKLCKGTAPFGVAFGNS
DSQNMTKAAQNVNNVQVAMTQTYRGLGTYPASGTTALGTQLSNGLVNLGKLSADDVKNPFSGSVMPVQSFARNGAANKAFAIVVDGLTGAQCKQLVTSVGDMFPYIAVKAAGGFATA-IDDFETVQANAADAAGVKKSIAAGSVNLNLADITHVENLCHGNS-FAVALGNS
DSQNMTKAAQSLNSIQVAMTQTYRALGNYPATADAAAAAKLTTGLISLGKISADEAKNPFTGGNMNIFSFSRNAAQNKAFAITVDGLTQAQCKTLITSVGDMFPYVAIKAGAAIAFGDLTDFETTQADAAAGAGVIKSIAPASKNLDLTNIIHVEELCKGAAPFGVAFGNS
DSQNMTKAAQNLNTIQVSLTQTYRSLGNYPATADAAAATALTSGLVSLGKISADEAKNPFTGNNLNIWSFPRNAAGNKAFAISVDGLTQAQCKTLVTSVGDMFPYINVQEKAAVALADLGDFETGPANAVTGKGIIKSIATGSVNLNLTEITHVQNLCAGTGTFSVAFGNS
DSQNMTKAAQNLNTIQVAMTQTYRSLGSYPTTADANAAGRLTSGLVSLGKISADEAKNPFTGTNMNIFAFSRNTAPQKAFAIAVDGLTKAQCKSLVTSVGEMFPYIVVKEGAAVENADLKDFETEAPVGNTGKGVIKSIATG-VNLNLTDVTHVENLCSGTGAFSVAFGNS
DSQNMTKAAQNLNTVQVSMTQTYRALGNYPATANIAAATKLTSGLVSLGKISSDEAKNPFTGTNMNIFAFPRNGAPNKAFAIAVDGLTQAQCKTLITSVGDMFPYVLIKSAGTIDFADLTDFETTQAKAADGVGVIKSIAPGGTNLKLTEIAHVEALCTGTAPFGVAFGNS
DSQNMTKAAQNLNSVQVALTQTYRGLGQYPATVDTAAATKLTAGLVSLGKVSSDEAKNPFTGANMNIFSFNRNSAPNKAFAIAVDGLTQAQCKTLVTSVGDMFPYVVVKQAAAIAFADLQDFETTAAVADTGVGVIKSISPGGNNLNLTNITHVEKLCDGTGTFSVAFGNS
DNQNVSKLSQALNVIQTSMVQTYRSQKSYPPIATVADSAKLTKALISMGKISTTDIVNPFTGAQLEVYTTADNKAANRGFAIKAGNLSQNQCRAIISSSNELFAFIQVETVGGTLAADM-----PQANEPT--GVIKSTKGGVNQFDISNLDQLTALCGGDA-YDLFVGN-
DNQNVSKLSQALNTIQTAMVQTYRSKQSYPDVQDAVKAKKLTDALVSMGRVTESDLINPFTGAPMLIFTAKDNKAANRGFAIKVSDLSKDQCTSLISNSADLFSFIEVQNRGTAMAADF-----PDATKSV--GVIKSTKGGAKTLDLTNLDHISALCGGPG-FDVFVGN-
DSQNMTKAAQNLNSVQVALIQTYRGLGNYPETTDDTAAAKLTAGLVSLGKISPDEAKNPFTGTDMNIFSFNRNGAANKAFAISVDGLTQAQCKTLVTSVGDMFPYVLVKNAAKVAKADLSDFETTAAVADTGVGVIKSISPTGKNLNLTDITHVEKLCSGATTFSVAFGNS
DSQNMTKAAQSLNSIQVALTQTYRGLGKYPTAAAAGDAAKLTAGLVSLGKISSDEAKNPFTGSDMNIFSFPRNSAADKAFAISVEGLTQAQCKTLITSVGDMFPYIAVKGATSLAVADLGDFETSPAKAADGVGVIKSIAPAGVNLNLTTITHVEKLCTGDAPFIVAFGNS
DSQNMTKAAQNLNSVQIAMTQTYRSLGNYPATANANAATQLANGLVSLGKVSADEAKNPFTGTAMGIFSFPRNSAANKAFAITVGGLTQAQCKTLVTSVGDMFPFINVKEGAFAAVADLGDFETSVADAATGAGVIKSIAPGSANLNLTNITHVEKLCTGTAPFTVAFGNS
DSRTVSEVVQSTNTIRVAIKDAYASTGAYPTYVSPLAYTNATSTLVKLGKVSTNEMRNKISGDFYAIGAGSQNQTAAKGYVIEVNGLTSEQCRSILTQVGNNWDFVSIGNSAAGAY-TLTNGAVDMTAQVNGTSILRSLNSNT---TITPNTIVST-CTGAS-NSIILG--
DSRAVTELVTNTNTVRVAMKDAYQRDGRYPAYVSPLTLTGSAAQLTQLGKITPDEVRNNISGDFIGIGGAVAGANALKGFAMELNGLSQEQCRSILGQVGNNWEYVAVGTSGSGNY-SLTAGGVDMTAASDGTAILRSLGADGQ-ESLNSAKLLAT-CTATV-NSITLA--
TAQDLSAVQDDLTSVRTAMNEAYKDQAEYPATDSILTLSKPIATLVRLGKISADEAFNGFSNDAFQIANADSSSSQKKGFVVMVNGLASEECRNLISQMGNQWDYVEATNAAAGS--EPTITGKDFSVGKSGA-ILKTLTSG----EIAPADIVADGC-GSV-NGVIFG--
DSRTVSELVTNTNTIRVAMKDAYQRDGKYPDYQAPLSLTAAVAQLVQLGKLTPDEARNGISGDYIGIGGASSGSTINKGFAMELNGLSQEQCRSILGQVGDNWEYVAVGTSPSGSYDALSAGAVNMLAATDNTTILRSLAANGQ-VSLTAEKILKT-CTATV-NSITLA--
DSRAVTELVTNTNTVRVAMKDAYQRDGVYPAYESPLTLTGPAAQLVQLGKITSDEVRNNISGDFFGIGGASAGSKVLKGFALELNGLSQEQCRSILGQVGNNWEYVAVGTTPSGSY-TLTTAGVDMTAAADGTAILRSLAANGQ-ESLTASKILNT-CTATV-NSITLA--
TAQDLSDVQDNLTSVRTAMTEAYKDQADYPTTDSVIGVTKPIVTLVRMGKIAADEAFNGFSNDAFQIDQADSTSNQYKGFVVLVNGLAADECRNLISQMGNQWDYVEATGATAGK--QITLTGRSLDVAASGA-ILKTLVSG----DVAPDQIVATGC-GSV-NGVVFG--
DSRAVTDLVTNTNTVRVAMKDAYQRDGKYPDFVDPLSLTAPAAQLVQLGKITPDEVRNNISGDFIAIGGASNGAQVKKGFAIELNGLSQEQCRSILGQVGNNWEYVAIGTSASGSY-AMTATGVDMSVAASTT-VLRSLGNNGQ-TTLTADKILST-CTAQV-NSITLG--
DSRTVTELVSNTNTVRVAMKDAYQRDGVYPQYVDPLSLTAPIAQLVQLGKLTADEGRNNISGDFMGIGGAS-AAAVLKGFAIELNGLSQEQCRSILGQVGNNWEYVKVGTSASGSY-SLTTDGVDMTVEANGD-ILRSLGTNGQ-ETLTAEKILGT-CNATI-NSIILG--
TAQDISELADNTNSVRVGTTEAYKDSAEYPATASAASLTKIISTLVKMGKISVGEAFNGISNDPFELGGVATADTKMKGFYLVINGLDTEDCRNIASQIGGQWDYVASAQVAAGG--DLASAPTSLSGTATAP-ILKTLTED----KLTPETIVGTGC-GSD-NALVLG--
DSRAITELVTNTNTVRVAMKDAFQRDGVYPAYASPLSLTGAAAQLVKLGKITADEVRNNISGDFIGIGGATSGSKVLKGFAMELNGLSQEQCRSILGQVGNNWEYVGVGSSGSGTY-SLNAAGVDMTQAADGSTILRSLGQDSQ---------------------------
DSRAVTELVTNTNTVRVAMKDAYQRDGAYPDYLSPLSLTAPAAQLVQLGKISPDEVRNNISGDFIGIGGASAAATVNKGFVIELNGLSQEQCRTILGQVGNNWEYVAVGNSASGAY-SLTNTALDMTTAANGTSILRSLGANSQ-DTLTAEKILAT-CTATV-NSITLG--
DSRAISDLVSNTNTVRIAVKDAYQREGNYPAAKDSTASTTIIARLVQLGKISVDEARNGISNDYLNIGGAITGATTDKGFVIEINGLDQSQCRSVISQVGNNWDYVATVTAPASYTVNGSNFTTAAVAAPTSQAVLRSLDTTTGSQTIT-AAMTAAVCSDDSSNGVILG--
DSRTLSEVVQTTNTIRVAMKDAYSNSGSYP-DLTNKQAGA-TATLVQLGKLSVSEARNKISGDFYAIGAASTGDTNKKGFAMEINGLTSEQCRNILTQVGNNWDYVAVGTSAATYSLSPST-AVDMAAASSTGPVLRSL-ANNGTAAIT-PDVTVNVCSNGASNSIILG--
DSRAVTDLVTNTNTVRVAMKDAYQRDGVYPDYVSPLKLKKPAAQLVQLGKITPDEVRNNISGDFIAIGALTSNGATKKGFAIELNGLSQEQCRSILGQVGNNWEYVAVGNSASG---------------------------------------------------------
