GSVVGGLGGYMLGSAMSRPIIHFGSDYEDRYYRENMHRYPNQVYYRPCDEYSNQNNFVHDCVNITIKQHTVTTTTKGENFTETDVKMMERVVEQMCITQYERCSQAYYQRGS
GAVVGGLGGYMLGSAMSRPMMHFGNDWEDRYYRENMNRYPNQVYYRPVDQYNNQNNFVHDCVNITIKQHTVTTTTKGENFTETDIKIMERVVEQMCTTQYQKESQAYYD---
GAVVGVLGGYMLGSAMSRPLIRFGNDYEDRYYRENMYRYPNQVYYRPVDQYSNQNNFVHDCVNITVKLHTVTTTTKGENFTETDIKIMERVVEQMCITQYQRESQA------
GAVVGGLGGYMLGSAMSRPLIHFGNDYEDRYYRENMYRYPNQVYYRPVNEYSNENNFVHDCVNITVKQHTTTTITKGENFTETDTKIMERMVEQMCVTQYRKEFQA-YQRGS
GAVVGGLGGYLVGSAMSRPPIHFGNDYEDRYYRENINRYPNQVYYKPVDQYSNQNNFVHDCVNITVKQHTVTTTTKGENFTETDVKIMERVVEQMCITQYQ-QARA-YHDGA
GAVVGGLGGYLVGSAMSRPLIHFGNDYEDRYYRENMYRYPNQVYYRPVEQYSSEKNFVHDCVNITIKEHTVTTTTKGENFTETDVRMMERVVEQMCITQYQREAQAAYQRGA
GAVVGGLGGYMLGSAMSRPIIHFGNEYEDRYYRDNMYRYPNQVYYKPVDQYSNQNNFVHDCVNITIKQHTVTTTTKGENFTETDIKMMERVVEQMCVTQYQQEYQASYQRAA
GAVVGGLGGYMLGSAMSRPIMHFGNDYEDRYYRENQYRYPNQVMYRPIDQYNNQNNFVHDCVNITVKQHTTTTTTKGENFTETDIKIMERVVEQMCITQYQNEYRSAYS---
GAVVGGLGGYMLGSAMSRPLIHFGSDYEDRYYRENMYRYPNQVYYKPVDQYSNQNSFVQDCVNITIKQHTVTTTTKGENFTETDVKIMERVLEQMCTTQYQKESQAYYYHGA
GAVVGGLGGYMIGSAMSRPPMHFGNEFEDRYYRENQNRYPNQVYYRPVDQYGSQDGFVRDCVNITVTQHTVT-TTEGKNLNETDVKIMTRVLEQMCVNLY------------
GAVVGGLGGYALGSAMSGMRMNFDRPEERQWWNENSNRYPNQVYYKEYDRSVPEGRFVRDCLNNTVTEYKIDPNENQNVT-QVEVRVMKQVIQEMCMQQYQQ----------
GAVVGGLGGYAMGRVMSGMQYRFDSPDEYRWWSENAARYPNQVYYRDYGGAVPQDVFVADCFNITVTEHNIGPAAKKNAS-ELETRVVTKVIREMCIQQYQE----------
GAVVGGLGGYAMGRVMSGMSYRFDSPDEYRWWNENSARYPNRVYYRDYSSPVSQDVFVADCFNITVTEYSIGPAAKKNTS-EMENKVVTKVIREMCVQQYRE----------
GAVVGGIGGIALGSAMSGMRMNFDRPDESRWWNENQNRYPNQVYYREYDRSVPRGTFVNDCVNITVTEYKIDPKENQNVT-EIEVKVLKRVIQEMCIQQYQK----------
GAAVGALGGYLLGRSMSNMQFGFPNQYDERWWYQNRDRYSDQVYHPPYNPSVSREVFVRDCVNVTVTEY-IQPTGNQTAD-EVEMRVVPLVVREMCTEQY------------
---VGALGGFFLGRAMN-MHFNFRNPAEEQWWYENRNRYSDHVYYPKYEQPVPADVFVRDCWNITVREF-IEPSGNETAD-EMESRVVAQVVHEMCVQQYRS----------
-----------------------------------------------------------LCRTVPIKQHTVTTITKGENFIKTNLKIMEFVLQQTCTTQYQKESQGYYY---
GAAAGAIGGYMLGNAMGRMSYHFSNPMEARYYNDYYNQMPERVY-RPMEEHVSEDRFVTDCYNMSVTEYIIK-PAEGKNTSEVETRVKSQIIREMCITEYRRGSG-------
GAAAGAIGGYMLGNAVGRMSYQFNNPMESRYYNDYYNQMPNRVY-RPMEEYVSEDRFVRDCYNMSVTEYIIK-PTEGKNNSELDTTVKSQIIREMCITEYRRGSG-------
GAVVGGLGGYVMGRAMSGMHYHFDSPDEYRWWNENAGRYPNRVYYRD-----------------------------------------------------------------
GAVAGMAVGYGLGRF-PRPHFGFRSPSEEYYYNNYMYRYGTYVY-KPPPRAESYENFMDRCMNRTDLLKDKGGKTDGDDDTVGEEIGYPALVDQMCVEEYMAYS--------
GALAGMAVGYGLGRF-PRPHFNFRNPEEESYYNNYMYRYGKYVY-KIPPRAESYEKFMARCMNETDNGQ---GVDT-EDDTVSQEIGYPALIEQMCVEKYMIYS--------
-------------------DIDFGTE-ANRYYEANYWQFPDEIHYNGCEANVTRERFVSGCINATQAANQEELS-QERPDNRLHRRILWRLVRELCSVKR------------
-------------------DVDLGAD-GNRYYEAHYWQFPDGIHYDGCDANVTREMLVTRCINATQAANQAEFSARDQQDSRLHQRVLWRLIRELCSAKR------------
-------------------HIDFGAE-GNKYYEANYWQFPDGIYYDGCEANVTKEVFIAKCINATQAANQAEFS-REKQDNKLHQRVLWRLIKELCSVKH------------
-------------------DIDFGAE-GNRYYEANYWQFPDGIHYDGCEGNVTKEMLVTGCINATQAANQAEFS-PEKQDNKLYQQVLWQLIKELCSIKH------------
-------------------DIDFGDE-GNRYYEAHYWQFPDGIHYDACKANVTKEKFVTSCINATQAANQEELS-QEKQDKLLYQRILWQLIRELCSIKH------------
-------------------SINFGEE-GNSYYEAHYKLFPDEIHYVGCESSVTKDVFISNCVNVTHTANKLEPP-EERNSSAIYSRVLEQLIKELCALKY------------
-------------------VIDFGEE-GNSYYATHYSLFPDEIHYAGCESNVTKEVFISNCVNATRVINKLEPL-EEQNISDIYSRILEQLIKELCALNY------------
-------------------DIDFGAE-GNKYYAANYWQFPDGIYYEGCEANVTKEVLVTRCVNATQAANQAEFS-REKQDSKLHQRVLWRLIKEICSTKH------------
-------------------NIDFGPE-GNKYYEANYWQFPDGVHYDGCEGNVTKEAFVAKCVNATQAANQGEFS-REKQDNKLHQRVLWRLIKELCSIKH------------
-------------------DIDLGAE-GNRYYEANYWQFPDGIHYNGCEANVTKEKFVSGCINATRVANQEELS-REKQDNRLYQRVLWRLVRELCSVKH------------
-------------------DIDFGVE-GNRYYEANYWQFPDGIHYNGCKANVTKEKFITSCINATQAANQEELS-REKQDNKLYQQVLWQLIRELCSTKQ------------
-------------------NINFGAE-GNRYYEANYWQFPDEIHYNGCEANVTKEKFVISCINATQEANQEELS-QEKQDDKLYQRILWRLISELCSVKH------------
-------------------DIDLGAE-ANRYYEANYWQFPDGIHYNGCEVNVTKAKFIASCINATHSANQEELR---EKHDKLYQRVLWRLVRELCSLKR------------
-------------------NIDFGAE-GNRYYETNYWQFPDGIHYNGCDTNVTKEVLVTSCINATQAANQAEFS-REKQDNKLHQRILWRLIKELCSAKH------------
-------------------DIDFGAE-GNRYYEANYWQFPDGIHYNGCEANVTKEVFVTGCINATQAANQGEFQ---KPDNKLHQQVLWRLVRELCSLKH------------
-------------------DIDFGAE-GNRYYEANYWQFPDGIHYDGCEANVTKEMFVTSCINTTQAANQEEFR---KQDNKVYQRILWRLIRELCSVKH------------
-------------------DIDFGAE-GNKYYEANYWQFPDGIHYDGCEANVTKEVFVTGCINATQAANQAEFR---EKHDKLHQRVLWRLIRELCSVKH------------
GALAGMAVGYGLGRF-PRPHFTFRNPEEEYYYNNYMYRYGDYVY-KPPPRAKTYENYMNECMDRTD----------------------------------------------
GAVAGMAIGYGLGRF-PRPHFNFRNPEEEQYYNNFMYRYGDYVY-KPPPRAESYENFMGRCMNKTD----------------------------------------------
GAVAGMAVGYGLGRF-PRPHFNFRNPEEEQHYNNYMYRYGDYVY-NPPPRAQTYENFMSTCMNRTD----------------------------------------------
GAVAGMAVGYGLGRF-PRPHFNFRNPEEEHYYNNYMYRYGDYVY-KLPPRAETYDKFMSRCMNRTD----------------------------------------------
GALAGMAVGYGLGRF-PRPHFTFRNPEEEYYYNNYMQRYGDYVY-RPPPRAETYESFMSRCMSSND----------------------------------------------
GAVAGMAVGYGLGRF-PRPHFGFRSPSEEYYYNNYMYRQDERDYGRDYPRAESYEQFMDRCMNRTNLLKDQGSSKAEDNDTSIEEIGYPALIEQMCLEEYMAKSEKY-----
-AGVGALAGMAVGYGLPRPHFNFRNPEEESYYNNYMYRYGEKDYGRDYPRAESYEKFMARCMNETDNGQSRSPAPVSGVDTEEDDTVSPALIEQMCVEKYM-----------
-------------------DIDFGAE-GNRYYEANYWLFPDGIHYDGCDTNVTKELFVTNCINTTQAVNQEEFS-REKQDNKLYQRILWRLIRELCS---------------
-------------------DIDFGAE-GNRYYEANYWQFPDGIHYNGCEASVTKEKFVTSCINATQEVNQEELS-LEKPDNKLYQRVLWRLIRELCS---------------
GAVVGGLAGKAAAEAIN-------PTAEEAYWRDNYDREPYYEQGRSFDDYGPA----------------------------------------------------------
GAVVGGLAGKAAAESVN-------PTVEDAYWRESYQREPYYRNGRTYDEYRPA----------------------------------------------------------
GAVVGGLGGKAVAERVN-------PTAERSYWENAYDREPYYETGRTYDDYGPA----------------------------------------------------------
GAIAGGMGGKEAAEALD-------PTAEEAYWRDRHTLEPYYESGRPFDDYAPA----------------------------------------------------------
GAVVGGLAGKGAAEAVN-------PTVEDAYWRDAYQSEPYYVAGRSYEEYRPA----------------------------------------------------------
GAIVGGLAGKAAAEAVN-------PTEEDAYWREAYAREPYYVSGRTYEQYRPA----------------------------------------------------------
GAVAGGLGGKAVAESIN-------PTAEEAYWRDNYQREPYYEIGRSYDDYAPA----------------------------------------------------------
GAVVGGLAGKAAAEAVN-------PTAEDAYWRETYQREPYYVTGRAYEEYRPA----------------------------------------------------------
GAVAGGLAGKAAGEAVN-------PTNEDAYWRENYHREPYYVGGRTYDQYRPA----------------------------------------------------------
GAVAGGLAGKAAAEAVN-------PTEEDAYWRESYQREPYYVGGRTYDEYRPA----------------------------------------------------------
GAVAGMALGYGLGQ-FPRPHFHFHSPQEEYYYNHYMYRKSKR--YRDSPPPDTFDKFMSSCMKRTDLLHQSSP---------------------------------------
GAFVGGLTGNAVGEAID-------PTVEDAYWRENYLNQP---YYN------------------------------------------------------------------
GAVVGGLAGHAAGEAMD-------PTVEDAYWRENSINQP---YYS------------------------------------------------------------------
GAVVGGLAGHGVGEVVN-------PTEEEAYWRENSLNTP---YYT------------------------------------------------------------------
GAVVGGLAGKGVGEAIN-------PTEEQAYWRDNSINTP---YYS------------------------------------------------------------------
GAVAGGLMGRGA--AMD-------PEAEDTYWRSNYTGRP---YVT------------------------------------------------------------------
GAVAGAVMGRSV--KVD-------PVAEDTYWRENYTSRP---YIA------------------------------------------------------------------
GAVVGAVTGRRA--KVD-------HSAEDTYWRDNYSSRP---YVK------------------------------------------------------------------
GAVVGGLAGKGAAEAVN-------PTAEDAYWRDAHGRQP---YYS------------------------------------------------------------------
GAVAGALAGKAM--NVD-------PKEEDAYWRSNYADRP---YVT------------------------------------------------------------------
GAVVGGKAGSAAGEAID-------PTGEDIYWRDEGYTSAT--YYS------------------------------------------------------------------
-----------------KLDIDFGAK-DNQYYEANYWQFPEGIHCDSCEANVTKEMLVTVCINTTQAANQAEFS-REKQDNKLYQQVLWQLIKELCFGKR------------
GAVAGMALGYGLGSF-PRPRFHFHNPQEEYYYNHYMYRYPDGIFQN--PPPQSYDKFMDTCMKRTDLL--------------------------------------------
GAVGGMALGYGLGRF-PRPHFHFHSPQEEYYYNHYMYRYPDGIFQN--PPPQSYDSFMETCVKRTDLL--------------------------------------------
GAMAGMAIGYGIGNF-QRPNFQFRSPQEERYYNNHMYQQPSHTAGTDNAQAQSYDNFMKTCMKRNNLLRDQDSKEPEKNDTSIMQIGYPALIEQMCVELYIVYSESQR----
GALAGMAVGYGLGRF-PRPHFNFRNPQEEQSYNNYMYSY--NDYGRDYPRAESYDSFMDTCMKRNDLLQDQ-GRD------SIEEIGYPALVEQLCVELYINHSERFL----
GAVAKMAVDYGLGRF-PRPPFQFQSPEEEYYYNHYMYRYGANDYSRDYKPLENYERHMNTCMKNIELLPQIESTADRDSDTSIVEIGYPALIEQICLERYMVYSEKYL----
GALVGMALGYGLGRF-PRPPFSFHNPQEEHYYNHYMYRYGANDYSRDYQVPQTYDSYMDSCMKRADLLPQV-----EDDDTSIEEIGYPALIEQVCLELYVVYAERYL----
GVVAKMAVDYGLGRV-PHRQFQFQSPEEEYYYNYYMYTYGANDYSRDYQPLEDYERHMNTCMKTVELLPQIGAAADRDDDTSIVEIGYPALIRMMCLKIYIVNSEKYL----
GAVAGMAVGYGIGNF-PRPNFQFRSPQEERQYNHYMYSRPNYTSKQPSPNPQSYEQYMSTCMKRKDLLKEQNTSAREDDDTSIMQIGYPALIEQMCVELYLVYSGSF-----
GVVAGMALGYGLGRF-PRPHFGFHNPQEEYYYNHYMYRYGDNDYSRDYPPPQSYDNYMKNCMKRTDLLR-------------------------------------------
GGVAGGLAGKGIAESID-------PTVEDAYWRANHTARPYYDPSRTYDDYEP-----------------------------------------------------------
GAFGGGLAGKGVGEAID-------PTVEDAYWQSNYSSRPYADTSATYEDYQP-----------------------------------------------------------
GSFSGGLVGKGVAEAID-------PTVEDAYWQSNYSSRPYVDTSATYEDYQP-----------------------------------------------------------
GAVAGGLVGKGAAESVN-------PTEEEAYWEQNYNTRPYAEVDTDYEYYRP-----------------------------------------------------------
GGLIGGLAGKGVAEAVD-------PTEEDAYWRDEHANRDYYKDDRDYDHYKP-----------------------------------------------------------
GGVAGGYAGKAVAENID-------PTVESAYWEEEHANRPYYNKSYGFDQYRP-----------------------------------------------------------
GSVVGGLAGKSTAEQIN-------PTVEDTHWRASYSSRPYVEKGAVYEDYQP-----------------------------------------------------------
GSVVGGLAGKGTAEQIN-------PTFEENRWDEVYNSRSYVEAGTTYDDYEP-----------------------------------------------------------
GAVAGMAFGYGLGRF-PSPHFQLRNRQEEYYYNHYNHRKSNREQFRPPP--QDYSRYMDLCMERSDL---------------------------------------------
GSVAGGLMGKGAAEAVN-------PTEEETYWKDNYASRP------------------------------------------------------------------------
GSVAGGLAGKATAEAFD-------PTVEDEYWRENYRTRP------------------------------------------------------------------------
GAIVGGVAGSSVGEMLD-------PTVEYEYWQETHTTRPY-----------------------------------------------------------------------
GAIAGGAAGHAAGEAID-------PTIEDTYWNDTYSQTTY-----------------------------------------------------------------------
