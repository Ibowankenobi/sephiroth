#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sephWrapper.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from wrapperSrc.wrapperUtils import *
from sephiroth3 import sephiroth3
import sys
verbosity = 0

#USAGE: python sephWrapper.py database_path numBonds MSA_dir_path MSA_suffixes

def main():
	if len(sys.argv) != 5 or "-h" in sys.argv[1]:
		print "\nUSAGE: python sephWrapper.py database_path numBonds MSA_dir_path MSA_suffixes\n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	numBonds = int(sys.argv[2])
	TARGET_MSA_DIR=sys.argv[3]
	TARGET_MSA_NAME=sys.argv[4]
	predictions = {}
	
	#########PREDICTION PERFORMANCES EVALUATION PARAMETERS
	Qprot = 0
	numProts = 0
	Rb = 0
	edgeTot = 0
	############
	
	for target in dataBase.items(): #iterates among all the proteins with N bonds
		targetCys = getBonded(target[1][2])	#retrieves bonded cysteines from the annotations in the text file-dataset
		assert (len(targetCys) % 2) == 0 #just in case
		if len(targetCys) == numBonds*2: # IFF we are predicting the right number of bonds, proceed
			numProts += 1 #increase the number of predicted proteins
			assert not predictions.has_key(target[0])
			if verbosity >= 1:
				print "\nPredicting protein %s" % target[0]
			if  TARGET_MSA_DIR[-1] != "/": # avoid stupid path mistakes
				TARGET_MSA_DIR += "/"
			predictedEdges = sephiroth3(TARGET_MSA_DIR+target[0]+TARGET_MSA_NAME, targetCys) #calculate predictions
			predictions[target[0]] = predictedEdges
			groundTruth = extractCouplingFromMeta(target[1][2]) #extract the real connectivity from the dataset annotations
			########## assessing results			
			if verbosity >= 1:
				print "Prediction: ", predictedEdges
				print "real      : ", groundTruth
			if predictedEdges == groundTruth: #if predicted and real connectivity are equal, increase Qp
				print "CORRECT\n"
				Qprot += 1
			else:
				print "WRONG!!!\n"
			for i in predictedEdges: # check how many edges have been correctly predicted
				if i in groundTruth:
					Rb += 1
			edgeTot += len(groundTruth)	
			################
	print "*******************SCORES**********************"
	print "Num of proteins with %d bonds: %d" % (numBonds, numProts)
	print "Qp = %3.3f" % (Qprot/float(numProts))
	print "Rb = %3.3f" % (Rb/float(edgeTot))	
	
	return 0




if __name__ == '__main__':
	main()

